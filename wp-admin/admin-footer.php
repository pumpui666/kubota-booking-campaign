<?php
/**
 * WordPress Administration Template Footer
 *
 * @package WordPress
 * @subpackage Administration
 */

// Don't load directly.
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
 * @global string $hook_suffix
 */
global $hook_suffix;
?>

<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div><!-- wpbody -->
<div class="clear"></div></div><!-- wpcontent -->

<div id="wpfooter" role="contentinfo">
	<?php
	/**
	 * Fires after the opening tag for the admin footer.
	 *
	 * @since 2.5.0
	 */
	do_action( 'in_admin_footer' );
	?>
	<p id="footer-left" class="alignleft">
		<?php
		$text = sprintf(
			/* translators: %s: https://wordpress.org/ */
			__( 'Thank you for creating with <a href="%s">WordPress</a>.' ),
			__( 'https://wordpress.org/' )
		);

		/**
		 * Filters the "Thank you" text displayed in the admin footer.
		 *
		 * @since 2.8.0
		 *
		 * @param string $text The content that will be printed.
		 */
		echo apply_filters( 'admin_footer_text', '<span id="footer-thankyou">' . $text . '</span>' );
		?>
	</p>
	<p id="footer-upgrade" class="alignright">
		<?php
		/**
		 * Filters the version/update text displayed in the admin footer.
		 *
		 * WordPress prints the current version and update information,
		 * using core_update_footer() at priority 10.
		 *
		 * @since 2.3.0
		 *
		 * @see core_update_footer()
		 *
		 * @param string $content The content that will be printed.
		 */
		echo apply_filters( 'update_footer', '' );
		?>
	</p>
	<div class="clear"></div>
</div>
<?php
/**
 * Prints scripts or data before the default footer scripts.
 *
 * @since 1.2.0
 *
 * @param string $data The data to print.
 */
do_action( 'admin_footer', '' );

/**
 * Prints scripts and data queued for the footer.
 *
 * The dynamic portion of the hook name, `$hook_suffix`,
 * refers to the global hook suffix of the current page.
 *
 * @since 4.6.0
 */
do_action( "admin_print_footer_scripts-{$hook_suffix}" ); // phpcs:ignore WordPress.NamingConventions.ValidHookName.UseUnderscores

/**
 * Prints any scripts and data queued for the footer.
 *
 * @since 2.8.0
 */
do_action( 'admin_print_footer_scripts' );

/**
 * Prints scripts or data after the default footer scripts.
 *
 * The dynamic portion of the hook name, `$hook_suffix`,
 * refers to the global hook suffix of the current page.
 *
 * @since 2.8.0
 */
do_action( "admin_footer-{$hook_suffix}" ); // phpcs:ignore WordPress.NamingConventions.ValidHookName.UseUnderscores

// get_site_option() won't exist when auto upgrading from <= 2.7.
if ( function_exists( 'get_site_option' )
	&& false === get_site_option( 'can_compress_scripts' )
) {
	compression_test();
}

?>

<div class="clear"></div></div><!-- wpwrap -->
<script type="text/javascript">if(typeof wpOnload==='function')wpOnload();</script>

<script type="text/javascript">
	jQuery(document).ready(function(){
		
		jQuery("#wpcontent").append('<div class="spinner_g"></div>');
		//send variable
		var get_posttype =  '<?php echo  (!empty($_GET['post_type']))? $_GET['post_type'] : '' ;?>';
		var p_ =   '<?php echo  (!empty($_GET['post']))? $_GET['post'] : '' ;?>';
		//set url
		var site_url_j = '<?php  echo site_url(); ?>';
		<?php 
			$getpost =  get_post(); 
			if (!empty($getpost)) {
				$post_type_name =  $getpost->post_type;
			}
		?>
		var post_type__ = '<?php echo  (!empty($post_type_name))? $post_type_name : ''  ;?>';
		console.log('console backend form admin-footer');
		// console.log(post_type__);
		if(post_type__ == 'data_'){
			jQuery('.acf-field-6212008931b3f').hide();
			jQuery('.acf-field-62115c37d1ebb').hide();
			jQuery('.acf-field-621146b6b3b44').hide();
			jQuery('.page-title-action').hide();
			jQuery('.inline .hide-if-no-js').remove();
			
			
		}
		///////////////////////////////////////////////////////////////////
		if(get_posttype =='subscriber' || get_posttype =='data_' )
		{
			var txt = " <select name='type' id='type'> ";
			txt += " <option value='4' selected>ผู้สมัคร (ทั้งหมด)</option>";
			txt += " <option value='1' >ผู้สมัคร 100 คนแรก</option>";
			txt += " <option value='2'>ผู้สมัคร 100 ขึ้นไป</option>";
			txt += " <option value='3'>ผู้สมัคร วันที 16 มี.ค. 65</option>";
			txt += "</select>";
			jQuery("#wpbody-content .wrap h1").append(txt);
			var txt = " <select name='models' id='models'> ";
			txt += " <option value='4' selected>รุ่นที่จอง (ทั้งหมด)</option>";
			txt += " <option value='1'>M7508</option>";
			txt += " <option value='2'>M8808</option>";
			txt += " <option value='3'>M9808</option>";
			txt += "</select>";
			jQuery("#wpbody-content .wrap h1").append(txt);
			jQuery("#wpbody-content .wrap h1").append('<a style="margin-left:10px;"href="javascript:;" id="btn-export-'+get_posttype+'" class="button action">Export</a>');
			
		}
		/* export to excel */
		jQuery('#btn-export-data_').on('click', function(){
			var type = jQuery('#type').val();
			var models = jQuery('#models').val();
			var url = "<?php echo site_url(); ?>/export-data_/?order=666-"+type+"-"+ models;
			window.open(url);
			return false;
		});
		/* export to excel */
		jQuery('#btn-export-recruit').on('click', function(){
			var url = '<?php echo site_url("export-career-apply"); ?>';
			window.open(url);
			return false;
		});
		/////////////////////////////////////////////////////////////////////
		if(get_posttype =='subscriber')
		{

			// email -> mailTo
			var mail = jQuery('.row-title');
			jQuery.each(mail, function( index, obj ) {
			var mailaddress = jQuery(obj).text();
			jQuery(obj).html('<a href="mailto:'+mailaddress+'">'+mailaddress+'</a>');
			});
		}

		////////////////////////////////////////////////////////////////////
		if(post_type__ == 'training_course'){
			// console.log(jQuery('input[name="post_title"]').val());
			jQuery('input[name="post_title"]').blur(function(){

				var string_ = jQuery('.acf-field-5b16315363c04 .acf-input .acf-input-wrap input:first-child').val();
				// console.log(string_+"string_");
				if(string_ == "" || string_ == undefined){
					var onblur_data = jQuery('input[name="post_title"]').val();
					if(onblur_data.length>5){
						var string_1 = onblur_data.substring(0, 6);
						var string_2 = onblur_data.substring(6,onblur_data.length);
						jQuery('.acf-field-5b16315363c04 .acf-input .acf-input-wrap input:first-child').val(string_1);
						jQuery('.acf-field-5b44a6fe48cc1  .acf-input .acf-input-wrap input:first-child').val(string_2);
					}else{
						jQuery('.acf-field-5b16315363c04 .acf-input .acf-input-wrap input:first-child').val(onblur_data);
					}
				}
			});
		}

	});
</script>
</body>
</html>
