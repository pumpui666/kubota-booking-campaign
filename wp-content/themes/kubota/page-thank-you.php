<?php 
session_start();
  $data = [];
	if(!empty($_POST['register_title'])){
		$register_title = $_POST['register_title'];
	}else{
		$register_title = 1; //redirect ot something went wrong!!
	}
	if(!empty($_POST['cm'])){
		$cm = $_POST['cm'];
		if($cm == '1'){
			$cm = 'M7508';
		}elseif($cm == 2){
			$cm = 'M8808';

		}else{
			$cm = 'M9808';
		}
	}else{
		$cm = 'M7508';
	}
  $data = thankyou_page();
?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="thank-you-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
			<div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>

			<div class="container">
				<div class="box-content">
					<div class="inner">
						<div class="title-page">
							<h1 class="title">จองสิทธิ์สำเร็จ</h1>
						</div>
						<div class="detail">
						<span class="m_name" qazwsx="<?php echo $register_title; ?>" > </span>
						<?php if($data['status_open_activity_1'] == 'open'): ?> <!-- กรณีเปิดกิจกรรมที่ 1 วันที่ 1-15 --> 
							<?php if($data['status_count'] == '1'): ?>  <!-- 100 คนแรก --> 
								<?php $data['text_activity_'] = str_replace("[C1]",$cm,$data['text_activity_']); ?>
								<?php $data['text_activity_'] = str_replace("[C2]",$register_title,$data['text_activity_']); ?>
								<?php echo $data['text_activity_']; ?>
							<?php else: ?> <!-- ครบ 100 คน เริ่ม 101-200 คน--> 
								<?php $count = (!empty($data['count'] ))? $data['count']-100 : ''; ?>
								<?php $data['text_activity_'] = str_replace("[C1]",$cm,$data['text_activity_']); ?>
								<?php $data['text_activity_'] = str_replace("[C2]",$register_title,$data['text_activity_']); ?>
								<?php $data['text_activity_'] = str_replace("[COUNT]",$count,$data['text_activity_']); ?>
								
							<?php echo $data['text_activity_']; ?>
							<?php endif; ?>
						</div>
						<div class="box-btn-download">
							<a href="#" onclick="loadimg_pomotion()" class="btn btn-downloading">
								<span>
									ดาวน์โหลดสิทธิ์
									<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/download.svg" alt="icon">
								</span>
							</a>
						</div>
						<div class="box-banner">
							<picture class='norm_picture'>
								<source srcset="<?php echo $data['banner_mobile']; ?>" media="(max-width: 479px)" type="image/jpeg">
								<source srcset="<?php echo $data['banner']; ?> 2x" type="image/jpeg">
								<img src="<?php echo $data['banner']; ?>" alt="" loading="lazy">
							</picture><!-- แบนเนอร์กิจกรรมที่1 -->
							<picture class='hidden_data_picture' style="display:none;">
								<img src="<?php echo $data['banner']; ?>" alt="" loading="lazy">
							</picture><!-- แบนเนอร์กิจกรรมที่2 -->
						</div>
	
						<?php elseif($data['status_open_activity_2'] == 'open'): ?> <!-- กรณีเปิดกิจกรรมที่ 2 วันที่ 16 --> 
							<?php $count = (!empty($data['count'] ))? $data['count']-100 : ''; ?>
							<?php $data['text_activity_'] = str_replace("[C1]",$cm,$data['text_activity_']); ?>
							<?php $data['text_activity_'] = str_replace("[C2]",$register_title,$data['text_activity_']); ?>
							<?php $data['text_activity_'] = str_replace("[COUNT]",$count,$data['text_activity_']); ?>
							<?php echo $data['text_activity_']; ?>
						<div class="box-btn-download">
							<a href="#" onclick="loadimg_pomotion()" class="btn btn-downloading">
								<span>
								ดาวน์โหลดสิทธิ์
								<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/download.svg" alt="icon">
								</span>
							</a>
						</div>
						<div class="box-banner">
							<picture class='norm_picture'>
								<source srcset="<?php echo $data['banner_mobile']; ?>" media="(max-width: 479px)" type="image/jpeg">
								<source srcset="<?php echo $data['banner']; ?> 2x" type="image/jpeg">
								<img src="<?php echo $data['banner']; ?>" alt="" loading="lazy">
							</picture><!-- แบนเนอร์กิจกรรมที่2 -->
							<picture class='hidden_data_picture' style="display:none;">
								<img src="<?php echo $data['banner']; ?>" alt="" loading="lazy">
							</picture><!-- แบนเนอร์กิจกรรมที่2 -->
						</div> 
						<?php endif; ?>
						<?php if($data['status_open_activity_1'] == 'open'): ?> <!-- กรณีเปิดกิจกรรมที่ 1 วันที่ 1-15 --> 
							<?php //if($data['status_count'] == '1'): ?> 
								<div class="detail">
									<div class="box-bg">
										<p>กรุณารอการติดต่อกลับจากตัวแทนจำหน่ายของทางบริษัทฯ ทั้งนี้ท่านจะต้องดำเนินการเช่าซื้อกับบริษัท สยามคูโบต้า ลีสซิ่ง จำกัด (SKL)
											<br>หรือดำเนินการชำระค่าสินค้า ให้แล้วเสร็จภายในวันที่ 15 เมษายน 2565 มิเช่นนั้นบริษัทฯ ขอสงวนสิทธิ์ในการมอบของสมนาคุณและสิทธิพิเศษของท่านทั้งหมด</p>
									</div>
								</div>
							<?php //endif; ?>
						<?php endif; ?>
						<div class="group-btn">
							<?php if(empty($_SESSION["username_codeid"]) || empty($_SESSION["codeid"])){?>
								<a href="<?php echo site_url(); ?>" class="btn">
									<span>
										กลับหน้าแรก
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/storage.svg" alt="icon">
									</span>
								</a>
							<?php }else{?>
								<a href="<?php echo site_url(); ?>" class="btn">
									<span>
										กลับหน้าแรก
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/storage.svg" alt="icon">
									</span>
								</a>
								<a href="<?php echo site_url('agent-management'); ?>" class="btn btn-back-customer-list">
									<span>
										กลับไปหน้ารายชื่อลูกค้า
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/storage.svg" alt="icon">
									</span>
								</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">
	    function iOSversion() {
			var iOS = !window.MSStream && /iPad|iPhone|iPod/.test(navigator.userAgent); // fails on iPad iOS 13
			if (iOS) { // <-- Use the one here above
			if (window.indexedDB) { return 'iOS 8 and up'; }
			if (window.SpeechSynthesisUtterance) { return 'iOS 7'; }
			if (window.webkitAudioContext) { return 'iOS 6'; }
			if (window.matchMedia) { return 'iOS 5'; }
			if (window.history && 'pushState' in window.history) { return 'iOS 4'; }
			return 'iOS 3 or earlier';
			}

			return 'notios';
      	}
      function download(url){
		var txt_name = $('.m_name').attr('qazwsx');
        var a = $("<a style='display:none' id='js-downloder'>")
        .attr("href", url)
        .attr("download", txt_name+".png")
        .appendTo("body");

        a[0].click();

        a.remove();
      }
      function saveCapture(element) {
		if(iOSversion() != 'notios'){
			// var vp = document.getElementById("viewportMeta").getAttribute("content");
        	// document.getElementById("viewportMeta").setAttribute("content", "width=800");
		}
		
		$('.box-btn-download').hide();
		
		$('.btn').hide();
		
		if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
		}else{
			$('.norm_picture').hide();
			$('.hidden_data_picture').show();
		}

        html2canvas(element , { 
			allowTaint: true,
			useCORS: true,
			logging: true, 
			windowWidth: element.scrollWidth,
                    windowHeight: element.scrollHeight
		}).then(  
				function(canvas) {
				download(canvas.toDataURL());
				if(iOSversion() != 'notios'){
					// document.getElementById("viewportMeta").setAttribute("content", vp);
				}
			}
		);
	
		$('.box-btn-download').show();
		$('.btn').show();
		
		if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
		}else{
			$('.norm_picture').show();
			$('.hidden_data_picture').hide();
		}
		// html2canvas(document.getElementById('#main-content'), {
		// 	onrendered: function(canvas) {
		// 		download(canvas.toDataURL("image/png"));
		// 	},
		// 	allowTaint : true,
		// 	letterRendering: 1
		// // width: 300,
		// // height: 300
		// });
		// html2canvas(document.getElementById('invoice-panel'), { letterRendering: 1, allowTaint : true, onrendered : function (canvas) { } });

      }

      function loadimg_pomotion(){
        // var element = document.querySelector("#capture");
        var element = document.querySelector("#main-content");
          
		saveCapture(element)
		// saveCapture();
      }
 
  </script>
  <!-- end javascript this page -->