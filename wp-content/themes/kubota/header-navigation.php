<?php
  $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);  
  $segments = explode('/', $url);
  if(empty($segments[2])){
    $segments[2] = '';
  }
?>
<header id="header">
  <div class="container">
    <a href="<?php echo site_url(); ?>" class="logo">
      <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo.svg" alt="Kubota นวัตกรรมเกษตรเพื่ออนาคต">
    </a>

    <a href="tel:02-029-1747" class="logo-connect"><img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-connect.svg" alt="Kubota Connect 02-029-1747"></a>
  </div>
</header>