<!-- main -->
<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.min.js"></script>

<!-- plugin -->
<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.mousewheel.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.easing.1.3.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/gsap.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/assets/js/fancybox.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.scrollbar.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/assets/js/scroll-out.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.mask.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.easy-autocomplete.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/assets/js/main.js"></script>

<script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>

<?php 
  $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);  
  $segments = explode('/', $url);
  if(empty($segments[1])){
    $segments[1] = '';
  }
?>
<script>
    var site_url = '<?php echo site_url(); ?>';
    var tem_url = '<?php echo get_template_directory_uri(); ?>';
    var base_url = '<?php echo get_site_url(); ?>';
    var admin_url = "<?php echo admin_url('admin-ajax.php'); ?>";
    var lang = "<?php echo $segments[1]; ?>";
</script>