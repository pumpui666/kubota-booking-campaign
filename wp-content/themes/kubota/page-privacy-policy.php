<?php 
	if(!empty($_GET['method'])){
		$method = $_GET['method'];
	}else{
		$method = 1;
	}
	if(!empty($_GET['cm'])){
		$cm = $_GET['cm'];
	}else{
		$cm = 4;
	}
?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="privacy-policy-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
			<div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>
			
			<div class="container">
				<div class="box-content">
					<div class="inner">
						<?php
							// Start the Loop.
							while ( have_posts() ) :
								the_post(); ?>
									<?php the_content(); ?>
									<!-- <div class="box-editor">
										<h1>นโยบายคุ้มครองข้อมูลส่วนบุคคล
											<br>บริษัทสยามคูโบต้าคอร์ปอเรชั่น จำกัด</h1>
										<p>บริษัทสยามคูโบต้าคอร์ปอเรชั่น จำกัด (“บริษัท”) ให้ความสำคัญในเรื่องการคุ้มครองและเคารพสิทธิในความเป็นส่วนตัวของท่านที่มีอยู่ตามพระราชบัญญัติคุ้มครองข้อมูลส่วนบุคคล พ.ศ. 2562 และฉบับปรับปรุงแก้ไขตามที่จะมีการปรับปรุงแก้ไขเป็นคราวๆ และกฎหมายและกฎระเบียบที่ใช้บังคับอื่น ๆ (“พ.ร.บ. คุ้มครองข้อมูลส่วนบุคคล”) ที่ใช้บังคับในประเทศไทย และมุ่งหมายที่จะคุ้มครองข้อมูลส่วนบุคคลของท่านที่บริษัทเก็บรวบรวม ใช้ เปิดเผยเพื่อการดำเนินกิจการของบริษัท บริษัทมีหน้าที่ตามที่กำหนดไว้ในเรื่องการคุ้มครองข้อมูลส่วนบุคคล ความเป็นส่วนตัว หรือความปลอดภัยของข้อมูล ที่ใช้บังคับและมีผลบังคับอยู่ในประเทศไทย การจัดทำนโยบายฉบับนี้เป็นส่วนหนึ่งในความพยายามของบริษัทที่จะดำเนินการให้บรรลุวัตถุประสงค์ของนโยบายและปฏิบัติหน้าที่ของตนตาม พ.ร.บ. คุ้มครองข้อมูลส่วนบุคคล ดังกล่าว</p>

										<span class="line"></span>
												
										<h2>ขอบเขตของนโยบายคุ้มครอง<br class="mobile-only">ข้อมูลส่วนบุคคล</h2>
										<p>นโยบายคุ้มครองข้อมูลส่วนบุคคลฉบับนี้มีรายละเอียดเกี่ยวกับแนวปฏิบัติของบริษัทในเรื่องความเป็นส่วนตัว วัตถุประสงค์ของการเก็บรวบรวม ใช้ เปิดเผยข้อมูลส่วนบุคคล ประเภทของข้อมูลส่วนบุคคลที่บริษัทอาจเก็บรวบรวม ใช้ เปิดเผย วิธีที่บริษัทมุ่งหมายจะใช้ข้อมูลส่วนบุคคล บุคคลภายนอกที่บริษัทเปิดเผยข้อมูลให้ทราบ การจัดเก็บรวบรวม และรักษาข้อมูลส่วนบุคคล วิธีที่ท่านสามารถเข้าถึงและขอแก้ไขข้อมูลส่วนบุคคลของท่านที่บริษัทเก็บไว้ และวิธีที่ท่านอาจร้องเรียนหากเห็นว่าบริษัทกระทำขัดต่อกฎหมายว่าด้วยการคุ้มครองข้อมูลส่วนบุคคลที่ใช้บังคับ</p>
										<p>กรุณาอ่านนโยบายคุ้มครองข้อมูลส่วนบุคคลโดยละเอียด</p>
										<p><a href="https://siamkubota.co.th/privacy-and-cookies-policy" target="_blank">https://siamkubota.co.th/privacy-and-cookies-policy</a></p>
									</div> -->
									<a href="<?php echo site_url('check-id')."?method=".$method."&cm=".$cm ?>" class="btn">
										<span>
											ยอมรับ
											<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
										</span>
									</a>
							<?php 
								// the_content();
							endwhile; // End the loop.
							?>
					</div>
				</div>


			</div>
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script>
    /*assets/js/main.js*/
    // runScriptHomePage();
  </script>
  <!-- end javascript this page -->