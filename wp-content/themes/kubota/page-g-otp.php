<?php 
	if(!empty($_POST['method'])){
		$method = $_POST['method'];
	}else{
		$method = 1;
	}
	if(!empty($_POST['cm'])){
		$cm = $_POST['cm'];
	}else{
    $cm = 4;
	}
  if(!empty($_POST['personalid'])){
		$personalid = $_POST['personalid'];
	}else{
    Redirect(site_url(), false);// redirect
	}
  $data = [];
  $data['status_open_activity_1'] = get_field('open_activety_1',6); //check open activity 1
	$data['status_open_activity_2'] = get_field('open_activety_2',6); //check open activity 1
  $data['count'] = 	get_field('count',21);
  $count = (!empty($data['count'] ))? $data['count'] : 0; 

  if(($data['status_open_activity_1'] == 'open' &&  $count <200 )|| $data['status_open_activity_2'] == 'open'){
    $check_status =  'op';
  }else{
    $check_status =  'ed';
    Redirect(site_url(), false);// redirect
  }

?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="request-otp">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
      <div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>
      
      <div class="container">
        <div class="box-content">
          <div class="inner">
            <div class="title-page">
              <h1 class="title">หมายเลขโทรศัพท์มือถือ</h1>
              <p class="sub-title">รับรหัส OTP เพื่อยืนยันตัวตนของคุณ</p>
            </div>
            
            <div class="form-style">
              <form action="<?php echo site_url('v-otp'); ?>" method="post" id="submit_form">
                <div class="box-input required">
                  <label for="phone" class="text-label">หมายเลขโทรศัพท์มือถือ</label>
                  <input type="tel" id="phone" class="form-input" name="phone" placeholder="000-000-0000" maxlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                  <p class="text-validate">กรุณาระบุ</p>
                </div>
                <input type="hidden" id="ref" name="ref" >
                <input type="hidden" id="personalid" name="personalid" value="<?php echo $personalid; ?>" >
                <input type="hidden" id="transid" name="transid" >
                <input type="hidden" id="method" name="method" value="<?php echo $method; ?>">
                <input type="hidden" id="cm" name="cm" value="<?php echo $cm; ?>">
                <button type="button" class="btn btn-submit">
                  <span>
                    รับรหัส OTP
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/otp.svg" alt="icon">
                  </span>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div> 
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <div style="display: none;">
    <div id="popup" class="popup-check-id">
      <div class="inner">
        <h2>หมายเลขโทรศัพท์มือถือ</h2>
        <div class="box-bg">
          <p id="id_p_box_bg"> </p>
          <!-- <p>หมายเลขบัตรประชาชนนี้ถูกลงทะเบียนแล้ว<br class="show-all">คุณจอง <strong>KUBOTA</strong> รุ่น <strong class="model">M7508</strong></p> -->
        </div>
        <div class="box-remark">
          <p id="id_p_box_remark"> </p>
          <!-- <p>หากต้องการแก้ไขข้อมูลกรุณาติดต่อที่<br class="show-all"><strong>หจก.คูโบต้าศรีสะเกษเลาเจริญ</strong></p> -->
        </div>
      </div>
    </div>
  </div>

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">
      function validatePhoneNumber(input_str) 
      {
          var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

          return re.test(input_str);
      }

      $('.btn-submit').click(function(){
        var check= true;
        // if($('#phone').val()=== ""){
        //   $("#phone").parent().addClass('error');
        //   check = false;
        // }else{
        //   $("#phone").parent().removeClass('error');
        // }
        if(!validatePhoneNumber($('#phone').val())){
          $('#id_p_box_bg').text('กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง');
          popupCheckId();
          check = false;
          // return false;
        }

        var data = new FormData();

        data.append("phone", $('#phone').val());
        data.append("action", 'send-ajax-phoneotp');
        if(check == false){
          return false;
        }
        $('.btn-submit').css('pointer-events','none');
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            // alert('success');
            $('.btn-submit').css('pointer-events','auto');
            if(data.Status == 'ok'){
              // console.log(data.transid);
              // console.log(data.ref);
              showLoading();
              $('#transid').val(data.transid[0]);
              $('#ref').val(data.ref[0]);
              $('#submit_form').submit();
            }else{
              if(typeof data.MSG[0] !== "undefined")
              {
                $('#id_p_box_bg').text(data.MSG[0]);
                popupCheckId();
                // alert(data.MSG[0]);
              }else{
                $('#id_p_box_bg').text(data.MSG);
                popupCheckId();
                // alert(data.MSG);
              }
            }

          },
          error: function (data) {
            // alert(data);
            console.log("error: "+data);
            alert('something went wrong! please try again later.');
            // return false;
          }
        }); // close ajax
       
      });

      function popupCheckId() {
        Fancybox.show([{ src: "#popup", type: "inline" }]);
      }
      // showLoading();
      // hideLoading();
  </script>
  <!-- end javascript this page -->