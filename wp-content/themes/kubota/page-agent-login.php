<?php 
session_start();
?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="agent-login-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
      <div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>
      
      <div class="container">
        <div class="box-content">
          <div class="inner">
            <div class="title-page">
              <h1 class="title">ระบบการสั่งจอง<br class="show-all">แทรกเตอร์คูโบต้า <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" class="logo-m-series" width="156" h="16"></h1>
              <p class="sub-title">สำหรับผู้แทนจำหน่ายสยามคูโบต้า</p>
            </div>
            
            <div class="form-style form-full">
              <form action="<?php echo site_url('agent-management'); ?>" method="post" id="submit_form">
                <div class="row">
                  <div class="box-input required">
                    <label for="username" class="text-label">Username</label>
                    <input type="text" id="username" class="form-input" name="username" placeholder="Username">
                    <p class="text-validate">กรุณาระบุ</p>
                  </div>
                  <div class="box-input required">
                    <label for="password" class="text-label">Password</label>
                    <input type="password" id="password" class="form-input" name="password" placeholder="Password">
                    <p class="text-validate">กรุณาระบุ</p>
                  </div>
                </div>
                <input type="hidden" id="codeusername" name="codeusername" >
                <button type="button" class="btn btn-submit">
                  <span>
                    ตกลง
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
                  </span>
                </button>
                <p class="text-forgot-password">ลืมรหัสผ่าน กรุณาติดต่อที่<br><strong>KUBOTA CONNECT <a href="tel:02-029-1747">02-029-1747</a></strong></p>
              </form>
            </div>
          </div>
        </div>
      </div> 
			
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">

      $('.btn-submit').click(function(){
        var check= true;
        if($('#personalid').val()=== ""){
          $("#personalid").parent().addClass('error');
          check = false;
        }else{
          $("#personalid").parent().removeClass('error');
        }

        var data = new FormData();

        data.append("username", $('#username').val());
        data.append("password", $('#password').val());
        data.append("action", 'check-ajax-userpassword');
        if(check == false){
          return false;
        }
        $('.btn-submit').css('pointer-events','none');
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            // alert('success');
            $('.btn-submit').css('pointer-events','auto');
            // $('input').val('');
            // $('textarea').val('');
            if(data.Status == 'OK'){
              $('#codeusername').val(data.codeid+"/"+data.username);
              $('#submit_form').submit();
            }else if(data.Status ==  'error'){
              alert(data.MSG);
            }else{
              console.log(data);
            } 

          },
          error: function (data) {
            // alert(data);
            console.log("error: "+data);
            // return false;
          }
        }); // close ajax
       
      });
  </script>
  <!-- end javascript this page -->