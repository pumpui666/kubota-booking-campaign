
<?php 
session_start();
$data = home_page();
// echo "<pre>"; var_dump($data); die;
?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="home-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
			<div class="section-highlight-content">
					<?php 
						if(!empty($data['banner']) && !empty($data['banner_mobile'])){
							$banner_ = $data['banner'];
							$banner_x2 = get_template_directory_uri().'/assets/img/home/highlight-content-bg@2x.jpg 2x';
							$banner_mobile = $data['banner_mobile'];
						}else{
							$banner_mobile=	get_template_directory_uri().'/assets/img/home/highlight-content-bg-mobile.jpg';
							$banner_x2 =	get_template_directory_uri().'/assets/img/home/highlight-content-bg@2x.jpg 2x';
							$banner_ =	get_template_directory_uri().'/assets/img/home/highlight-content-bg.jpg';
						}
					?>
				<div class="box-img-bg">
					<picture>
						<source srcset="<?php echo $banner_mobile;?>" media="(max-width: 479px)" type="image/jpeg">
						<source srcset="<?php echo $banner_x2;?> 2x" type="image/jpeg">
						<img src="<?php echo $banner_;?>" alt="bg" loading="lazy" width="1920" height="880">
					</picture>
				</div>
				
				<div class="box-text">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/home/highlight-content-text-title.svg" alt="เป็นเจ้าของแทรกเตอร์ คูโบต้า M-SERIES ก่อนใครในโลก" class="text-title" width="439" height="155">
					<div class="detail">
						<p>สั่งจองล่วงหน้า<br><span class="mobile-hide">เพื่อ</span>รับข้อเสนอสุดพิเศษ<span class="mobile-hide">กว่าใคร</span></p>
						<p><span class="mobile-hide">รวม</span>มูลค่าสูงสุด<strong>168,000</strong>บาท</p>
					</div>

					<?php if(($data['status_open_activity_2'] == 'open') || ($data['status_open_activity_1'] == 'open')): ?> <!-- กรณีเปิดกิจกรรมทั้งสอง --> 

						<?php if($data['status_open_activity_1'] == 'open'): ?> <!-- กรณีเปิดกิจกรรมที่ 1 วันที่ 1-15 --> 
							<?php if($data['status_count'] == '1'): ?>  <!-- 100 คนแรก --> 
								<?php $count = (!empty($data['count'] ))? $data['count'] : ''; ?>
								<?php $data['text_activity_'] = str_replace("[COUNT]",$count,$data['text_activity_']); ?>
								<?php 
									if(!empty($count)){
										$input = str_pad($count, 3, "0", STR_PAD_LEFT);
									}else{
										$input = '100';
									}
								?>
									<?php $data['text_activity_'] = str_replace("[CD1]",$input[0],$data['text_activity_']); ?>
									<?php $data['text_activity_'] = str_replace("[CD2]",$input[1],$data['text_activity_']); ?>
									<?php $data['text_activity_'] = str_replace("[CD3]",$input[2],$data['text_activity_']); ?>
								<div class="box-proposal">
									<?php echo $data['text_activity_']; ?>
								</div>
							<?php elseif($data['status_count'] == '2'): ?> <!-- ครบ 100 คน เริ่ม 101-200 คน-->
								<?php $count = (!empty($data['count'] ))? $data['count'] : ''; ?>
								<?php $data['text_activity_'] = str_replace("[COUNT]",$count-100,$data['text_activity_']); ?>
								<?php 
									if(!empty($count)){
										$input = str_pad($count, 3, "0", STR_PAD_LEFT);
									}else{
										$input = '100';
									}
								?>
								<?php $data['text_activity_'] = str_replace("[CD1]",$input[0],$data['text_activity_']); ?>
								<?php $data['text_activity_'] = str_replace("[CD2]",$input[1],$data['text_activity_']); ?>
								<?php $data['text_activity_'] = str_replace("[CD3]",$input[2],$data['text_activity_']); ?>
								
								<div class="box-proposal">
									<?php echo $data['text_activity_']; ?>
								</div>
							<?php else: ?>
							<?php endif; ?>
						

						<?php elseif($data['status_open_activity_2'] == 'open'): ?> <!-- กรณีเปิดกิจกรรมที่ 2 วันที่ 16 --> 
							<div class="box-proposal">
								<?php echo $data['text_activity_']; ?>
							</div>
						<?php endif; ?>

					<?php endif; ?>
					

					
					
					<?php if(($data['status_open_activity_2'] == 'open') || ($data['status_open_activity_1'] == 'open')): ?> <!-- กรณีเปิดกิจกรรมทั้งสอง --> 
						<div class="group-btn">
							<?php 
								$count = (!empty($data['count'] ))? $data['count'] : 0; 

								if(($data['status_open_activity_1'] == 'open' &&  $count <200 )|| $data['status_open_activity_2'] == 'open'){
									$txt_norm =  'ลงทะเบียนรับสิทธิ์ด้วยตนเอง';
									$txt_agent =  'ลงทะเบียนรับสิทธิ์ผ่านผู้แทนจำหน่าย';
							?>
							<span id="ccc" qaz="op"></span>
							<?php
								}else{
							?>
							<!-- <span id="ccc" qaz="cd"></span> -->
							<?php
									$txt_norm =  'แก้ไขข้อมูลส่วนตัว';
									$txt_agent = 'ผู้แทนจำหน่ายแก้ไขข้อมูลลูกค้า';
								}
								
							?>
								
								<a href="<?php echo site_url('privacy-policy').'?method=1' ?>" class="btn btn-personal">
									<span>
										<?php echo $txt_norm;  ?>
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/personal.svg" alt="icon">
									</span>
								</a>
							<?php if(empty($_SESSION["username_codeid"]) || empty($_SESSION["codeid"])){?>
								<a href="<?php echo site_url('agent-login'); ?>" class="btn">
									<span>
										<?php echo $txt_agent;  ?>
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/agent.svg" alt="icon">
									</span>
								</a>
							<?php }else{?>
								<a href="<?php echo site_url('agent-management'); ?>" class="btn">
									<span>
										<?php echo $txt_agent;  ?>
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/agent.svg" alt="icon">
									</span>
								</a>
							<?php } ?>
						
						</div>
					<?php endif; ?>
				</div>
			</div>

			<?php if(($data['status_open_activity_2'] == 'open') || ($data['status_open_activity_1'] == 'open')): ?> <!-- กรณีเปิดกิจกรรมทั้งสอง --> 
				<div class="fixed-position">
					<div class="group-btn">
						<a href="<?php echo site_url('privacy-policy').'?method=1' ?>" class="btn btn-personal">
							<span>
								<?php echo $txt_norm;  ?>
								<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/personal.svg" alt="icon">
							</span>
						</a>
						<?php if(empty($_SESSION["username_codeid"]) || empty($_SESSION["codeid"])){?>
							<a href="<?php echo site_url('agent-login'); ?>" class="btn">
								<span>
									<?php echo $txt_agent;  ?>
									<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/agent.svg" alt="icon">
								</span>
							</a>
						<?php }else{?>
							<a href="<?php echo site_url('agent-login'); ?>" class="btn">
								<span>
									<?php echo $txt_agent;  ?>
									<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/agent.svg" alt="icon">
								</span>
							</a>
						<?php } ?>
					</div>
				</div>
			<?php endif; ?>

			<div class="section-promotion">
				<div class="img-promotion">
					<picture>
						<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/promotion-img-01-mobile.png" media="(max-width: 479px)" type="image/jpeg">
						<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/promotion-img-01@2x.png 2x" type="image/png">
						<img src="<?php echo get_template_directory_uri();?>/assets/img/home/promotion-img-01.png" alt="Img Promotion" loading="lazy" width="1920" height="726">
					</picture>
				</div>
			</div>
			
			<div class="section-video">
				<div class="container">
					<div class="video">
						<iframe width="100%" height="100%" src="<?php echo $data['youtube']; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			
			<div class="section-special-strength">
				<div class="container">
					<h2 class="title-section">จุดเด่นพิเศษ<br>สำหรับ <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" width="271" height="28" class="logo-m-series"></h2>

					<div class="list-content">
						<div class="item item-01">
							<div class="box-text">
								<h3>อีกขั้นของความว้าว<br>ด้วยดีไซน์โฉมใหม่</h3>
								<p>โดดเด่น ด้วยดีไซน์เร้าใจ แข็งแกร่ง ดุดัน สะกดทุกสายตา<br>พร้อมไฟหลังคาแบบ LED ส่องสว่างกว้างไกล <br>เพื่อทัศนวิสัยที่ยอดเยี่ยม</p>
							</div>
							<div class="img">
								<picture>
									<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-01@2x.jpg 2x" type="image/jpeg">
									<img src="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-01.jpg" alt="อีกขั้นของความว้าว ด้วยดีไซน์โฉมใหม่" loading="lazy" width="609" height="460">
								</picture>
							</div>
						</div>
						<div class="item item-02">
							<div class="box-text">
								<h3>อีกขั้นของความแกร่ง<br class="mobile-hide">ด้วย<br class="mobile-only">น้ำหนักตัวรถมากกว่า 3 ตัน</h3>
								<p>พลังแห่งความหนัก แรงฉุดลากเต็มพลัง <br>ด้วยน้ำหนักตัวแทรกเตอร์ที่มากกว่า 3 ตัน <br>พร้อมลุยทั้งงานไร่และงานสวน</p>
							</div>
							<div class="img">
								<picture>
									<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-02@2x.jpg 2x" type="image/jpeg">
									<img src="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-02.jpg" alt="อีกขั้นของความแกร่ง ด้วยน้ำหนักตัวรถมากกว่า 3 ตัน" loading="lazy" width="609" height="460">
								</picture>
							</div>
						</div>
						<div class="item item-03">
							<div class="box-text">
								<h3>อีกขั้นของที่สุด<br>แห่งความคล่องตัว</h3>
								<p>ให้คุณเคลื่อนที่ได้อย่างอิสระ ด้วยเกียร์เปลี่ยนทิศทาง<br>ไฮดรอลิกชัทเทิล เดินหน้า-ถอยหลัง โดยไม่ต้อง<br>เหยียบคลัตซ์และหยุดรถสนิท</p>
							</div>
							<div class="img">
								<picture>
									<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-03@2x.jpg 2x" type="image/jpeg">
									<img src="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-03.jpg" alt="อีกขั้นของที่สุดแห่งความคล่องตัว" loading="lazy" width="609" height="460">
								</picture>
							</div>
						</div>
						<div class="item item-04">
							<div class="box-text">
								<h3>อีกขั้นของความแรง<br>เต็มขุมพลัง</h3>
								<p>ด้วยเครื่องยนต์ดีเซล ไดเร็กต์อินเจ็กชัน <br>*ระบบเทอร์โบชาร์จเจอร์มีเฉพาะในรุ่น M8808 และ M9808 <br>แรง รอบจัด ประหยัดน้ำมัน</p>
							</div>
							<div class="img">
								<picture>
									<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-04@2x.jpg 2x" type="image/jpeg">
									<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-04@2x.jpg 2x" type="image/jpeg">
									<img src="<?php echo get_template_directory_uri();?>/assets/img/home/special-strength-img-04.jpg" alt="อีกขั้นของความแรงเต็มขุมพลัง" loading="lazy" width="609" height="460">
								</picture>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="section-feature">
				<div class="leaf"><img src="<?php echo get_template_directory_uri();?>/assets/img/home/feature-leaf.png" alt="Leaf"></div>
				
				<div class="box-img-bg">
					<picture>
						<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/feature-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
						<source srcset="<?php echo get_template_directory_uri();?>/assets/img/home/feature-bg@2x.jpg 2x" type="image/jpeg">
						<img src="<?php echo get_template_directory_uri();?>/assets/img/home/feature-bg.jpg" alt="bg" loading="lazy" width="1920" height="400">
					</picture>
				</div>

				<div class="container">
					<div class="list-feature">
						<div class="item">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri();?>/assets/img/home/icon-feature-01.svg" alt="icon">
							</div>
							<p>12 เกียร์เดินหน้า<br>12 เกียร์ถอยหลัง</p>
						</div>
						<div class="item">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri();?>/assets/img/home/icon-feature-02.svg" alt="icon">
							</div>
							<p>โครงหลังคานิรภัย<br>แบบโรลบาร์ (ROPS)</p>
						</div>
						<div class="item">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri();?>/assets/img/home/icon-feature-03.svg" alt="icon">
							</div>
							<p>ช่องจ่ายไฟ 12V</p>
						</div>
						<div class="item">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri();?>/assets/img/home/icon-feature-04.svg" alt="icon">
							</div>
							<p>เบาะนั่งนุ่มสบาย ปรับได้<br>พร้อมที่พักแขน</p>
						</div>
						<div class="item">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri();?>/assets/img/home/icon-feature-05.svg" alt="icon">
							</div>
							<p>อุปกรณ์ต่อพ่วง<br>ครบครัน</p>
						</div>
						<div class="item">
							<div class="icon">
								<img src="<?php echo get_template_directory_uri();?>/assets/img/home/icon-feature-06.svg" alt="icon">
							</div>
							<p>รับประกันคุณภาพทั้งคัน 2 ปี<br>หรือ 2,000 ชั่วโมง</p>
						</div>
					</div>
				</div>
			</div>
			
			<div class="section-model">
				<div class="container">
					<h2 class="title-section">รุ่นพิเศษที่เปิดจอง</h2>

					<div class="list-model">
						<div class="item">
							<div class="img">
								<picture>
									<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m9808@2x.png 2x" type="image/png">
									<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m9808.png" alt="M9808" loading="lazy" width="365" height="260">
								</picture>

								<div class="badge">
									<p>
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
										จองรุ่นนี้
									</p>
								</div>
							</div>
							<div class="text">
								<div class="group-text">
									<h3>แทรกเตอร์คูโบต้า</h3>
									<img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m9808.svg" alt="M9808">
									<p>ขนาด 98 แรงม้า</p>
								</div>
								<p class="price"><small>ราคา</small><strong>1,379,000</strong>ล้านบาท</p>
							</div>
							<a href="<?php echo site_url('privacy-policy').'?cm=3' ?>">Link จองรุ่นนี้</a>
						</div>
						<div class="item">
							<div class="img">
								<picture>
									<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m8808@2x.png 2x" type="image/png">
									<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m8808.png" alt="M8808" loading="lazy" width="365" height="260">
								</picture>

								<div class="badge">
									<p>
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
										จองรุ่นนี้
									</p>
								</div>
							</div>
							<div class="text">
								<div class="group-text">
									<h3>แทรกเตอร์คูโบต้า</h3>
									<img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m8808.svg" alt="M8808">
									<p>ขนาด 88 แรงม้า</p>
								</div>
								<p class="price"><small>ราคา</small><strong>1,254,000</strong>ล้านบาท</p>
							</div>
							<a href="<?php echo site_url('privacy-policy').'?cm=2' ?>">Link จองรุ่นนี้</a>
						</div>
						<div class="item">
							<div class="img">
								<picture>
									<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m7508@2x.png 2x" type="image/png">
									<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m7508.png" alt="M7508" loading="lazy" width="365" height="260">
								</picture>

								<div class="badge">
									<p>
										<img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
										จองรุ่นนี้
									</p>
								</div>
							</div>
							<div class="text">
								<div class="group-text">
									<h3>แทรกเตอร์คูโบต้า</h3>
									<img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m7508.svg" alt="M7508">
									<p>ขนาด 75 แรงม้า</p>
								</div>
								<p class="price"><small>ราคา</small><strong>1,058,000</strong>ล้านบาท</p>
							</div>
							<a href="<?php echo site_url('privacy-policy').'?cm=1' ?>">Link จองรุ่นนี้</a>
						</div>
					</div>
				</div>
			</div>
			
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

	<div style="display: none;">
    <div id="popup" class="popup-full-register">
      <div class="inner">
        <h2>ขอขอบคุณลูกค้าทุกท่าน<br class="mobile-only">ที่ให้ความสนใจ<br>ลงทะเบียนสั่งจอง<br class="mobile-only">แทรกเตอร์คูโบต้า <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" class="logo-m-series" width="161" h="17">
				</h2>
        <p><strong>ขณะนี้มีผู้ลงทะเบียนรับสิทธิ์เต็มจำนวนแล้ว</strong></p>
				<p>โปรดติดตามรายละเอียดการจัดจำหน่าย<br class="mobile-only">อย่างเป็นทางการอีกครั้ง<br><br class="mobile-only">ตั้งแต่วันที่ 17 มีนาคม 2565 <br class="mobile-only">ณ ตัวแทนจำหน่ายคูโบต้าทั่วประเทศ</p>
        <a href="javascript:void(0);" class="btn" onclick="closePopup();">
          <span>
            ปิด
            <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/close-white.svg" alt="icon">
          </span>
        </a>
      </div>
    </div>
  </div>

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script>
    /*assets/js/main.js*/
    runScriptHomePage();
	
		function popupFullRegister() {
			Fancybox.show([{ src: "#popup", type: "inline" }]);
		}
		function closePopup() {
			Fancybox.close();
		}

		$( document ).ready(function() {
		if($('#ccc').attr('qaz') != 'op'){
				popupFullRegister();
			}
		});
  </script>
  <!-- end javascript this page -->