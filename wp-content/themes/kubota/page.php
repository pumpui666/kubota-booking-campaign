<?php
function rr_404_notfound() {
    // 1. Ensure `is_*` functions work
    global $wp_query;
    $wp_query->set_404();
 
    // 2. Fix HTML title
    add_action( 'wp_title', function () {
        return '404: Not Found';
    }, 9999 );
 
    // 3. Throw 404
    status_header( 404 );
    nocache_headers();
 
    // 4. Show 404 template
    require get_404_template();
 
    // 5. Stop execution
    exit;
}
rr_404_notfound();
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			// Start the Loop.
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

			endwhile; // End the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
