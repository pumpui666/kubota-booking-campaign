<?php 
	if(!empty($_GET['method'])){
		$method = $_GET['method'];
	}else{
		$method = 1;
	}
	if(!empty($_GET['cm'])){
		$cm = $_GET['cm'];
	}else{
		$cm = 1;
	}
  if(!empty($_POST['phone'])){
		$phone = $_POST['phone'];
	}else{
		$phone = '';
	}
  $data = [];
  $data['status_open_activity_1'] = get_field('open_activety_1',6); //check open activity 1
	$data['status_open_activity_2'] = get_field('open_activety_2',6); //check open activity 1
  $data['count'] = 	get_field('count',21);
  $count = (!empty($data['count'] ))? $data['count'] : 0; 

  if(($data['status_open_activity_1'] == 'open' &&  $count <200 )|| $data['status_open_activity_2'] == 'open'){
    $check_status =  'op';
  }else{
    $check_status =  'ed';
  }
  

?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="check-id-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
      <div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>
      <div class="container">
        <div class="box-content">
          <div class="inner">
            <div class="title-page">
              <h1 class="title">สั่งจองล่วงหน้า
                <br><br class="mobile-only">แทรกเตอร์คูโบต้า <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" class="logo-m-series" width="156" h="16"></h1>
              <p class="sub-title">กรุณาระบุเลขบัตรประชาชน <br class="mobile-only">เพื่อตรวจสอบสิทธิ์หรือแก้ไขข้อมูลการจอง</p>
            </div>

            <div class="form-style">
              <form action="<?php echo site_url('g-otp'); ?>" method="post" id="submit_form">
                <div class="box-input required">
                  <label for="personalid" class="text-label">รหัสบัตรประชาชน</label>
                  <input type="tel" id="personalid" class="form-input" maxlength="13" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="personalid" placeholder="0-0000-00000-00-0">
                  <p class="text-validate">กรุณาระบุ</p>
                </div>
                <input type="hidden" id="phone" name="phone" value="<?php echo $phone; ?>">
                <input type="hidden" id="fullname" name="fullname" >
                <input type="hidden" id="method" name="method" value="<?php echo $method; ?>">
                <input type="hidden" id="cm" name="cm" value="<?php echo $cm; ?>">
                <button type="button" class="btn btn-submit">
                  <span>
                    ตกลง
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
                  </span>
                </button>
              </form>
            </div> 
          </div>
        </div>
      </div>
      <span id="ccc" qaz="<?php echo $check_status; ?>"></span>
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <div style="display: none;">
    <div id="popup" class="popup-check-id">
      <div class="inner">
        <h2>ตรวจสอบหมายเลขบัตรประชาชน</h2>
        <div class="box-bg">
       
          <p id="id_p_box_bg_error_agent"> หมายเลขบัตรประชาชนของท่าน ได้ทำการลงทะเบียนไว้แล้ว แก้ไขข้อมูลได้ที่หน้าจัดการของท่าน <a href="<?php echo site_url('agent-management'); ?>"> <u>แก้ไขข้อมูล</u> </a> </p>
          <p id="id_p_box_bg"> </p>
          <!-- <p>หมายเลขบัตรประชาชนนี้ถูกลงทะเบียนแล้ว<br class="show-all">คุณจอง <strong>KUBOTA</strong> รุ่น <strong class="model">M7508</strong></p> -->
        </div>
        <div class="box-remark">
          <p id="id_p_box_remark"> </p>
          <!-- <p>หากต้องการแก้ไขข้อมูลกรุณาติดต่อที่<br class="show-all"><strong>หจก.คูโบต้าศรีสะเกษเลาเจริญ</strong></p> -->
        </div>
      </div>
    </div>
  </div>

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">
    function Script_checkID(id){
      if(! IsNumeric(id)) return false;
      if(id.substring(0,1)== 0) return false;
      if(id.length != 13) return false;
      for(i=0, sum=0; i < 12; i++)
          sum += parseFloat(id.charAt(i))*(13-i);
      if((11-sum%11)%10!=parseFloat(id.charAt(12))) return false;
      return true;
    }
    function IsNumeric(input){
      var RE = /^-?(0|INF|(0[1-7][0-7]*)|(0x[0-9a-fA-F]+)|((0|[1-9][0-9]*|(?=[\.,]))([\.,][0-9]+)?([eE]-?\d+)?))$/;
      return (RE.test(input));
    }

      $('.btn-submit').click(function(){
        $('#id_p_box_bg_error_agent').hide();
        var check= false;
        if($.trim($('#personalid').val()) != '' && $('#personalid').val().length == 13){
          id = $('#personalid').val().replace(/-/g,"");
          var result = Script_checkID(id);
          if(result === false){
            // $('span.error').removeClass('true').text('เลขบัตรผิด');
            
          }else{
            // $('span.error').addClass('true').text('เลขบัตรถูกต้อง');
            check= true;
          }
        }
        if(check == false){
          $('#id_p_box_bg').text('กรุณากรอกเลขบัตรประชาชนให้ถูกต้อง');
          popupCheckId();
          // alert('กรุณากรอกเลขบัตรประชาชนให้ถูกต้อง');
          return false;
        }
        

       

        var data = new FormData();

        data.append("personalid", $('#personalid').val());
        data.append("method", $('#method').val());
        data.append("action", 'check-ajax-porsenalid');
        if(check == false){
          return false;
        }
        showLoading();
        $('.btn-submit').css('pointer-events','none');
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            hideLoading();
            // alert('success');
            $('.btn-submit').css('pointer-events','auto');
            // $('input').val('');
            // $('textarea').val('');
            console.log(data.Status);
            if(data.Status == 'ok'){
              showLoading();
              
              // ขออภัย ขณะนี้มีผู้ลงทะเบียนรับข้อเสนอสุดพิเศษเต็มจำนวนแล้วค่ะ
              if($('#ccc').attr('qaz') == 'op'){
                if($('#method').val()==1){
                  $('#submit_form').submit();
                }else{
                  $('#submit_form').attr('action', site_url+'/agent-register-form');
                  $('#submit_form').submit();
                }
              }else{
                hideLoading();
                $('#id_p_box_bg').text('ขออภัย ขณะนี้มีผู้ลงทะเบียนรับข้อเสนอสุดพิเศษเต็มจำนวนแล้วค่ะ');
                popupCheckId();
                return false;
              }
               

            }else if(data.Status ==  'edit_normal'){
              showLoading();
              $('#submit_form').attr('action', site_url+'/gu-otp');
              $('#fullname').val(data.fullname);
              $('#phone').val(data.phone);
              $('#submit_form').submit();
            }else if(data.Status ==  'alreadyuse_agent'){ //alreadyuse_agent
              $('#id_p_box_bg').text(data.MSG);

              popupCheckId();
              // alert(data.MSG);
            }else if(data.Status ==  'error_login'){ //error_login
              $('#id_p_box_bg').text(data.MSG);
              popupCheckId();
              // alert(data.MSG);
            }else if(data.Status ==  'error_alreadyuse_agent'){ //error_alreadyuse_agent
             $('#id_p_box_bg_error_agent').show();
              popupCheckId();
              // alert(data.MSG);
            }else if(data.Status ==  'error_edit_agent'){ //error_edit_agent
              $('#id_p_box_bg').text(data.MSG);
              popupCheckId();
              // alert(data.MSG);
            }else if(data.Status ==  'error_edit_normal'){ //error_edit_normal
              $('#id_p_box_bg').text(data.MSG);
              popupCheckId();
              // alert(data.MSG); 
            }

          },
          error: function (data) {
            hideLoading();
            // alert(data);
            console.log("error: "+data);
            // return false;
          }
        }); // close ajax
      });

      function popupCheckId() {
        Fancybox.show([{ src: "#popup", type: "inline" }]);
      }
      // showLoading();
      // hideLoading();
  </script>
  <!-- end javascript this page -->