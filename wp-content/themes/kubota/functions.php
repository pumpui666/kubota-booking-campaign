<?php
/**
 * Twenty Nineteen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */
//get_aboutus()
//get_home_shipbuilding
// get_ship_product_cat
//get_contactus
//get_career
//save_ajax_carrer
//save_ajax_contact
// get_ship_repair
//home_page()
//top_menu()
//footer_menu()
//get_new_activity()
//get_new_activityByid()
/**
 * Twenty Nineteen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
error_reporting(E_ERROR | E_PARSE);

if ( ! function_exists( 'twentynineteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function twentynineteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Nineteen, use a find and replace
		 * to change 'twentynineteen' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'twentynineteen', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primary', 'twentynineteen' ),
				'footer' => __( 'Footer Menu', 'twentynineteen' ),
				'social' => __( 'Social Links Menu', 'twentynineteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'script',
				'style',
				'navigation-widgets',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		// add_editor_style( 'style-editor.css' );

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'twentynineteen' ),
					'shortName' => __( 'S', 'twentynineteen' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'twentynineteen' ),
					'shortName' => __( 'M', 'twentynineteen' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'twentynineteen' ),
					'shortName' => __( 'L', 'twentynineteen' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'twentynineteen' ),
					'shortName' => __( 'XL', 'twentynineteen' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);

		// Editor color palette.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => 'default' === get_theme_mod( 'primary_color' ) ? __( 'Blue', 'twentynineteen' ) : null,
					'slug'  => 'primary',
					'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 33 ),
				),
				array(
					'name'  => 'default' === get_theme_mod( 'primary_color' ) ? __( 'Dark Blue', 'twentynineteen' ) : null,
					'slug'  => 'secondary',
					'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 23 ),
				),
				array(
					'name'  => __( 'Dark Gray', 'twentynineteen' ),
					'slug'  => 'dark-gray',
					'color' => '#111',
				),
				array(
					'name'  => __( 'Light Gray', 'twentynineteen' ),
					'slug'  => 'light-gray',
					'color' => '#767676',
				),
				array(
					'name'  => __( 'White', 'twentynineteen' ),
					'slug'  => 'white',
					'color' => '#FFF',
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Add support for custom line height.
		add_theme_support( 'custom-line-height' );
	}
endif;
add_action( 'after_setup_theme', 'twentynineteen_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentynineteen_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Footer', 'twentynineteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'twentynineteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'twentynineteen_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Nineteen 2.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function twentynineteen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf(
		'<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Post title. */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
// add_filter( 'excerpt_more', 'twentynineteen_excerpt_more' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function twentynineteen_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'twentynineteen_content_width', 640 );
}
// add_action( 'after_setup_theme', 'twentynineteen_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function twentynineteen_scripts() {
	wp_enqueue_style( 'twentynineteen-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );

	wp_style_add_data( 'twentynineteen-style', 'rtl', 'replace' );

	if ( has_nav_menu( 'menu-1' ) ) {
		wp_enqueue_script( 'twentynineteen-priority-menu', get_theme_file_uri( '/js/priority-menu.js' ), array(), '20181214', true );
		wp_enqueue_script( 'twentynineteen-touch-navigation', get_theme_file_uri( '/js/touch-keyboard-navigation.js' ), array(), '20181231', true );
	}

	wp_enqueue_style( 'twentynineteen-print-style', get_template_directory_uri() . '/print.css', array(), wp_get_theme()->get( 'Version' ), 'print' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
// add_action( 'wp_enqueue_scripts', 'twentynineteen_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentynineteen_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'twentynineteen_skip_link_focus_fix' );

/**
 * Enqueue supplemental block editor styles.
 */
function twentynineteen_editor_customizer_styles() {

	wp_enqueue_style( 'twentynineteen-editor-customizer-styles', get_theme_file_uri( '/style-editor-customizer.css' ), false, '1.1', 'all' );

	if ( 'custom' === get_theme_mod( 'primary_color' ) ) {
		// Include color patterns.
		require_once get_parent_theme_file_path( '/inc/color-patterns.php' );
		wp_add_inline_style( 'twentynineteen-editor-customizer-styles', twentynineteen_custom_colors_css() );
	}
}
add_action( 'enqueue_block_editor_assets', 'twentynineteen_editor_customizer_styles' );

/**
 * Display custom color CSS in customizer and on frontend.
 */
function twentynineteen_colors_css_wrap() {

	// Only include custom colors in customizer or frontend.
	if ( ( ! is_customize_preview() && 'default' === get_theme_mod( 'primary_color', 'default' ) ) || is_admin() ) {
		return;
	}

	require_once get_parent_theme_file_path( '/inc/color-patterns.php' );

	$primary_color = 199;
	if ( 'default' !== get_theme_mod( 'primary_color', 'default' ) ) {
		$primary_color = get_theme_mod( 'primary_color_hue', 199 );
	}
	?>

	<style type="text/css" id="custom-theme-colors" <?php echo is_customize_preview() ? 'data-hue="' . absint( $primary_color ) . '"' : ''; ?>>
		<?php echo twentynineteen_custom_colors_css(); ?>
	</style>
	<?php
}
// add_action( 'wp_head', 'twentynineteen_colors_css_wrap' );

function home_page(){
	$data = [];
	$data['banner'] = '';
	$open_activety_1 = get_field('open_activety_1',6); //check open activity 1
	$open_activety_2 = get_field('open_activety_2',6); //check open activity 1
	if($open_activety_1 == 'open'){ //activity 1 
		//100 calculated
		// $count_data_= wp_count_posts( 'data_' )->publish;
		$count_data_ = 	get_field('count',21);
		$status_count = '3'; // ถ้าครบ 100 คน
		if(empty($count_data_)){
			$count_data_= 0;
		}
		if($count_data_ < 100){
			$count_data_ = 100 - $count_data_;
			$status_count = '1';
		}elseif($count_data_ < 200){
			$status_count = '2';
		}
		if($status_count == '1'){ // banner 100 คนแรก
			$data['banner'] = get_field('image_activity_1_before100',6);
			$data['banner_mobile'] = get_field('image_activity_1_before100_mobile',6);
			$data['text_activity_'] = get_field('text_activity_1_before100',6);
		}
		if($status_count == '2'){ // banner ครบ 100 คนแรก
			$data['banner'] = get_field('image_activity_1_after100',6);
			$data['banner_mobile'] = get_field('image_activity_1_after100_mobile',6);
			$data['text_activity_'] = get_field('text_activity_1_after100',6);
			
		}
	}elseif($open_activety_2 == 'open'){
		$data['banner'] = get_field('image_activity_2',6);
		$data['banner_mobile'] = get_field('image_activity_2_mobile',6);
		$data['text_activity_'] = get_field('text_activity_2',6);
		$count_data_ = 201;
		$status_count = '3';
	}else{
		$count_data_ = 0;
		$status_count = '3';
	}

	$data['count'] = $count_data_;
	$data['status_count'] = $status_count;
	$data['status_open_activity_1'] = $open_activety_1;
	$data['status_open_activity_2'] = $open_activety_2;
	$data['youtube'] = get_field('youtube',6);
	
	// echo "<pre>"; var_dump($data); die;
	return $data;
}

function thankyou_page(){
	if(!empty($_POST['paymentmethod'])){
		$paymentmethod = $_POST['paymentmethod'];
	}else{
		$paymentmethod = 'cash';
	}
	// echo $paymentmethod ; die;
	$data = [];
	$data['banner'] = '';
	$open_activety_1 = get_field('open_activety_1',6); //check open activity 1
	$open_activety_2 = get_field('open_activety_2',6); //check open activity 1
	if($open_activety_1 == 'open'){ //activity 1 
		//100 calculated
		// $count_data_= wp_count_posts( 'data_' )->publish;
		$count_data_ = 	get_field('count',21);
		$status_count = '3'; // ถ้าครบ 100 คน
		if(empty($count_data_)){
			$count_data_= 0;
		}
		if($count_data_ < 100){
			$count_data_ = 100 - $count_data_;
			$status_count = '1';
		}elseif($count_data_ < 200){
			$status_count = '2';
		}else{
			$status_count = '3';
		}
		if($status_count == '1'){ // banner 100 คนแรก
			if($paymentmethod == "loan"){
				// _mobile
				$data['banner'] = get_field('image_thankyou_activity_1_before100',6);
				$data['banner_mobile'] = get_field('image_thankyou_activity_1_before100_mobile',6);
			}else{
				$data['banner'] = get_field('image_thankyou_activity_1_before100_cash',6);
				$data['banner_mobile'] = get_field('image_thankyou_activity_1_before100_cash_mobile',6);
			}
			$data['text_activity_'] = get_field('text_activity_1_before100_thankyou',6);
		}
		if($status_count == '2'){ // banner ครบ 100 คนแรก
			$data['banner'] = get_field('image_thankyou_activity_1_after100',6);
			$data['banner_mobile'] = get_field('image_thankyou_activity_1_after100_mobile',6);
			$data['text_activity_'] = get_field('text_activity_1_after100_thankyou',6);
		}
	}elseif($open_activety_2 == 'open'){
		$data['banner'] = get_field('image_thankyou_activity_2',6);
		$data['banner_mobile'] = get_field('image_thankyou_activity_2_mobile',6);
		$data['text_activity_'] = get_field('text_activity_3',6);
		$count_data_ = 0;
		$status_count = 3;
	}else{
		$count_data_ = 0;
		$status_count = 3;
	}

	$data['count'] = $count_data_;
	$data['status_count'] = $status_count;
	$data['status_open_activity_1'] = $open_activety_1;
	$data['status_open_activity_2'] = $open_activety_2;
	$data['youtube'] = get_field('youtube',6);
	
	// echo "<pre>"; var_dump($data); die;
	return $data;
}

function get_databy_personalid($personalid){
	$data = [];
	if(!empty($personalid)){
		$id =1;
		$args = [
			'post_type' => 'data_', //restaurant
			'post_status'      => 'publish',
			'posts_per_page' => 1,
			'meta_query' => 	[
				// 'relation' => 'OR',
				[
					'key'     => 'personal_id',
					'value'   => $personalid,
					'compare' => '='
				],
			],
		];
		$wp_query = new WP_Query($args);
		// echo "<pre>";var_dump($wp_query); 
		if(count($wp_query->posts)>=1){
			foreach($wp_query->posts as $key => $value){
				$id = $value->ID;
			}
		}
		$data['id'] =$id;
		$data['address'] = get_field('address',$id);
		$data['province'] = get_field('province',$id);
		$data['fullname'] = get_field('fullname',$id);
		$data['amphures'] = get_field('amphures',$id);
		$data['district'] = get_field('district',$id);
		$data['zip_code'] = get_field('zip_code',$id);
		$data['use_area'] = get_field('use_area',$id);
		$data['email'] = get_field('email',$id);
		if(is_array($data['use_area'])){
			if(!empty($data['use_area'][0])){
				$data['use_area'] = $data['use_area'][0];
			}
		}
		
		$data['use_province'] = get_field('use_province',$id);
		$data['use_amphures'] = get_field('use_amphures',$id);
		$data['models_car'] = get_field('models_car',$id);
		$data['payment_methond'] = get_field('payment_methond',$id);
		$data['other_payment_methond_text'] = get_field('other_payment_methond_text',$id);
		if(is_array($data['payment_methond'])){
			if(!empty($data['payment_methond'][0])){
				$data['payment_methond'] = 	$data['payment_methond'][0];
			}
		}
		$data['agent_'] = get_field('agent_',$id);
	}
	// echo "<pre>"; var_dump($data); die;
	return $data;
}

function get_agent_data(){
	$data = [];
	$username = $_SESSION["username_codeid"];
	$id = $_SESSION["codeid"];
	// echo "<pre>"; var_dump($_POST); die;
	$args = [
		'post_type' => 'data_', //restaurant
		'post_status'      => 'publish',
		'posts_per_page' => -1,
		'meta_query' => 	[
			// 'relation' => 'OR',
			[
				'key'     => 'user_agent_',
				'value'   => $username,
				'compare' => '='
			],
		],
	];
	$wp_query = new WP_Query($args);
	// echo "<pre>";var_dump($wp_query);
	if(count($wp_query->posts)>=1){
		foreach($wp_query->posts as $key => $value){
			$id = $value->ID;

			$arr_ = [
				'id' => $id,
				'fullname' => get_field('fullname',$id),
				'phone' => get_field('phone',$id),
				'personal_id' => get_field('personal_id',$id),
				'date' => thai_date_long_(get_the_date('Y-m-d', $id)),
			];
			
			array_push($data,$arr_);
		}
	}
	// throw new Exception("Error data!!");
	return $data;
}

function edit_ajax_register(){
	$data = [];
	// echo "<pre>"; var_dump($_POST); die;
	if (!empty($_POST) && isset($_POST)) {
		try {

			if(($_POST["method"] == 2) && (empty($_SESSION["codeid"]) || empty($_SESSION["username_codeid"]) )){
				$data = ['MSG' => 'เกิดข้อผิดพลาดกรุณา login ใหม่อีกครั้ง', "Status" => 'error_login' ];
			}else{
				if(!empty($_POST["personalid"])){
					$args = [
						'post_type' => 'data_', //restaurant
						'post_status'      => 'publish',
						'posts_per_page' => -1,
						'meta_query' => 	[
							// 'relation' => 'OR',
							[
								'key'     => 'personal_id',
								'value'   => $_POST["personalid"],
								'compare' => '='
							],
						],
					];
					$wp_query = new WP_Query($args);
					// echo "<pre>";var_dump($wp_query);
					if(count($wp_query->posts)>=1){
						foreach($wp_query->posts as $key => $value){
							$check_id_bypersonal_id = $value->ID;
						}
					}
				
					if (!empty($_POST['codeid']) && isset($_POST['codeid'])) {
						if($check_id_bypersonal_id == $_POST['codeid']){
							$post_id = $_POST['codeid'];
							$codetitle = get_the_title($post_id);
							update_post_meta($post_id, "fullname", $_POST["fullname"]);
							// update_post_meta($post_id, "phone", $_POST["phone"]);
							// update_post_meta($post_id, "personal_id", $_POST["personalid"]);
							update_post_meta($post_id, "province", $_POST["provinces"]);
							update_post_meta($post_id, "amphures", $_POST["amphures"]);
							update_post_meta($post_id, "district", $_POST["districts"]);
							update_post_meta($post_id, "zip_code", $_POST["zip_code"]);
							update_post_meta($post_id, "email", $_POST["email"]);
							update_post_meta($post_id, "address", $_POST["address"]);
	
							// if(($_POST["method"] ==2) && !empty($_SESSION["codeid"]) && !empty($_SESSION["username_codeid"]) ){
							// 	update_post_meta($post_id, "agent_", get_the_title($_SESSION["codeid"]));
							// 	update_post_meta($post_id, "user_agent_", $_POST["username_codeid"]);
							// }
							
							update_post_meta($post_id, "use_area", [$_POST["use_area"]]);
							update_post_meta($post_id, "use_province", $_POST["provinces_otheraddress"]);
							update_post_meta($post_id, "use_amphures", $_POST["amphures_otheraddress"]);	
							update_post_meta($post_id, "models_car", $_POST["models_car"]);
							update_post_meta($post_id, "payment_methond", [$_POST["paymentmethod"]]);
							// update_post_meta($post_id, "loan", $_POST["use_amphures"]);
							update_post_meta($post_id, "other_payment_methond_text", $_POST["other_payment_methond_text"]);
							// update_post_meta($post_id, "pdpa", 'allow');
							if(!empty($_POST["models_car"])){
								$cm = $_POST["models_car"];
								if($cm == '1'){
									$cm = 'M7508';
								}elseif($cm == 2){
									$cm = 'M8808';
						
								}else{
									$cm = 'M9808';
								}
							}else{
								$cm = 'M7508';
							}
							
							if(!empty($_POST["phone"])){
								$txt = get_field('smstext_4',6);
								$txt= str_replace("[C1]",$cm,$txt); 
								$txt = str_replace("[C2]",$codetitle,$txt); 
								// $txt = str_replace("[COUNT]",$count__,$txt); 
								// $txt = "ท่านได้สั่งจองแทรกเตอร์คูโบต้ารุ่น ".$cm ." เรียบร้อยแล้ว หมายเลขลงทะเบียนของคุณคือ ".$codetitle;
								send_sms($_POST["phone"],$txt);
							}
							
							$data = ['MSG' => 'Inseart Success!!', "Status" => 'OK' , 'codetitle'=>$codetitle];
						}else{
							$data = ['MSG' => 'CodeID Error!!', "Status" => 'Error'];
						}
					}else{
						$data = ['MSG' => 'CodeID Error!!', "Status" => 'Error'];
					}

				}else{
					$data = ['MSG' => 'Datas Error!!', "Status" => 'Error'];
				}


			}
		} catch (Exception $e) {
			$data = ['MSG' => 'Datas Error!!', "Status" => 'Error'];
		}
	}else{
		$data = ['MSG' => 'Please Correct Form in right way.', "Status" => 'Error'];
	}
	echo wp_json_encode($data);
	exit();
}
add_action('wp_ajax_edit-ajax-register', 'edit_ajax_register');
add_action('wp_ajax_nopriv_edit-ajax-register', 'edit_ajax_register');

function save_ajax_register(){
	$data = [];
	if (!empty($_POST) && isset($_POST)) {
		try {
			if(($_POST["method"] == 2) && (empty($_SESSION["codeid"]) || empty($_SESSION["username_codeid"])) ){
				$data = ['MSG' => 'เกิดข้อผิดพลาดกรุณา login ใหม่อีกครั้ง', "Status" => 'error_login' ];
			}else{
				$input = rand(1, 999999);
				$input = str_pad($input, 6, "0", STR_PAD_LEFT);
				$codetitle =  "KMS".$input;
				// var_dump($input);
				// var_dump($_POST); die;
				/// this is insert.
				date_default_timezone_set("Asia/Bangkok");
				$postadd = array("post_type" => "data_", "post_title" => $codetitle , "post_status" => "publish");
				$post_id = wp_insert_post($postadd);  //this is create posttype
				if (intval($post_id) > 0) {
					update_post_meta($post_id, "fullname", $_POST["fullname"]);
					update_post_meta($post_id, "phone", $_POST["phone"]);
					update_post_meta($post_id, "personal_id", $_POST["personalid"]);
					update_post_meta($post_id, "province", $_POST["provinces"]);
					update_post_meta($post_id, "amphures", $_POST["amphures"]);
					update_post_meta($post_id, "district", $_POST["districts"]);
					update_post_meta($post_id, "zip_code", $_POST["zip_code"]);
					update_post_meta($post_id, "email", $_POST["email"]);
					update_post_meta($post_id, "address", $_POST["address"]);
					update_post_meta($post_id, "contact_back",['pending']);

					//insert type for check export
					$open_activety_1 = get_field('open_activety_1',6); //check open activity 1
					$open_activety_2 = get_field('open_activety_2',6); //check open activity 1
					if($open_activety_1 == 'open'){ //activity 1 
						//100 calculated
						// $count_data_= wp_count_posts( 'data_' )->publish;
					
						$count_data_ = 	get_field('count',21);
						$status_count = 3; // ถ้าครบ 100 คน
						if(empty($count_data_)){
							$count_data_= 0;
						}
						if($count_data_ <= 100){
							$count_data_ = 100 - $count_data_;
							$status_count = 1;
						}elseif($count_data_ <= 200){
							$status_count = 2;
						}

					

						
						update_post_meta($post_id, "round_",$status_count);

						

					}elseif($open_activety_2 == 'open'){
						update_post_meta($post_id, "round_",3);
					}else{
						update_post_meta($post_id, "round_",3);
					}
					
					
					//update counts_number
					$counts_number = 	get_field('count',21);
					if(empty($counts_number )){
						$counts_number = 0;
					}
					
					update_post_meta(21, "count", ++$counts_number);
					update_post_meta($post_id, "counts_number", $counts_number);
					if($status_count == 1){
						$txt = get_field('smstext_1',6);
						$count__ = $counts_number;
					}elseif($status_count == 2){
						$txt = get_field('smstext_2',6);
						$count__ = $counts_number - 100;
					}else{
						$txt = get_field('smstext_3',6);
						$count__ = 0;
					}

					
					if(($_POST["method"] ==2) && !empty($_SESSION["codeid"]) && !empty($_SESSION["username_codeid"]) ){
						update_post_meta($post_id, "agent_", get_the_title($_SESSION["codeid"]));
						update_post_meta($post_id, "user_agent_", $_SESSION["username_codeid"]);
					}
					
					update_post_meta($post_id, "use_area", [$_POST["use_area"]]);
					update_post_meta($post_id, "use_province", $_POST["provinces_otheraddress"]);
					update_post_meta($post_id, "use_amphures", $_POST["amphures_otheraddress"]);	
					update_post_meta($post_id, "models_car", $_POST["models_car"]);
					update_post_meta($post_id, "payment_methond", [$_POST["paymentmethod"]]);
					// update_post_meta($post_id, "loan", $_POST["use_amphures"]);
					update_post_meta($post_id, "other_payment_methond_text", $_POST["other_payment_methond_text"]);
					// update_post_meta($post_id, "pdpa", 'allow');
					if(!empty($_POST["models_car"])){
						$cm = $_POST["models_car"];
						if($cm == '1'){
							$cm = 'M7508';
						}elseif($cm == 2){
							$cm = 'M8808';
				
						}else{
							$cm = 'M9808';
						}
					}else{
						$cm = 'M7508';
					}
					
					if(!empty($_POST["phone"])){
						
						$txt= str_replace("[C1]",$cm,$txt); 
						$txt = str_replace("[C2]",$codetitle,$txt); 
						$txt = str_replace("[COUNT]",$count__,$txt); 
						// $txt = "ท่านได้สั่งจองแทรกเตอร์คูโบต้ารุ่น ".$cm ." เรียบร้อยแล้ว หมายเลขลงทะเบียนของคุณคือ ".$codetitle;
						send_sms($_POST["phone"],$txt);
					}
					
					$data = ['MSG' => 'บันทึกข้อมูลแล้ว!!!', "Status" => 'OK' , 'codetitle'=>$codetitle];
				}
			}
		} catch (Exception $e) {
			$data = ['MSG' => 'Datas Error!!', "Status" => 'Error'];
		}
	}else{
		$data = ['MSG' => 'Please Correct Form in right way.', "Status" => 'Error'];
	}
	echo wp_json_encode($data);
	exit();
}
add_action('wp_ajax_save-ajax-register', 'save_ajax_register');
add_action('wp_ajax_nopriv_save-ajax-register', 'save_ajax_register');

function check_ajax_porsenalid(){
	$data = [];
	$id =1;
	if (!empty($_POST['personalid']) && isset($_POST['personalid'])) {
		try {
				
					// echo "<pre>"; var_dump($_POST); die;
					$args = [
						'post_type' => 'data_', //restaurant
						'post_status'      => 'publish',
						'posts_per_page' => 1,
						'meta_query' => 	[
							// 'relation' => 'OR',
							[
								'key'     => 'personal_id',
								'value'   => $_POST['personalid'],
								'compare' => '='
							],
						],
					];
					$wp_query = new WP_Query($args);
					// echo "<pre>";var_dump($wp_query); 
				if($_POST['method'] == 1){ // nomal register
					if(count($wp_query->posts)>=1){
						foreach($wp_query->posts as $key => $value){
							$id = $value->ID;
						}
						if(!empty($id)){
							
							$fullname = get_field('fullname',$id);
							$phone = get_field('phone',$id);
							$agent_ = get_field('agent_',$id);
							$user_agent_ = get_field('user_agent_',$id);
						}
						if(!empty($agent_) && !empty($user_agent_) ){
							$data = ['codeid' =>$id  , 'MSG' => 'หมายเลขบัตรประชาชนนี้ได้ทำการสมัครผ่านทางตัวแทน กรุณาติดต่อตัวแทนของท่าน', "Status" => 'alreadyuse_agent' ,"fullname" =>$fullname ,"phone" => $phone ];
						}else{
							$data = ['codeid' =>$id  , 'MSG' => 'หมายเลขบัตรประชาชนนี้ใช้ไปแล้ว ไปทำการแก้ไข', "Status" => 'edit_normal' ,"fullname" =>$fullname ,"phone" => $phone ];
						}
					}else{
						$data = ['MSG' => 'ไปต่อ otp', "Status" => 'ok'];
					}
					// throw new Exception("Error data!!");
				}else{ //agent register
					if(!empty($_SESSION["username_codeid"]) &&  !empty($_SESSION["codeid"]) ){ //checklogin
						if(count($wp_query->posts)>=1){
							foreach($wp_query->posts as $key => $value){
								$id = $value->ID;
							}
							if(!empty($id)){
								
								$fullname = get_field('fullname',$id);
								$phone = get_field('phone',$id);
								$agent_ = get_field('agent_',$id);
								$user_agent_ = get_field('user_agent_',$id);
							}
							if(!empty($agent_) && !empty($user_agent_) ){ //check agent register
								if($_SESSION["username_codeid"] == $user_agent_){
									$data = ['codeid' =>$id  , 'MSG' => 'หมายเลขบัตรประชาชนของท่าน ได้ทำการลงทะเบียนไว้แล้ว แก้ไขข้อมูลได้ที่หน้าจัดการของท่าน [edit]', "Status" => 'error_alreadyuse_agent' ,"fullname" =>$fullname ,"phone" => $phone ];
								}else{
									$data = ['codeid' =>$id  , 'MSG' => 'หมายเลขบัตรประชาชนนี้ได้ทำการลงทะเบียนรับสิทธิ์กับตัวแทนท่านอื่นแล้ว', "Status" => 'error_edit_agent' ,"fullname" =>$fullname ,"phone" => $phone ];
								}
							}else{ //check normal register
								$data = ['codeid' =>$id  , 'MSG' => 'หมายเลขบัตรประชาชนนี้ผู้ใช้ได้ทำการลงทะเบียนไว้แล้วด้วยตนเอง', "Status" => 'error_edit_normal' ,"fullname" =>$fullname ,"phone" => $phone ];
							}
						}else{ // avaliable
							$data = ['MSG' => 'ไปต่อ otp', "Status" => 'ok'];
						}
					}else{ //session timeout && error
						$data = ['MSG' => 'เกิดข้อผิดพลาดกรุณา login ใหม่อีกครั้ง', "Status" => 'error_login'];
					}
				}
		} catch (Exception $e) {
			$data = ['MSG' => 'Datas Error!!', "Status" => 'Error'];
		}
	}
	echo wp_json_encode($data);
	exit();
}
add_action('wp_ajax_check-ajax-porsenalid', 'check_ajax_porsenalid');
add_action('wp_ajax_nopriv_check-ajax-porsenalid', 'check_ajax_porsenalid');


function check_ajax_userpassword(){
	$data = [];
	$id =1;
	if (!empty($_POST['username']) && isset($_POST['username']) && !empty($_POST['password']) && isset($_POST['password'])) {
		try {
				// echo "<pre>"; var_dump($_POST); die;
				$args = [
					'post_type' => 'users_', //restaurant
					'post_status'      => 'publish',
					'posts_per_page' => 1,
					'meta_query' => 	[
						'relation' => 'AND',
						[
							'key'     => 'username',
							'value'   => $_POST['username'],
							'compare' => '='
						],
						[
							'key'     => 'password',
							'value'   => $_POST['password'],
							'compare' => '='
						],
					],
				];
				$wp_query = new WP_Query($args);
				// echo "<pre>";var_dump($wp_query); 
				if(count($wp_query->posts)>=1){
					foreach($wp_query->posts as $key => $value){
						$id = $value->ID;
					}
					if(!empty($id)){
						$title = get_the_title($id);
						$username = $_POST['username'];
					}
					$_SESSION["username_codeid"] =  $username;
					$_SESSION["codeid"] = $id;
					$data = ['codeid' =>$id  , 'MSG' => 'login', "Status" => 'OK' , "username" => $username ];
				}else{
					$data = ['MSG' => 'Username หรือ รหัสผ่านผิดพลาด', "Status" => 'error'];
				}
				// throw new Exception("Error data!!");
		} catch (Exception $e) {
			$data = ['MSG' => 'Datas Error!!', "Status" => 'Error'];
		}
	}
	echo wp_json_encode($data);
	exit();
}
add_action('wp_ajax_check-ajax-userpassword', 'check_ajax_userpassword');
add_action('wp_ajax_nopriv_check-ajax-userpassword', 'check_ajax_userpassword');


function v_ajax_otp(){
	// SMS Provider
	// User : ydmthailand
	// Password : ydmthailandpass
	// Sender : YDM OTP
	// var_dump($_POST);
	$data= [];
	if (!empty($_POST['phone']) && isset($_POST['phone']) && isset($_POST['ref']) && isset($_POST['transid']) && isset($_POST['otp']) ) {
		try {
			$phone =$_POST['phone'];//Set phone
			$ref =$_POST['ref'];//Set Reference Code
			$transid =$_POST['transid'];//Set Token key
			$otp =$_POST['otp'];//Set OTP Code
			//Set Connection : Select only one URL
			//Set Connection : Select only one URL
			$url="http://v2.arcinnovative.com/partnerConnectOTPVerify.php";//API 1:1 
			$username="ydmthailand"; //Set Username
			$password=md5("ydmthailandpass"); //Set Password (MD5)
			$msisdn= $phone; //Set Phone No
			$Parameter="username=$username&";
			$Parameter.="password=$password&";
			$Parameter.="ref=$ref&";
			$Parameter.="otp=$otp&";
			$Parameter.="transid=$transid&";
			$Parameter.="msisdn=$msisdn";
			$status=curls ($url,$Parameter); //Send 
			$fp = fopen ("./testlog.txt","a");
			fputs ($fp, '$status\r\n------------------------\r\n');
			fclose($fp);
			if(!empty(simplexml_load_string($status))){
				$obj = simplexml_load_string($status);
				if(!empty($obj->STATUS)){
					// var_dump($obj);die;
					$data = [
						'MSG' => 'otp sent!!', 
						'objSTATUS' => $obj->STATUS,
						'objDETAIL' => $obj->DETAIL,
						"Status" => 'ok',
					];
				}else{
					if(!empty($obj->DETAIL)){
						$msg = $obj->DETAIL;
					}else{
						$msg = 'something went wrong!';
					}
					$data = ['MSG' => $msg, "Status" => 'smsError'];
				}
			}else{
				$data = ['MSG' => 'SMS Error!!', "Status" => 'Error'];
			}
		} catch (Exception $e) {
			$data = ['MSG' => 'Datas Error!!', "Status" => 'Error'];
		}
	}
	echo wp_json_encode($data);
	exit();
}
add_action('wp_ajax_v-ajax-otp', 'v_ajax_otp');
add_action('wp_ajax_nopriv_v-ajax-otp', 'v_ajax_otp');

function send_ajax_phoneotp(){
	// SMS Provider
	// User : ydmthailand
	// Password : ydmthailandpass
	// Sender : YDM OTP
	// var_dump($_POST);
	$data= [];
	if (!empty($_POST['phone']) && isset($_POST['phone'])) {
		try {
			$phone =$_POST['phone'];
			//Set Connection : Select only one URL
			$url="http://v2.arcinnovative.com/partnerConnectOTP.php";//API 1:1 
			$username="ydmthailand"; //Set Username
			$password=md5("ydmthailandpass"); //Set Password (MD5)
			$msisdn=$phone; //Set Mobile No.
			$msg='รหัส OTP เพื่อยืนยันตัวตน [OTP] [ รหัสอ้างอิง: [REF] ]';//Set Message
			// $msg='OTP Code is [OTP] ';//Set Message
			$msg=str_replace('%','%25',$msg); //Convert %
			$msg=str_replace('&','%26',$msg); //Convert &
			$msg=str_replace('+','[25]',$msg); //Convert +
			$sender='SIAMKUBOTA'; // YDM OTP
			$sender=str_replace('%','%25',$sender); //Convert %
			$sender=str_replace('&','%26',$sender); //Convert &
			$sender=str_replace('+','[25]',$sender); //Convert +
			$Parameter="sender=$sender&";
			$Parameter.="msisdn=$msisdn&";
			$Parameter.="msg=$msg&";
			$Parameter.="username=$username&";
			$Parameter.="password=$password";
			
			//Set ntype 
			$status=curls ($url,$Parameter); //Send SMS
			$fp = fopen ("./testlog.txt","a");fputs ($fp, '$status\r\n------------------------\r\n');fclose($fp);
			
			if(!empty(simplexml_load_string($status))){
				$obj = simplexml_load_string($status);
				// var_dump($obj);die;
				if(!empty($obj->REF) && !empty($obj->TRANSID)){
					$REF = $obj->REF;
					$TRANSID = $obj->TRANSID;
					
					$data = [
						'MSG' => 'otp sent!!', 
						'ref' => $REF,
						'transid' => $TRANSID,
						"Status" => 'ok',
					];
				}else{
					if(!empty($obj->DETAIL)){
						$msg = $obj->DETAIL;
					}else{
						$msg = 'something went wrong!';
					}
					$data = ['MSG' => $msg, "Status" => 'smsError'];
				}
			}else{
				$data = ['MSG' => 'SMS Error!!', "Status" => 'Error'];
			}
		} catch (Exception $e) {
			$data = ['MSG' => 'Datas Error!!', "Status" => 'Error'];
		}
	}
	echo wp_json_encode($data);
	exit();
}

add_action('wp_ajax_send-ajax-phoneotp', 'send_ajax_phoneotp');
add_action('wp_ajax_nopriv_send-ajax-phoneotp', 'send_ajax_phoneotp');

function get_dataform(){
	$data =[];
	$data['fullname_'] = get_field('fullname_',154);
	$data['email_'] = get_field('email_',154);
	$data['address_'] = get_field('address_',154);
	$data['province_'] = get_field('province__',154);
	$data['use_area_'] = get_field('use_area_',154);
	$data['paymentmethod_'] = get_field('paymentmethod_',154);
	// 154
	return $data;
}

///////////////////////////////////////////// core function ////////////////////////////////
function Redirect($url, $permanent = false)
{
    header('Location: ' . $url, true, $permanent ? 301 : 302);

    exit();
}
function get_provinces(){
	$con= mysqli_connect("localhost","ukubomsuat","UFnuQ8Ler#AXMV","db_kubotamsuat") or die("Error: " . mysqli_error($con));
	mysqli_query($con, "SET NAMES 'utf8' ");
	error_reporting( error_reporting() & ~E_NOTICE );
	date_default_timezone_set('Asia/Bangkok');

	$sql_provinces = "SELECT * FROM provinces where 1 ORDER BY name_th ASC";
    $query = mysqli_query($con, $sql_provinces);
	return $query;
}
function thai_date_long_($time='',$mon=''){
	// $dateData="2013-12-19";
	$month_TH_brev = [null,"มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
	if(!empty($mon)){
		// $mon++;
		$month = $month_TH_brev[$mon];
	}else{
		// $time="2013-12-19";
		$time = strtotime($time);
		$month = $month_TH_brev[date("n",$time)];
	}
	
	$data_r = date('j',$time);
	$data_r .= ' ' . $month_TH_brev[date("n",$time)];
	$data_r .= ' ' . substr((date('Y',$time)+543),-2);
	return $data_r;
}
function pagegination_custom($current_page = 1, $post_type = "null", $maximum_page = 15, $data_ = [])
{
	//need current_page(select page) $post_type(no need) $maximum_page(requires) $data_($requires)
	/////////////////////
	$data = [];
	$max = 0;
	$min = 0;
	$count_page = 0;
	$next = 0;
	$prev = 0;
	if ($current_page  < 0) {
		$current_page = 0;
	}
	$data['data'] = [];
	$data['prev'] = 0;
	$data['next'] = 0;
	$data['all_page'] = 0;
	/////////////////////
	if ($post_type != 'null' && !empty($data_) && is_numeric($current_page)) {
		/////////////////////
		$count_page = count($data_) / $maximum_page;
		// $count_page = 60/$maximum_page; 
		$current_page = intval($current_page);
		$p = 0;
		if (is_float($count_page)) {
			$p++;
		}
		$count_page = intval($count_page) + $p;
		if ($current_page != 0) {
			$prev = $current_page - 1;
			if ($current_page > $count_page) {
				$prev = 0;
			}
		}
		if ($current_page >= $count_page) {
			$next = 0;
		} else {
			$next = $current_page + 1;
			if ($current_page == 0) {
				$next = 0;
			}
		}
		$data['prev'] = $prev;
		$data['next'] = $next;
		$data['all_page'] =	$count_page;
		$data['current_page'] = $current_page;
		// $next,$prev,$count_page,$current_page(done)
		if (($next != 0 || $prev != 0) || ($count_page == 1)) {
			$max = ($current_page * $maximum_page) - 1;
			$min = ($current_page * $maximum_page) - $maximum_page;
			for ($i = $min; $i <= $max; $i++) {
				if (!empty($data_[$i])) {
					array_push($data['data'], $data_[$i]);
				}
			}
			unset($data_);
		}
	}
	// $max,$min
	// var_dump($data);die;
	return $data;
}
function translate_monthto_thai($favcolor="January"){
	switch ($favcolor) {
		case "January":
			return "มกราคม";
			break;
		case "February":
			return "กุมภาพันธ์";
			break;
		case "March":
			return "มีนาคม";
			break;
		case "April":
			return "เมษายน";
			break;
		case "May":
			return "พฤษภาคม";
			break;	
		case "June":
			return "มิถุนายน";
			break;	
		case "July":
			return "กรกฎาคม";
			break;	
		case "August":
			return "สิงหาคม";
			break;	
		case "September":
			return "กันยายน";
			break;	
		case "October":
			return "ตุลาคม";
			break;	
		case "November":
			return "พฤศจิกายน";
			break;	
		case "December":
			return "ธันวาคม";
			break;	
		default:
			return "มกราคม";
	  }
}
function admin_post_hack()
{
	$getpost =  get_post();
	$post_type_id = [];
	$cpt = [];
	if (!empty($getpost)) {
		$post_type_id =  $getpost->ID;
		$cpt =  $getpost->post_type;
		$post_type_name =  $getpost->post_type;
		//echo  'post_type_name ='. $post_type_name;
	
		// $cpt  = $_GET['post_type'];
	}

	/* Hide Menu */
	echo '<style>#wp-admin-bar-new-content,#wp-admin-bar-all-in-one-seo-pack,#wp-admin-bar-wp-logo,#wp-admin-bar-comments,#footer-thankyou,#footer-upgrade{display:none!important;}</style>';
	/* Hide Dashboard*/
	// echo '<style>#wp-admin-bar-litespeed-menu,#welcome-panel,#dashboard-widgets-wrap,#footer-left,#wp-admin-bar-new_draft{display:none!important;}</style>';
	/* Hide Right Sidebar */
	//echo '<style>#news-categorydiv,#product-basediv,#product-typediv{display:none!important;}</style>';
	/* Hide comment */
	echo '<style>#commentstatusdiv,#commentsdiv{display:none!important;}</style>';
	/* Hide wmpl */
	//echo '<style>#icl_div_config{display:none!important;}</style>';
	/* Hide new post */
	if (in_array($post_type_id, array("71",'197', "67", "73","69","35","283","301","285","155","49","75","144"))) {
		// echo '<style>.wrap a.page-title-action{display:none!important;}</style>';
		echo '<script>jQuery(document).ready(function($){
				$("#title").attr("disabled", "disabled");
			});</script>';
	}


	//hide button Add New in listing page
	if (in_array($cpt, array("page", "recruit", "booking", "contactus", "detail_treatments"))) {
		echo '<style>.wrap a.page-title-action{display:none!important;}</style>';
	}

	//hide zone action listing page for title column
	if (in_array($cpt, array("recruit", "subscribe_", "booking", "contactus", "detail_treatments"))) {
		echo '<style>#the-list div.row-actions{display:none!important;}</style>';
	}
	if (in_array($cpt, array("detail_treatments"))) {
		echo '<script>jQuery(document).ready(function($){
					$("#title").attr("disabled", "disabled");
				});</script>';
	}
	//hide button Add New in edit item page
	// "page",
	if (in_array($cpt, array("page", "recruit", "subscribe_", "booking", "detail_treatments"))) {
		echo '<style>.wrap a.page-title-action{display:none!important;}</style>';
	}

}
add_action('admin_head', 'admin_post_hack');
///////////////////////////////////////////// end core function ////////////////////////////////

///////////////////////////////////////////// start example function ////////////////////////////////
function get_career(){
 
	$data['contents'] = [];
	$args = [
		'post_type' => 'career',
		'post_status'=>'publish',
		// 'p'=> 49,
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
	];
	$query_ = new WP_Query($args);
	if(count($query_->posts))
	{
		foreach ($query_->posts as $q) {
			$arr_ =  [
				'id' => $q->ID,
				'title' => get_the_title($q->ID),
				'recruit_number' => get_field('recruit_number',$q->ID),
				'yearofexperience' => get_field('yearofexperience',$q->ID),
				'detail' => get_field('detail',$q->ID),
			];
			array_push($data['contents'],$arr_);
		}
		wp_reset_query();
	}
	$data['banner'] = [];
	$args = [
		'post_type' => 'page',
		'post_status'=>'publish',
		'p'=> 197,
		'posts_per_page' => 1,
	];
	$query_ = new WP_Query($args);
	if(count($query_->posts))
	{
		foreach ($query_->posts as $q) {
			$arr_ =  [
				'id' => $q->ID,
				'title' => get_the_title($q->ID),
				'bannerName' => get_field('bannerName',$q->ID),
				'detailBanner' => get_field('detailBanner',$q->ID),
				'image' => get_field('image',$q->ID),
			];
			array_push($data['banner'],$arr_);
		}
		wp_reset_query();
	}
	// echo "<pre>"; var_dump($data); die;
	return $data;
}//end career
function save_ajax_contactus(){
	$data = [];
	if (!empty($_POST) && isset($_POST)) {
		try {
			/// this is insert.
			date_default_timezone_set("Asia/Bangkok");
			$postadd = array("post_type" => "contactus", "post_title" => $_POST["fullname"], "post_status" => "publish");
			$post_id = wp_insert_post($postadd);  //this is create posttype
			if (intval($post_id) > 0) {
				update_post_meta($post_id, "fullname", $_POST["fullname"]);           //$_POST["name_contact"]); //this is insert or update contactform
				update_post_meta($post_id, "call", $_POST["call"]);      //$_POST["email_contact"]);
				update_post_meta($post_id, "email", $_POST["email"]);
				update_post_meta($post_id, "subject", $_POST["subject"]);
				update_post_meta($post_id, "message", $_POST["detail"]);
				//upload image and file
				/*	// upload photo
					$upload_photo = wp_upload_bits($_FILES['photo']['name'], null, file_get_contents($_FILES['photo']['tmp_name']));
					$wp_photo_filetype = wp_check_filetype( basename($upload_photo['file'] ), null );
					$wp_photo_upload_dir = wp_upload_dir();

					$photo_attachment = array(
						'guid' => $wp_photo_upload_dir['baseurl'] . _wp_relative_upload_path( $upload_photo['file'] ),
						'post_mime_type' => $wp_photo_filetype['type'],
						'post_title' => preg_replace('/\.[^.]+$/', '', basename( $upload_photo['file'])),
						'post_content' => '',
						'post_status' => 'inherit'
					);

					$photo_attach_id = wp_insert_attachment($photo_attachment, $upload_photo['file'], $post_id);
					update_field('photo', $photo_attach_id , $post_id);

					// upload resume
					$upload_resume = wp_upload_bits($_FILES['file_resume']['name'], null, file_get_contents($_FILES['file_resume']['tmp_name']));
					update_field('file_resume', $upload_resume['url'] , $post_id);
				}*/
				$data = ['MSG' => 'Inseart Success!!', "Status" => 'OK'];
			} else {
				throw new Exception("Error data!!");
			}
		} catch (Exception $e) {
			$data = ['MSG' => 'Datas Error!!', "Status" => 'Error'];
		}
	}
	echo wp_json_encode($data);
	exit();
}
add_action('wp_ajax_save-ajax-contactus', 'save_ajax_contactus');
add_action('wp_ajax_nopriv_save-ajax-contactus', 'save_ajax_contactus');

function example(){

	$contact_email = [];
	$args = array(
		'post_type' => 'contact_email',
		'post_status'=>'publish',
		'posts_per_page' => -1,
	); 
	$query_ = new WP_Query($args);
	if(count($query_->posts))
	{
		foreach ($query_->posts as $q) {
			$arr_ =  [
				'id' => $q->ID,
				'display' => get_the_title($q->ID),
			];
			array_push($contact_email,$arr_);
		}
	}
	wp_reset_query();
}
///////////////////////////////////////////// end example function ////////////////////////////////
function curls ($url,$param) {

	// SMS Provider
	// User : ydmthailand
	// Password : ydmthailandpass
	// Sender : YDM OTP

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,"$url");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,"$param");
	//  echo "<pre>";  var_dump($url); var_dump($param);die;
	$Status=curl_exec ($ch);
	curl_close ($ch);
	return $Status;
}
function get_otp($phone){

		// SMS Provider
		// User : ydmthailand
		// Password : ydmthailandpass
		// Sender : YDM OTP

		// echo $phone; die;
		$data= [];
		//Set Connection : Select only one URL
		$url="http://v2.arcinnovative.com/partnerConnectOTP.php";//API 1:1 
		$username="ydmthailand"; //Set Username
		$password=md5("ydmthailandpass"); //Set Password (MD5)
		$msisdn=$phone; //Set Mobile No.
		$msg='รหัส OTP เพื่อยืนยันตัวตน [OTP] [ รหัสอ้างอิง: [REF] ]';//Set Message
		$msg=str_replace('%','%25',$msg); //Convert %
		$msg=str_replace('&','%26',$msg); //Convert &
		$msg=str_replace('+','[25]',$msg); //Convert +
		$sender='SIAMKUBOTA'; // YDM OTP
		$sender=str_replace('%','%25',$sender); //Convert %
		$sender=str_replace('&','%26',$sender); //Convert &
		$sender=str_replace('+','[25]',$sender); //Convert +
		$Parameter="sender=$sender&";
		$Parameter.="msisdn=$msisdn&";
		$Parameter.="msg=$msg&";
		$Parameter.="username=$username&";
		$Parameter.="password=$password";
		//Set ntype 
		$status=curls ($url,$Parameter); //Send SMS
		$fp = fopen ("./testlog.txt","a");fputs ($fp, '$status\r\n------------------------\r\n');fclose($fp);
	return $status;
}

function verify_otp($otp , $ref ,$phone,$transid){
	//Set Connection : Select only one URL
	$url="http://v2.arcinnovative.com/partnerConnectOTPVerify.php";//API 1:1 
	$username="ydmthailand"; //Set Username
	$password=md5("ydmthailandpass"); //Set Password (MD5)
	$ref=$ref; //Set Reference Code
	$otp=$otp; //Set OTP Code
	$transid=$transid; //Set Token key
	$msisdn= $phone; //Set Phone No
	$Parameter="username=$username&";
	$Parameter.="password=$password&";
	$Parameter.="ref=$ref&";
	$Parameter.="otp=$otp&";
	$Parameter.="transid=$transid&";
	$Parameter.="msisdn=$msisdn";
	$status=curls ($url,$Parameter); //Send 
	$fp = fopen ("./testlog.txt","a");
	fputs ($fp, '$status\r\n------------------------\r\n');
	fclose($fp);
	return $status;
}

function send_sms($phone , $msg){
	//Set Connection : Select only one URL
	$url="http://v2.arcinnovative.com/APIConnect.php"; //API 1:1
	$username="ydmthailand"; //Set Username
	$password=md5("ydmthailandpass"); //Set Password (MD5)
	$msisdn=$phone; //Set Mobile No.
	// $msg='TEST SMS by Grob system'; //Set Message
	$msg=str_replace('%','%25',$msg); //Convert %
	$msg=str_replace('&','%26',$msg); //Convert &
	$msg=str_replace('+','[2B]',$msg); //Convert +
	$sender='SIAMKUBOTA'; //Set Sender
	$sender=str_replace('%','%25',$sender); //Convert %
	$sender=str_replace('&','%26',$sender); //Convert &
	$sender=str_replace('+','[2B]',$sender); //Convert +
	$Parameter="";
	$Parameter.="sender=$sender&";
	$Parameter.="msisdn=$msisdn&";
	$Parameter.="msg=$msg&";
	$Parameter.="smstype=sms&"; //Set smstype
	$Parameter.="username=$username&";
	$Parameter.="password=$password&";
	$Parameter.="ntype=in&"; //Set ntype 
	$status=curls ($url,$Parameter); //Send SMS
	$fp = fopen ("./testlog.txt","a");
	fputs ($fp, '$status\r\n------------------------\r\n');fclose($fp);
	return $status;
}

/**
 * SVG Icons class.
 */
require get_template_directory() . '/classes/class-twentynineteen-svg-icons.php';

/**
 * Custom Comment Walker template.
 */
require get_template_directory() . '/classes/class-twentynineteen-walker-comment.php';

/**
 * Common theme functions.
 */
require get_template_directory() . '/inc/helper-functions.php';

/**
 * SVG Icons related functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Block Patterns.
 */
require get_template_directory() . '/inc/block-patterns.php';
