  <?php
    // $data = footer_menu(); 
  ?>   
  <div class="box-loading">
    <div class="inner">
      <div class="loading">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  </div>
  
  <footer id="footer">
    <div class="section-top">
      <div class="container">
        <div class="social">
          <p>ช่องทางออนไลน์</p>
          
          <div class="group-btn">
            <a href="https://www.facebook.com/SiamKubotaClub/" target="_blank"><img src="<?php echo get_template_directory_uri();?>/assets/img/icons/facebook.svg" alt="icon"><span>Siam Kubota Club</span></a>
            <a href="https://www.youtube.com/user/SIAMKUBOTACLUB" target="_blank"><img src="<?php echo get_template_directory_uri();?>/assets/img/icons/youtube.svg" alt="icon"><span>Siam Kubota</span></a>
            <a href="https://bit.ly/3xYGhGN" target="_blank"><img src="<?php echo get_template_directory_uri();?>/assets/img/icons/line.svg" alt="icon"><span>@siamkubota</span></a>
          </div>
        </div>

        <div class="contact">
          <p>สอบถามข้อมูลเพิ่มเติมติดต่อ</p>
          <a href="tel:02-029-1747" class="logo-connect"><img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-connect.svg" alt="Kubota Connect 02-029-1747"></a>
        </div>
      </div>
    </div>

    <div class="section-bottom">
      <div class="container">
        <p>สงวนลิขสิทธิ์ © 2022 บริษัทสยามคูโบต้าคอร์ปอเรชั่น จำกัด</p>
      </div>
    </div>
  </footer>
</body>
</html>