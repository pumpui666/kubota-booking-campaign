<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */
?>
<!doctype html>
<html class="th">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="apple-mobile-web-app-capable" content="yes" />

	<title><?php echo get_bloginfo(); ?></title>
	<meta name="description" content="<?php echo get_bloginfo('description'); ?>">
	<meta name="keywords" content="">

	<!-- Meta Open Graph (FB) -->
	<meta property="og:type" content="website">
	<meta property="og:url" content="<?php site_url(); ?>">
	<meta property="og:title" content="<?php echo get_bloginfo(); ?>">
	<meta property="og:description" content="<?php echo get_bloginfo('description'); ?>">
	<meta property="og:image" content="<?php echo get_template_directory_uri();?>/assets/img/og/OG_kubota.png">
	<meta property="og:site_name" content="<?php echo get_bloginfo(); ?>">

	<!-- Twitter Card -->
	<!-- <meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="Kubato">
	<meta name="twitter:description" content="Kubato">
	<meta name="twitter:url" content="http://url.com"> -->
	<!-- <meta name="twitter:image:src" content="<?php //echo get_template_directory_uri();?>/assets/img/share-img/og-630x630.jpg"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" id="viewportMeta" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/assets/img/share/favicon-192x192.png">
	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri();?>/assets/img/share/favicon-180x180.png">

	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/main.css?version=<?php echo time(); ?>">
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
