<header id="header">
  <div class="container">
    <a href="#" class="hamburger">
      <div></div>
    </a>

    <div class="logo">
    <a href="./"><img src="assets/img/share/logo.png" srcset="assets/img/share/logo@2x.png 2x, assets/img/share/logo@3x.png 3x" alt="" /></a>
    </div>
    
    <div class="box-menu">
      <nav class="main-menu">
        <a href="./" class="active">Home</a>
        <a href="ship-building.php">Ship Building</a>
        <a href="ship-repair.php">Ship Repair</a>
        <a href="about-us.php">About us</a>
        <a href="news-activities.php">News & Activities</a>
        <a href="contact-us.php">Contact us</a>
      </nav>
    </div>

    <a href="#" class="btn-search"></a>
    <div class="box-search">
      <div class="container">
        <form>
          <div class="form-style">
            <input type="text" class="form-input" placeholder="Search entire site">
            <button type="submit" class="btn-submit">Search</button>
          </div>
        </form>
      </div>
    </div>

    <a href="#" class="btn-language" title="EN">EN</a>
    <div class="box-language">
      <a href="#" class="active" title="EN">EN</a>
      <a href="#" title="TH">TH</a>
    </div>
  </div>
</header>