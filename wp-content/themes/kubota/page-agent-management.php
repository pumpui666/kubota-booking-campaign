<?php 
  session_start();
  if(!empty(	$_SESSION["username_codeid"] ) ):
    $data = [];
    $data = get_agent_data();
    
?>

  <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="agent-management-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
      <div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>

      <div class="container">
        <div class="box-content">
          <div class="inner">
            <div class="title-page">
              <h1 class="title">ข้อมูลผู้ลงทะเบียนสั่งจอง<br class="mobile-l">แทรกเตอร์คูโบต้า <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" class="logo-m-series" width="156" h="16"></h1>
            </div>

            <div class="box-bg">
              <p> <?php echo get_the_title($_SESSION["codeid"]); ?></p>
            </div>

            <div class="box-group-top">
              <form class="submit-form-home" method="get">
                <div class="form-style form-full">
                  <div class="form-search">
                    <input type="text" class="form-input autocomplete" id="autocom_" placeholder="ค้นหา">
                    <button type="submit" class="btn-search">
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/search.svg" alt="icon" width="28" height="28">
                    </button>
                  </div>
                </div>
              </form>
              <a href="<?php echo site_url('privacy-policy').'?method=2'; ?>" class="btn">
                <span>
                  สั่งจองเพิ่มเติม
                  <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/add.svg" alt="icon">
                </span>
              </a>
            </div>

            <div class="table-style">
              <table>
                <thead>
                  <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th>บัตรประชาชน</th>
                    <th>เบอร์โทร</th>
                    <th>วันที่ลงทะเบียน</th>
                    <th>แก้ไข</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($data as $key => $value): ?>
                  <tr class="alltable">
                    <!-- <td ><?php //echo get_field('counts_number',$value['id']); ?></td> -->
                    <td class="show_number"><?php echo $key+1; ?> </td>
                    <!-- <td id="<?php echo $key+1; ?>" class="hide_number" style="display:none;">1</td> -->
                    <td class="fullnamedata"><?php echo $value['fullname']; ?></td>
                    <td class="personal_iddata"><?php echo $value['personal_id']; ?></td>
                    <td class="phonedata"><?php echo $value['phone']; ?></td>
                    <td><?php echo $value['date'].""; ?></td>
                    <td><a href="<?php echo site_url('agentedit-register-form')."?codeid=".$value['id']; ?>" target="_blank" class="btn-edit"><img src="<?php echo get_template_directory_uri();?>/assets/img/icons/edit.svg" alt="icon"></a></td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>

            <div class="box-btn">
              <a href="<?php echo site_url('agent-logout'); ?>" class="btn">
                <span>
                  ออกจากระบบ
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">

      $('.btn-submit').click(function(){
        var check= true;
        if($('#personalid').val()=== ""){
          $("#personalid").parent().addClass('error');
          check = false;
        }else{
          $("#personalid").parent().removeClass('error');
        }

        var data = new FormData();

        data.append("personalid", $('#personalid').val());
        data.append("action", 'check-ajax-porsenalid');
        if(check == false){
          return false;
        }
        $('.btn-submit').css('pointer-events','none');
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            // alert('success');
            $('.btn-submit').css('pointer-events','auto');
            // $('input').val('');
            // $('textarea').val('');
            if(data.Status == 'ok'){
              $('#submit_form').submit();
            }else if(data.Status ==  'alreadyuse'){
              $('#submit_form').attr('action', site_url+'/gu-otp');
              $('#fullname').val(data.fullname);
              $('#phone').val(data.phone);
              $('#submit_form').submit();
            }else{
              
            }

          },
          error: function (data) {
            // alert(data);
            console.log("error: "+data);
            // return false;
          }
        }); // close ajax
       
      });

      var options = {
        data: [
          "ทั้งหมด",
          <?php if (!empty($value['fullname'])) :  ?>
            <?php foreach ($data as $key => $value) : ?> 
              <?php if(!empty($value['fullname'])): ?>
                "<?php echo $value['fullname']; ?>",
              <?php endif; ?>
            <?php endforeach; ?>
          <?php else : ?> 
          <?php endif; ?>
          <?php if (!empty($value['personal_id'])) :  ?>
            <?php foreach ($data as $key => $value) : ?> 
              <?php if(!empty($value['personal_id'])): ?>
                "<?php echo $value['personal_id']; ?>",
              <?php endif; ?>
            <?php endforeach; ?>
          <?php else : ?> 
          <?php endif; ?>
          <?php if (!empty($value['phone'])) :  ?>
            <?php foreach ($data as $key => $value) : ?> 
              <?php if(!empty($value['phone'])): ?>
                "<?php echo $value['phone']; ?>",
              <?php endif; ?>
            <?php endforeach; ?>
          <?php else : ?> 
          <?php endif; ?>
			  ],
        list: {
          maxNumberOfElements: 30,
          match: {
            enabled: true
          },
          onClickEvent: function() {
            showLoading();
            $('.btn-search').trigger('click');
            // console.log("on click event");
          }
        }
      };
      $(".autocomplete").easyAutocomplete(options);

      $('.submit-form-home').submit(function() {
        hideLoading();
        var check_ = true;
        var val_form = $('#autocom_').val();
        $('.hide_number').hide();
        // console.log(val_form);
        
        $('.fullnamedata').each(function() {
          var dataName = $(this).html();
          // console.log(dataName);
          if(dataName==val_form){
            check_ = false;
            $(this).parent().show();
            $(this).parent().find('.show_number').html('1');
            // $(this).parent().find('.hide_number').show();
            // console.log(id_number);

          
          }else{
            $(this).parent().hide();
          }
        });

        if(check_){
          $('.personal_iddata').each(function() {
            var dataName = $(this).html();
            // console.log(dataName);
            if(dataName==val_form){
              check_ = false;
              $(this).parent().show();
              $(this).parent().find('.show_number').html('1');
            }else{
              $(this).parent().hide();
            }
          });
        }
      
        if(check_){ 
          $('.phonedata').each(function() {
            var dataName = $(this).html();
            // console.log(dataName);
            if(dataName==val_form){
              check_ = false;
              $(this).parent().show();
              $(this).parent().find('.show_number').html('1');

            }else{
              $(this).parent().hide();
            }
          });
        }

        if('ทั้งหมด' == val_form){
          showLoading();
          location.reload();
        }

        return false;
      });

      $('.table-style').scrollbar();
       // showLoading();
      // hideLoading();
  </script>
  <!-- end javascript this page -->
<?php else: ?>
    <h1> <a href="<?php echo site_url('agent-login'); ?>">  Please Login </a></h1>
    <h1> กรณี Login แล้วข้อมูลไม่แสดง <a href="<?php echo site_url('agent-logout'); ?>">  Logout </a> และทำการ Login ใหม่อีกรอบ</h1>
<?php endif; ?>