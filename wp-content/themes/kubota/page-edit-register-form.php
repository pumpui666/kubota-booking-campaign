<?php 
  session_start();
	if(!empty($_POST['method'])){
		$method = $_POST['method'];
	}else{
		$method = 1;
	}
	if(!empty($_POST['cm'])){
		$cm = $_POST['cm'];
	}else{
    $cm = 1;
	}
 
  if(!empty($_POST['personalid'])){
    $personalid = $_POST['personalid'];
  }else{
    Redirect(site_url(), false);// redirect
  }
  if(!empty($_POST['transid'])){
    $transid = $_POST['transid'];
  }else{
    Redirect(site_url(), false);// redirect
  }

  if(!empty($_POST['otp'])){
    $otp = $_POST['otp'];
  }else{
    Redirect(site_url(), false);// redirect
  }

  if(!empty($_POST['phone'])){
    $phone = $_POST['phone'];
  }else{
    Redirect(site_url(), false);// redirect
  }

  if(!empty($_POST['ref'])){
    $ref = $_POST['ref'];
  }else{
    Redirect(site_url(), false);// redirect
  }


  if(empty($_SESSION["username_codeid"]) || empty($_SESSION["codeid"])){


  }else{
    // Redirect(site_url(), false);// redirect

  }

  $data = [];
  $data['data'] = get_databy_personalid($personalid);
  if(empty( $data['data'])){
    Redirect(site_url(), false);// redirect
    //redirect
  }
  $data['provinces'] = get_provinces();
  $data['dataform'] = get_dataform(); //154


?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="register-page edit-form">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
      <div class="img-bg bg-promotion">
        <?php 
          $type = get_field('round_',$data['data']['id']);
          $payment_methond = get_field('payment_methond',$data['data']['id']);
          if(!empty($payment_methond)){
            if(is_array($payment_methond)){
              $payment_methond = $payment_methond[0];
            }
          }else{
            $payment_methond = 'other';
          }
        
          if($type ==1){
            if($payment_methond == "loan"){
              // _mobile
          
              $data['banner'] = get_field('image_activity_1_before100_edit_1percen',6);
              $data['banner_mobile'] = get_field('image_activity_1_before100_edit_1percen_mobile',6);
            }else{
              $data['banner'] = get_field('image_activity_1_before100_edit',6);
              $data['banner_mobile'] = get_field('image_activity_1_before100_edit_mobile',6);
            }
          }elseif($type ==2 ){
            $data['banner'] = get_field('image_activity_1_after100_edit',6);
            $data['banner_mobile'] = get_field('image_activity_1_after100_edit_mobile',6);
          }else{
            $data['banner'] = get_field('image_activity_2_edit',6);
            $data['banner_mobile'] = get_field('image_activity_2_edit_mobile',6);
          }
        ?>
        <?php if(!empty($data['banner'])): ?>
          <picture>
            <source srcset="<?php echo $data['banner_mobile'];?>" media="(max-width: 479px)" type="image/jpeg">
            <img src="<?php echo  $data['banner'];?>" alt="Background" loading="lazy" width="1920" height="680">
          </picture>
        <?php else: ?>
          <picture>
            <source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-banner-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
            <img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-banner-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
          </picture>
        <?php endif; ?>
          

			</div>
      <?php 
        if(!empty($data['data']['models_car'])){
          $check = $data['data']['models_car'];
        }else{
          $check = '';
        }
        if(!empty($check)){
          $models_txt = $check;
          if($models_txt == '1'){
            $models_txt = 'M7508';
          }elseif($models_txt == 2){
            $models_txt = 'M8808';
      
          }else{
            $models_txt = 'M9808';
          }
        }else{
          $models_txt = 'M7508';
        }
      ?>
      <span class="m_name" qazwsx="<?php echo get_the_title($data['data']['id']); ?>" > </span>
      <div class="container">
        <div class="box-content register-form">
          <div class="inner">
            <div class="form-style form-full disabled-form">
              <form action="<?php echo site_url('thank-you'); ?>" method="post" id="submit_form">
                <input type="hidden" id="register_title" name="register_title" >
                <input type="hidden" id="otp" name="otp"  value="<?php echo $otp; ?>" >
                <input type="hidden" id="transid" name="transid"  value="<?php echo $transid; ?>" >  
                <input type="hidden" id="method" name="method" value="<?php echo $method; ?>">
                <input type="hidden" id="cm" name="cm" value="<?php echo $cm; ?>">
                <input type="hidden" id="codeid" name="codeid" value="<?php echo $data['data']['id']; ?>">
                <input type="hidden" id="ref" name="ref"  value="<?php echo $ref; ?>" >

                <div class="title-page">
                  <h1 class="title">รายละเอียดการสั่งจอง<br class="mobile-l">แทรกเตอร์คูโบต้า <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" class="logo-m-series" width="156" h="16"></h1>
                  <p class="sub-title">ข้อมูลลงทะเบียนจองสิทธิ์รถแทรกเตอร์คูโบต้า <?php echo $models_txt; ?></p>
                </div>

                <div class="box-bg">
                  <p>หมายเลขลงทะเบียนของคุณคือ : <strong><?php echo get_the_title($data['data']['id']); ?></strong></p>
                </div>
                <div class="box-btn-hidden">
                <br><br>
                  <br><br>
                </div>
                <div class="box-btn-download">
                  <a href="#" onclick="loadimg_pomotion()" class="btn btn-downloading">
                    <span>
                      ดาวน์โหลดสิทธิ์
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/download.svg" alt="icon">
                    </span>
                  </a>
                </div>
                
                <div class="box-step box-profile fixed-show">
                  <div class="flex-content">
                    <h2>ข้อมูลส่วนตัว</h2>
                    <a href="#" class="btn btn-edit">
                      <span>
                        แก้ไข
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/edit-btn.svg" alt="icon">
                      </span>
                    </a>
                  </div>
                  <div class="row">
                    <div class="box-input required disabled">
                      <label for="personalid" class="text-label">หมายเลขบัตรประชาชน</label>
                      <input type="text" id="personalid" class="form-input" name="personalid" value="<?php echo $personalid; ?>" disabled>
                      <p class="text-validate">กรุณาระบุ</p>
                    </div>
                    <?php if($data['dataform']['fullname_'] == 'open'):  ?>
                    <div class="box-input required">
                      <label for="fullname" class="text-label">ชื่อ - นามสกุล</label>
                      <input type="text" id="fullname" class="form-input" name="fullname" value="<?php echo $data['data']['fullname']; ?>">
                      <p class="text-validate">กรุณาระบุ</p>
                    </div>
                    <?php endif; ?>
                    <div class="box-input required disabled">
                      <label for="phone" class="text-label">หมายเลขโทรศัพท์มือถือที่ติดต่อได้</label>
                      <input type="tel" id="phone" class="form-input" name="phone" value="<?php echo $phone; ?>">
                      <p class="text-validate" disabled>กรุณาระบุเบอร์โทรศัพท์ให้ถูกต้อง</p>
                    </div>
                    <?php if($data['dataform']['email_'] == 'open'):  ?>
                    <div class="box-input">
                      <label for="email" class="text-label">อีเมล (ถ้ามี)</label>
                      <input type="email" id="email" class="form-input" name="email" value="<?php echo $data['data']['email']; ?>">
                    </div>
                    <?php endif; ?>
                  </div>
                  <div class="row">
                    <?php if($data['dataform']['address_'] == 'open'):  ?>
                    <div class="box-input required w-full">
                      <label for="address" class="text-label">ที่อยู่ (บ้านเลขที่ หมู่ ซอย ถนน)</label>
                      <input type="textarea" id="address" class="form-input" name="address" value="<?php echo $data['data']['address']; ?>">
                      <p class="text-validate">กรุณาระบุ</p>
                    </div>
                    <?php endif; ?>
                    <?php if($data['dataform']['province_'] == 'open'):  ?>
                    <div class="box-input required">
                      <label for="provinces" class="text-label">จังหวัด</label>
                      <select class="form-select" name="Ref_prov_id" id="provinces">
                          <option value=""  disabled>เลือกจังหวัด</option>
                          <?php foreach ($data['provinces'] as $value) { ?>
                            <?php if($value['name_th'] == $data['data']['province']): ?>
                                <option value="<?=$value['id']?>" selected><?=$value['name_th']?></option>
                              <?php else: ?>
                                <option value="<?=$value['id']?>"><?=$value['name_th']?></option>
                              <?php endif; ?>
                          <?php } ?>
                      </select>
                      <p class="text-validate">กรุณาระบุ</p>
                    </div>
                    <div class="box-input required">
                      <label for="amphures" class="text-label">อำเภอ</label>
                      <select class="form-select" name="Ref_dist_id" id="amphures">
                        <option value="" selected disabled>เลือกอำเภอ</option>
                        </select>
                      <p class="text-validate">กรุณาระบุ</p>
                    </div>
                    <div class="box-input required">
                      <label for="districts" class="text-label">ตำบล</label>
                      <select class="form-select" name="Ref_subdist_id" id="districts">
                        <option value="" selected disabled>เลือกตำบล</option>
                        </select>
                      <p class="text-validate">กรุณาระบุ</p>
                    </div>
                    <div class="box-input required">
                      <label for="zip_code" class="text-label">รหัสไปรษณีย์</label>
                      <input type="tel" name="zip_code" id="zip_code" class="form-input" value="<?php echo $data['data']['zip_code'];  ?>">
                      <p class="text-validate">กรุณาระบุ</p>
                    </div>
                    <?php endif; ?>
                  </div>
                </div>

                <div class="box-step box-model fixed-show">
                  <h2>รุ่นที่จอง</h2>

                  <div class="select-model">
                 
                    <div class="item">
                      <input type="radio" id="M9808" name="models_car" <?php echo ($check== '3') ?  "checked" : "" ;  ?> value="3">
                      <label for="M9808">
                        <span class="check"></span>
                
                        <picture>
                          <source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m9808@2x.png 2x" type="image/png">
                          <img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m9808.png" alt="M9808" loading="lazy" width="176" height="140">
                        </picture>
                        <span class="text">
                          <span class="name-model">แทรกเตอร์คูโบต้า M9808</span>
                          <span class="group-text">
                            <span>ขนาด 98 แรงม้า</span>
                            <span><strong>1,379,000</strong> บาท</span>
                          </span>
                        </span>
                      </label>
                    </div>
                    <div class="item">
                      <input type="radio" id="M8808" name="models_car" <?php echo ($check== '2') ?  "checked" : "" ;  ?> value="2">
                      <label for="M8808">
                        <span class="check"></span>
                
                        <picture>
                          <source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m8808@2x.png 2x" type="image/png">
                          <img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m8808.png" alt="M8808" loading="lazy" width="176" height="140">
                        </picture>
                        <span class="text">
                          <span class="name-model">แทรกเตอร์คูโบต้า <br class="mobile-only">M8808</span>
                          <span class="group-text">
                            <span>ขนาด 88 แรงม้า</span>
                            <span><strong>1,254,000</strong> บาท</span>
                          </span>
                        </span>
                      </label>
                    </div>
                    <div class="item">
                      <input type="radio" id="M7508" name="models_car" <?php echo ($check== '1') ?  "checked" : "" ;  ?> value="1">
                      <label for="M7508">
                        <span class="check"></span>
                
                        <picture>
                          <source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m7508@2x.png 2x" type="image/png">
                          <img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m7508.png" alt="M7508" loading="lazy" width="176" height="140">
                        </picture>
                        <span class="text">
                          <span class="name-model">แทรกเตอร์คูโบต้า <br class="mobile-only">M7508</span>
                          <span class="group-text">
                            <span>ขนาด 75 แรงม้า</span>
                            <span><strong>1,058,000</strong> บาท</span>
                          </span>
                        </span>
                      </label>
                    </div>
                   
                  </div>

                  <div class="row row-relate">
                    <div class="group-form payment">
                      <?php if($data['dataform']['paymentmethod_'] == 'open'):  ?>
                        <h2>ท่านประสงค์ชำระค่าสินค้าในรูปแบบใด</h2>
                        <?php 
                          if(!empty($data['data']['payment_methond'])){
                            $check = $data['data']['payment_methond'];
                          }else{
                            $check = '';
                          }
                          //  echo  $check ;
                        ?>
                        <div class="form-radio">
                          <input type="radio" id="loan" name="paymentmethod" <?php echo ($check== 'loan') ?  "checked" : "" ;  ?> value="loan">
                          <label for="loan">เช่าซื้อผ่านบริษัท สยามคูโบต้า ลีสซิ่ง จำกัด (SKL)<br><small>ส่วนลดดอกเบี้ยพิเศษ 1% (100 คันแรก)</small></label>
                        </div>
                        <div class="form-radio">
                          <input type="radio" id="cash" name="paymentmethod" <?php echo ($check== 'cash') ?  "checked" : "" ;  ?> value="cash">
                          <label for="cash">เงินสด<br><small>ส่วนลดสินค้า 20,000 บาท (100 คันแรก)</small></label>
                        </div>
                        <div class="form-radio">
                          <input type="radio" id="other" name="paymentmethod" <?php echo ($check== 'other') ?  "checked" : "" ;  ?> value="other">
                          <label for="other">อื่นๆ โปรดระบุ<br><small>ส่วนลดสินค้า 20,000 บาท (100 คันแรก)</small></label>
                        </div>
                        <div class="group-form-hide">
                          <div class="box-input w-full required">
                            <label for="other_payment_methond_text" class="text-label">โปรดระบุ</label>
                            <input type="text" name="other_payment_methond_text" id="other_payment_methond_text" class="form-input" value="<?php echo $data['data']['other_payment_methond_text']; ?>">
                            <p class="text-validate">กรุณาระบุ</p>
                          </div>
                        </div>
                      <?php endif; ?>
                    </div>
                    
                    <div class="group-form area">
                      <?php if($data['dataform']['use_area_'] == 'open'):  ?>
                        <h2>พื้นที่ในการใช้งานแทรกเตอร์</h2>
                        <?php 
                          if(!empty($data['data']['use_area'])){
                            $check = $data['data']['use_area'];
                          }else{
                            $check = '';
                          }
                        ?>
                        <div class="form-radio">
                          <input type="radio" id="profileaddress" name="use_area"  <?php echo ($check== 'profileaddress') ?  "checked" : "" ;  ?> value="profileaddress">
                          <label for="profileaddress">ใช้ที่อยู่เดียวกับข้อมูลส่วนตัว</label>
                        </div>
                        <div class="form-radio">
                          <input type="radio" id="otheraddress" name="use_area" <?php echo ($check== 'otheraddress') ?  "checked" : "" ;  ?> value="otheraddress">
                          <label for="otheraddress">ใช้ในพื้นที่อื่น</label>
                        </div>
                        <div class="group-form-hide">
                          <div class="box-input w-full required">
                            <label for="sel1" class="text-label">จังหวัด:</label>
                            <select class="form-select" name="Ref_prov_id_otheraddress" id="provinces_otheraddress">
                              <?php foreach ($data['provinces'] as $value) { ?>
                                <?php if($value['name_th'] == $data['data']['use_province']): ?>
                                    <option value="<?=$value['id']?>" selected><?=$value['name_th']?></option>
                                  <?php else: ?>
                                    <option value="<?=$value['id']?>"><?=$value['name_th']?></option>
                                  <?php endif; ?>
                              <?php } ?>
                            </select>
                            <p class="text-validate">กรุณาระบุ</p>
                          </div>
                          <div class="box-input w-full required">
                            <label for="sel1" class="text-label">อำเภอ</label>
                            <select class="form-select" name="Ref_dist_id_otheraddress" id="amphures_otheraddress">
                              <option value="">เลือกอำเภอ</option>
                            </select>
                            <p class="text-validate">กรุณาระบุ</p>
                          </div>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>

                <div class="group-btn">
                  <button type="button" class="btn -outline btn-cancel">
                    <span>
                      ยกเลิก
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/cross.svg" alt="icon">
                    </span>
                  </button>
                  <button type="button" class="btn btn-submit">
                    <span>
                      บันทึก
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/save.svg" alt="icon">
                    </span>
                  </button>
                </div>
              </form> 
            </div>
          </div>
        </div>
      </div>
      
			
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <div style="display: none;">
    <div id="popup" class="popup-check-id">
      <div class="inner">
        <h2>แก้ไขข้อมูลการสมัคร</h2>
        <div class="box-bg">
          <p id="id_p_box_bg"> </p>
          <!-- <p>หมายเลขบัตรประชาชนนี้ถูกลงทะเบียนแล้ว<br class="show-all">คุณจอง <strong>KUBOTA</strong> รุ่น <strong class="model">M7508</strong></p> -->
        </div>
        <div class="box-remark">
          <p id="id_p_box_remark"> </p>
          <!-- <p>หากต้องการแก้ไขข้อมูลกรุณาติดต่อที่<br class="show-all"><strong>หจก.คูโบต้าศรีสะเกษเลาเจริญ</strong></p> -->
        </div>
      </div>
    </div>
  </div>

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">
      function iOSversion() {
        var iOS = !window.MSStream && /iPad|iPhone|iPod/.test(navigator.userAgent); // fails on iPad iOS 13
        if (iOS) { // <-- Use the one here above
          if (window.indexedDB) { return 'iOS 8 and up'; }
          if (window.SpeechSynthesisUtterance) { return 'iOS 7'; }
          if (window.webkitAudioContext) { return 'iOS 6'; }
          if (window.matchMedia) { return 'iOS 5'; }
          if (window.history && 'pushState' in window.history) { return 'iOS 4'; }
          return 'iOS 3 or earlier';
        }

        return 'notios';
      }
      function download(url){
        var txt_name = $('.m_name').attr('qazwsx');
        var a = $("<a style='display:none' id='js-downloder'>")
        .attr("href", url)
        .attr("download", txt_name+".png")
        .appendTo("body");

        a[0].click();

        a.remove();
      }
      function saveCapture(element) {
        if(iOSversion() != 'notios'){
          // var vp = document.getElementById("viewportMeta").getAttribute("content");
          // document.getElementById("viewportMeta").setAttribute("content", "width=800");
        }
        $('.box-btn-download').hide();
        $('.box-btn-hidden').show();
        html2canvas(element , { 
                    allowTaint : true ,
                    letterRendering: 1 ,
                    logging: true, 
                    useCORS : true,
                    // width: 800,
                    height: 700,
                    // windowHeight: 800,
                    // windowWidth: 600,
                    windowWidth: element.scrollWidth,
                    // windowHeight: element.scrollHeight
                  }).then(function(canvas) {
                    
                    download(canvas.toDataURL());
                    if(iOSversion() != 'notios'){
                      // document.getElementById("viewportMeta").setAttribute("content", vp);
                    }
                    
                  });
        $('.box-btn-download').show();
        $('.box-btn-hidden').hide();
      }

      function loadimg_pomotion(){
      // var element = document.querySelector("#capture");
      
        var element = document.querySelector("#main-content");
          
          saveCapture(element)
      }
     
      //start set when ready
      $( document ).ready(function() {
        // iOSversion();
        console.log(iOSversion());
        $('.box-btn-hidden').hide();
        var amphures_text = "<?php echo $data['data']['amphures']; ?>";
 
        var district_text = "<?php echo $data['data']['district']; ?>";
        var id_province = $('#provinces').val();
        $.ajax({
          type: "POST",
          async: false,
          url: site_url+'/ajax_db',
          data: {id:id_province , text : amphures_text ,function:'provinces'},
          success: function(data){
              $('#amphures').html(data); 
              $('#districts').html(' '); 
              $('#districts').val(' ');  
              // $('#zip_code').val(' '); 
          }
        });

        var id_amphures = $('#amphures').val();
        $.ajax({
          type: "POST",
          // async: false,
          url: site_url+'/ajax_db',
          data: {id:id_amphures , text : district_text , function:'amphures'},
          success: function(data){
              $('#districts').html(data);  
          }
        });

        var provinces_otheraddress = $('#provinces_otheraddress').val();
        var use_amphures = "<?php echo $data['data']['use_amphures']; ?>";
        $.ajax({
          type: "POST",
          async: false,
          url: site_url+'/ajax_db',
          data: {id:provinces_otheraddress , text : use_amphures ,function:'provinces'},
          success: function(data){
            $('#amphures_otheraddress').html(data);  
          }
        });
        
      });
      //end set when ready
    
      $('.btn-cancel').click(function(){
        showLoading();
        location.reload();
        // console.log('run gif and refresh');
        //refresh
      });

      //บันทึก
      $('.btn-submit').click(function(){
        var check= true;
        var filter_email =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if($("#email").length != 0) {
          if($('#email').val() != "" ){ 
            if(!filter_email.test($('#email').val())){
                check = false;
                $("#email").parent().addClass('error');
            }else {
              $("#email").parent().removeClass('error');
            }
          }else{
            $("#email").parent().removeClass('error');
          }
        }
        if($("#fullname").length != 0) {
          if($('#fullname').val()=== ""){ 
            $("#fullname").parent().addClass('error');
            check = false;
          }else{
            $("#fullname").parent().removeClass('error');
          }
        }

        if($("#address").length != 0) {
          if($('#address').val()=== ""){ 
            $("#address").parent().addClass('error');
            check = false;
          }else{
            $("#address").parent().removeClass('error');
          }
        }


        if(($("#provinces").length != 0) &&  ($("#amphures").length != 0) && ($("#districts").length != 0)  ) {
          if($('#provinces').val()=== "" || $('#provinces').val()=== null){ 
            $("#provinces").parent().addClass('error');
            check = false;
          }else{
            $("#provinces").parent().removeClass('error');
          }

          if($('#amphures').val()=== "" || $('#amphures').val()=== null){ 
            $("#amphures").parent().addClass('error');
            check = false;
          }else{
            $("#amphures").parent().removeClass('error');
          }

          // if($('#districts').val()=== "" || $('#districts').val()=== null ){ 
          //   $("#districts").parent().addClass('error');
          //   check = false;
          // }else{
          //   $("#districts").parent().removeClass('error');
          // }

          if($('#zip_code').val()=== "" || $('#zip_code').val()=== null ){ 
            $("#zip_code").parent().addClass('error');
            check = false;
          }else{
            $("#zip_code").parent().removeClass('error');
          }
        }

        if( $("input[name='paymentmethod']").length != 0   ) {
          if(typeof $("input[name='paymentmethod']:checked").val() === 'undefined' ){ 
            $(".payment").parent().addClass('error');
            check = false;
          }else{
            $(".payment").parent().removeClass('error');
            // $("#").parent().removeClass('error');
          }

          if(typeof $("input[name='paymentmethod']:checked").val() !== 'undefined' ){ 
            if($("input[name='paymentmethod']:checked").val() == 'other'){
              if($('#other_payment_methond_text').val() == ""){
                $("#other_payment_methond_text").parent().addClass('error');
                check = false;
              }else{
                $("#other_payment_methond_text").parent().removeClass('error');
              }
            }
          }
          
        }// check payment

        if( $("input[name='use_area']").length != 0   ) {
          if( typeof $("input[name='use_area']:checked").val() === 'undefined' ){ 
            $(".area").parent().addClass('error');
            check = false;
          }else{
            $(".area").parent().removeClass('error');
          }

          if(typeof $("input[name='use_area']:checked").val() !== 'undefined' ){ 
            if($("input[name='use_area']:checked").val() == 'otheraddress'){

              if($('#provinces_otheraddress').val()=== "" || $('#provinces_otheraddress').val()=== null){ 
                $("#provinces_otheraddress").parent().addClass('error');
                check = false;
              }else{
                $("#provinces_otheraddress").parent().removeClass('error');
              }

              if($('#amphures_otheraddress').val()=== "" || $('#amphures_otheraddress').val()=== null){ 
                $("#amphures_otheraddress").parent().addClass('error');
                check = false;
              }else{
                $("#amphures_otheraddress").parent().removeClass('error');
              }

            }
          }

        } //check area class



        if(check == false){
          return false;
        }
        $('.btn-submit').css('pointer-events','none'); // prevent multiple click

        var data = new FormData(); //set data
        data.append("otp", $('#otp').val());
        data.append("ref", $('#ref').val());
        data.append("codeid", $('#codeid').val());
        data.append("method", $('#method').val());
        data.append("transid", $('#transid').val());
        data.append("phone", $('#phone').val());
        data.append("fullname", $('#fullname').val());
        data.append("email", $('#email').val());   
        data.append("address", $('#address').val());
        data.append("personalid", $('#personalid').val());
        data.append("provinces", $('#provinces option:selected').text());
        data.append("amphures", $('#amphures option:selected').text());
        data.append("districts", $('#districts option:selected').text());
        data.append("zip_code", $('#zip_code').val());
        data.append("other_payment_methond_text", $('#other_payment_methond_text').val());
        data.append("address", $('#address').val());
        data.append("models_car", $("input[name='models_car']:checked").val());
        data.append("paymentmethod", $("input[name='paymentmethod']:checked").val());
        data.append("use_area", $("input[name='use_area']:checked").val());
        data.append("provinces_otheraddress", $('#provinces_otheraddress option:selected').text());
        data.append("amphures_otheraddress", $('#amphures_otheraddress option:selected').text());
        data.append("action", 'edit-ajax-register');
        showLoading();
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
           
            $('.btn-submit').css('pointer-events','auto');
            if(data.Status == 'OK'){
              // $('#register_title').val(data.codetitle);
              $('#cm').val($("input[name='models_car']:checked").val());
              $('.form-style').addClass('disabled-form');
              // $('#submit_form').submit(); //if ok goto thank you page
              showLoading();
              location.reload();
            }else if(data.Status == 'error_login'){
              hideLoading();
              $('#id_p_box_bg').text(data.MSG);
              popupCheckId();
              
            }else{
              hideLoading();
              $('#id_p_box_bg').text(data.MSG);
              popupCheckId();
              // $('#submit_form').attr('action', site_url+'/gu-otp');
              // $('#submit_form').submit();
            }

          },
          error: function (data) {
            console.log("error: "+data);
            alert('something went wrong! please try again later.');
            // return false;
          }
        }); // close ajax
       
      });

      $('#provinces').change(function() {
        var id_province = $(this).val();

          $.ajax({
          type: "POST",
          url: site_url+'/ajax_db',
          data: {id:id_province,function:'provinces'},
          success: function(data){
              $('#amphures').html(data); 
              $('#districts').html(' '); 
              $('#districts').val(' ');  
              $('#zip_code').val(' '); 
          }
        });
      });

      $('#amphures').change(function() {
        var id_amphures = $(this).val();

          $.ajax({
          type: "POST",
          url: site_url+'/ajax_db',
          data: {id:id_amphures,function:'amphures'},
          success: function(data){
              $('#districts').html(data);  
          }
        });
      });

      $('#districts').change(function() {
        var id_districts= $(this).val();

        $.ajax({
          type: "POST",
          url: site_url+'/ajax_db',
          data: {id:id_districts,function:'districts'},
          success: function(data){
              $('#zip_code').val(data);
              console.log( $('#zip_code').val(data));
          }
        });
  
      });

      $('#provinces_otheraddress').change(function() {
        var id_province = $(this).val();

          $.ajax({
          type: "POST",
          url: site_url+'/ajax_db',
          data: {id:id_province,function:'provinces'},
          success: function(data){
              $('#amphures_otheraddress').html(data);
          }
        });
      });
      
      $(function(){
        $('.box-step .btn-edit').on('click', function(){
          $('.form-style').removeClass('disabled-form');
          
          return false;
        });
        
        $('.select-model input:checked').parents('.item').addClass('checked');

        $('.form-radio input:checked').parents('.form-radio').addClass('checked');
        
        $('.payment .form-radio').on('change', function () {
          $('.payment .form-radio').removeClass('checked');
          $('.payment .form-input').val("");
          $(this).addClass('checked');
        });

        $('.area .form-radio').on('change', function () {
          $('.area .form-radio').removeClass('checked');
          $('.area option:selected').prop("selected", false);
          $(this).addClass('checked');
        });
      });




      function popupCheckId() {
        Fancybox.show([{ src: "#popup", type: "inline" }]);
      }
      // showLoading();
      // hideLoading();
  </script>
  <!-- end javascript this page -->