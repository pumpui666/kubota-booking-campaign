<?php 
	if(!empty($_POST['method'])){
		$method = $_POST['method'];
	}else{
		$method = 1;
	}
	if(!empty($_POST['cm'])){
		$cm = $_POST['cm'];
	}else{
    $cm = 4;
	}

  if(!empty($_POST['personalid'])){
		$personalid = $_POST['personalid'];
	}else{
    Redirect(site_url(), false);// redirect
	}

  if(!empty($_POST['phone'])){
		$phone = $_POST['phone'];
	}else{
    Redirect(site_url(), false);// redirect
	}

  if(!empty($_POST['fullname'])){
		$fullname = $_POST['fullname'];
	}else{
    Redirect(site_url(), false);// redirect
	}

  // $data = [];
  // $data['status_open_activity_1'] = get_field('open_activety_1',6); //check open activity 1
	// $data['status_open_activity_2'] = get_field('open_activety_2',6); //check open activity 1
  // $data['count'] = 	get_field('count',21);
  // $count = (!empty($data['count'] ))? $data['count'] : 0; 

  // if(($data['status_open_activity_1'] == 'open' &&  $count <200 )|| $data['status_open_activity_2'] == 'open'){
  //   $check_status =  'op';
  // }else{
  //   $check_status =  'ed';
  //   Redirect(site_url(), false);// redirect
  // }

?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="confirm-otp-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
      <div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>

      <div class="container">
        <div class="box-content">
          <div class="inner">
            <div class="title-page">
              <?php if(($method == 2) && empty($_SESSION["codeid"]) && empty($_SESSION["username_codeid"]) ): ?>
                <h1 class="title">สวัสดีค่ะ คุณ<?php echo get_the_title($_SESSION["codeid"]); ?></h1>
              <?php else: ?>
                <h1 class="title">สวัสดีค่ะ คุณ<?php echo $fullname; ?></h1>
              <?php endif; ?>
              <p class="sub-title">กรุณายืนยันตัวตนผ่าน OTP เพื่อทำการตรวจ<br class="mobile-only">สอบสิทธื์ หรือแก้ไขข้อมูลส่วนตัว</p>
              <p class="sub-title">ระบุ OTP <br class="mobile-only">[รหัสอ้างอิง:<span id="_insertref"> </span>] ที่ได้รับผ่านทาง SMS ที่</p>
            </div>

            <div class="form-style">
              <p class="telephone-number"><?php echo $phone; ?></p>

              <form action="<?php echo site_url('edit-register-form'); ?>" method="post" id="submit_form">

                <div class="box-input required">
                  <label for="otp" class="text-label">รหัส OTP</label>
                  <input type="tel" id="otp" class="form-input" name="otp" placeholder="000000">
                  <p class="text-validate">กรุณาระบุ</p>
                </div>
     
                <input type="hidden" id="transid" name="transid"  value="" >
                <input type="hidden" id="ref" name="ref"  value="" >
                <input type="hidden" id="phone" name="phone"  value="<?php echo $phone; ?>" >
                <input type="hidden" id="personalid" name="personalid" value="<?php echo $personalid; ?>" >
                <input type="hidden" id="method" name="method" value="<?php echo $method; ?>">
                <input type="hidden" id="cm" name="cm" value="<?php echo $cm; ?>">

                <div class="box-resend">
                  <p>OTP มีอายุในการใช้งาน 5 นาที
                    <br><strong>ไม่ได้รับรหัส? <button type="button" class="btn -link btn-resubmit">ขอรหัส OTP อีกครั้ง</button></strong>
                  </p>
                </div>
                
                <button type="button" class="btn btn-submit">
                  <span>
                    ตกลง
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
                  </span>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->
  
  <div style="display: none;">
    <div id="popup" class="popup-check-id">
      <div class="inner">
        <h2>ยืนยันรหัส OTP</h2>
        <div class="box-bg">
          <p id="id_p_box_bg"> </p>
          <!-- <p>หมายเลขบัตรประชาชนนี้ถูกลงทะเบียนแล้ว<br class="show-all">คุณจอง <strong>KUBOTA</strong> รุ่น <strong class="model">M7508</strong></p> -->
        </div>
        <div class="box-remark">
          <p id="id_p_box_remark"> </p>
          <!-- <p>หากต้องการแก้ไขข้อมูลกรุณาติดต่อที่<br class="show-all"><strong>หจก.คูโบต้าศรีสะเกษเลาเจริญ</strong></p> -->
        </div>
      </div>
    </div>
  </div>

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">
      // $('.btn-resubmit').click(function(){
      //   $('#submit_form').attr('action', site_url+'/g-otp');
      //   $('#submit_form').submit();
      // });
      
      $('.btn-submit').click(function(){
        var check= true;
        if($('#otp').val()=== ""){
          $("#otp").parent().addClass('error');
          check = false;
        }else{
          $("#otp").parent().removeClass('error');
        }

        var data = new FormData();

        data.append("otp", $('#otp').val());
        data.append("ref", $('#ref').val());
        data.append("transid", $('#transid').val());
        data.append("phone", $('#phone').val());
        
        data.append("action", 'v-ajax-otp');
        if(check == false){
          return false;
        }
        $('.btn-submit').css('pointer-events','none');
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            // alert('success');
            $('.btn-submit').css('pointer-events','auto');
            // $('input').val('');
            // $('textarea').val('');
            if(data.Status == 'ok'){
              if(data.objSTATUS[0] == 'OK'){
                showLoading();
                $('#submit_form').submit();
              }else{
                console.log('something went wrong'+data.objDETAIL[0]);
                if(data.objDETAIL[0] == "The OTP. Verifed"){
                  // alert('OTP นี้ได้รับการยืนยันไปแล้ว กรุณากดส่ง OTP');
                  $('#id_p_box_bg').text('OTP นี้ได้รับการยืนยันไปแล้ว กรุณากดส่ง OTP');
                  popupCheckId();
                }else{
                  $('#id_p_box_bg').text('Something Went Wrong : '+data.objDETAIL[0]);
                  popupCheckId();
                  // alert('Something Went Wrong : '+data.objDETAIL[0]);
                }
                
              }
            }else{
              if(typeof data.MSG[0] !== "undefined")
              {
                // alert(data.MSG[0]);
                $('#id_p_box_bg').text(data.MSG[0]);
                popupCheckId();
              }else{
                // alert(data.MSG);
                $('#id_p_box_bg').text(data.MSG);
                popupCheckId();
              }
            }

          },
          error: function (data) {
            // alert(data);
            console.log("error: "+data);
            alert('something went wrong! please try again later.');
            // return false;
          }
        }); // close ajax
       
      });

      $('.btn-resubmit').click(function(){
        var check= true;
        if($('#phone').val()=== ""){
          $("#phone").parent().addClass('error');
          check = false;
        }else{
          $("#phone").parent().removeClass('error');
        }

        var data = new FormData();

        data.append("phone", $('#phone').val());
        data.append("action", 'send-ajax-phoneotp');
        if(check == false){
          return false;
        }
        $('.btn-resubmit').css('pointer-events','none');
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            // alert('success');
            $('.btn-resubmit').css('pointer-events','auto');
            if(data.Status == 'ok'){
              showLoading();
              $('#transid').val(data.transid[0]);
              $('#ref').val(data.ref[0]);
              $('#_insertref').text(data.ref[0]);
              hideLoading();
              // alert('OTP sent!');
            }else{
              if(typeof data.MSG[0] !== "undefined")
              {
                // alert(data.MSG[0]);
                $('#id_p_box_bg').text(data.MSG[0]);
                popupCheckId();
              }else{
                // alert(data.MSG);
                $('#id_p_box_bg').text(data.MSG);
                popupCheckId();
              }
            }

          },
          error: function (data) {
            // alert(data);
            console.log("error: "+data);
            alert('something went wrong! please try again later.');
            // return false;
          }
        }); // close ajax
       
      });

      $( ".btn-resubmit" ).trigger( "click" );

      function popupCheckId() {
        Fancybox.show([{ src: "#popup", type: "inline" }]);
      }
      // showLoading();
      // hideLoading();
  </script>
  <!-- end javascript this page -->