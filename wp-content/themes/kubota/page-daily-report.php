<?php
if(!empty($_GET['order'])){
    if($_GET['order'] == 'qazwsxedcrfvtgb'){
        $dates_time =   date('d-m-Y',strtotime("-1 days"));
            date_default_timezone_set('Asia/Bangkok');
            //export to server

                $arr = [];
                $args = [
                            'post_type' => 'data_',
                            'posts_per_page' =>-1,
                            'post_status'=>'publish',
                            'orderby' => 'date',
                            //'orderby' => 'post_date',
                            'order'   => 'ASC',
                    ];
                    
                    
                    // echo "<pre>"; var_dump($data_time);//die;


                $the_download = new WP_Query($args);
                // echo "<pre>"; var_dump($the_download->posts);die;
                if(empty($the_download->posts)){
                    echo "no data to sent"; die;
                }
                $arr = [];
                foreach ($the_download->posts as $key => $post) {
                    array_push($arr,
                            [
                                'title' => get_the_title($post->ID),
                                'fullname' => get_field('fullname',$post->ID),
                                'phone' => get_field('phone',$post->ID),
                                'email' => get_field('email',$post->ID),
                                'personal_id' => get_field('personal_id',$post->ID),
                                'address' => get_field('address',$post->ID) ." ตำบล". get_field('district',$post->ID)." อำเภอ".get_field('amphures',$post->ID)." จังหวัด".get_field('province',$post->ID)." ".get_field('zip_code',$post->ID) ,
                                'use_area' =>  get_field('use_area',$post->ID),
                                'use_province' =>  get_field('use_province',$post->ID),
                                'use_amphures' =>  get_field('use_amphures',$post->ID),
                                'models_car' =>  get_field('models_car',$post->ID),
                                'use_address' =>  "อำเภอ".get_field('use_amphures',$post->ID)." จังหวัด".get_field('use_province',$post->ID),
                                'payment_methond' =>  get_field('payment_methond',$post->ID),
                                'other_payment_methond_text' =>  get_field('other_payment_methond_text',$post->ID),
                                'agent_' =>  get_field('agent_',$post->ID),
                                // 'recruit_branchs' => get_field('recruit_branchs',$post->ID),
                                'contact_back' => get_field('contact_back',$post->ID),
                                'counts_number' => get_field('counts_number',$post->ID),
                                'dates_time' => get_the_date( 'd-m-Y', $post->ID ),
                                'dates' => get_the_date( 'd-m-Y H:i:s', $post->ID ),
                            ]
                    );
                }
                // echo "<pre>"; var_dump($arr);die;

                if(empty($arr) && !isset($arr)){
                    echo 'no data to sent'; die;
                }
                ?>
                <?php
                /**
                 * PHPExcel
                 *
                 * Copyright (C) 2006 - 2014 PHPExcel
                 *
                 * This library is free software; you can redistribute it and/or
                 * modify it under the terms of the GNU Lesser General Public
                 * License as published by the Free Software Foundation; either
                 * version 2.1 of the License, or (at your option) any later version.
                 *
                 * This library is distributed in the hope that it will be useful,
                 * but WITHOUT ANY WARRANTY; without even the implied warranty of
                 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
                 * Lesser General Public License for more details.
                 *
                 * You should have received a copy of the GNU Lesser General Public
                 * License along with this library; if not, write to the Free Software
                 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
                 *
                 * @category   PHPExcel
                 * @package    PHPExcel
                 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
                 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
                 * @version    1.8.0, 2014-03-02
                 */

                /** Error reporting */
                error_reporting(E_ALL);
                ini_set('display_errors', TRUE);
                ini_set('display_startup_errors', TRUE);


                if (PHP_SAPI == 'cli')
                    die('This example should only be run from a Web Browser');

                /** Include PHPExcel */
                include_once("lib/Classes/PHPExcel.php");


                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();

                // Set document properties
                $objPHPExcel->getProperties()->setCreator("ADYIM")
                                                        ->setLastModifiedBy("Maarten Balliauw")
                                                        ->setTitle("Office 2007 XLSX Test Document")
                                                        ->setSubject("Office 2007 XLSX Test Document")
                                                        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                                        ->setKeywords("office 2007 openxml php")
                                                        ->setCategory("Test result file");
                // Add some data
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ผู้สมัครลำดับที่')
                ->setCellValue('B1', 'Booking-ID')
                ->setCellValue('C1', 'ชื่อ-นามสกุล')
                ->setCellValue('D1', 'เบอร์โทร')
                ->setCellValue('E1', 'อีเมล')
                ->setCellValue('F1', 'หมายเลขบัตรประชาชน')
                ->setCellValue('G1', 'รุ่นรถที่จอง')
                ->setCellValue('H1', 'ที่อยู่')
                ->setCellValue('I1', 'สถานที่ใช้งานรถ')
                ->setCellValue('J1', 'ที่อยู่สถานที่ใช้งานรถ(กรณีไม่ตรงกับที่อยู่)')
                ->setCellValue('K1', 'รูปแบบการชำระเงิน')
                ->setCellValue('L1', 'ตัวแทน(กรณีสมัครผ่านตัวแทน)')
                ->setCellValue('M1', 'สถานะการติดต่อกลับ')
                ->setCellValue('N1', 'วันเวลาที่สมัคร');
                //  ->setCellValue('F1', 'order detail');
                // Miscellaneous glyphs, UTF-8
                $count = 0;
                foreach ($arr as $key => $item) {
                    if($item['dates_time'] ===    $dates_time ){
                        
                        if($item['models_car'] == 1){
                            $item['models_car'] = 'M7508';
                        }elseif($item['models_car'] == 2){
                            $item['models_car'] = 'M8808';   
                        }else{
                            $item['models_car'] = 'M9808';  
                        }
                        if($item['use_area'] == 'profileaddress'){
                            $item['use_area'] = 'ใช้ที่อยู่เดียวกับข้อมูลส่วนตัว';
                            $item['use_address'] = ' ';
                        }else{
                            $item['use_area'] = 'ใช้ที่อยู่อื่น';
                        }
                    
                        if($item['payment_methond'] == 'cash'){
                            $item['payment_methond'] = 'เงินสด';
                        }elseif($item['payment_methond'] == 'loan'){
                            $item['payment_methond'] = ' เช่าซื้อผ่าน SKL';
                        }else{
                            $item['payment_methond'] = 'other '.$item['other_payment_methond_text'];
                        }
                        if(empty($item['contact_back'])){
                            
                            $item['contact_back'] = 'pending';
                        }else{
                            if( $item['contact_back'] =="pending"){
                                $item['contact_back'] =="pending";
                            }else{
                                $item['contact_back'] = 'complete';
                            }
                        
                        }
                    
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit('A'.($count+2),$item['counts_number'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('B'.($count+2),$item['title'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('C'.($count+2),$item['fullname'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('D'.($count+2),$item['phone'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('E'.($count+2),$item['email'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('F'.($count+2),$item['personal_id'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('G'.($count+2),$item['models_car'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('H'.($count+2),$item['address'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('I'.($count+2),$item['use_area'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('J'.($count+2),$item['use_address'],PHPExcel_Cell_DataType::TYPE_STRING)
                    
                        ->setCellValueExplicit('K'.($count+2),$item['payment_methond'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('L'.($count+2),$item['agent_'],PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit('M'.($count+2),$item['contact_back'],PHPExcel_Cell_DataType::TYPE_STRING)
                    
                        ->setCellValueExplicit('N'.($count+2),$item['dates'],PHPExcel_Cell_DataType::TYPE_STRING);
                        $count ++;
                    }
                    
                }
                
                if(empty($count)){
                    echo 'no data to sent'; die;
                }
                // Rename worksheet
                $objPHPExcel->getActiveSheet()->setTitle('LeadFormEmail');


                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);


                // // Redirect output to a client’s web browser (Excel2007)
                // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                // header('Content-Disposition: attachment;filename="export-file-register-kubota.xlsx"');
                // header('Cache-Control: max-age=0');
                // // If you're serving to IE 9, then the following may be needed
                // header('Cache-Control: max-age=1');

                // // If you're serving to IE over SSL, then the following may be needed
                // header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                // header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                // header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                // header ('Pragma: public'); // HTTP/1.0

                // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

                // $objWriter->save('php://output');






            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save(__DIR__."/assets/exelfile/report".$dates_time.".xls");
       


            $attachments = __DIR__."/assets/exelfile/report".$dates_time.".xls";
            if(empty($attachments)){
                $attachments = '';
            }
            // sendmail
            $to = get_field('email', 131);
            if (empty($to) && !isset($to)) {
                $to = 'mongkol.n@adyim.com';
            }
            if(empty($_POST['email']) || $_POST['email']=='' || $_POST['email']=='undefined'){
                $fromemail = 'callcenter@Kubota.com';
            }else{
                $fromemail = $_POST['email'];
            }
            $fromemail = 'kubotamseries@adyim.com';
            // echo  $fromemail;
            // die;
            // $to = 'god.bushido1@gmail.com';
            $sendfromMSG = "Daily Report Kubota";
            $time = time();
            $body = '<html>';
            $body .= '<head>';
            $body .= '<meta charset="UTF-8">';
            $body .= '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
            $body .= '<meta http-equiv="X-UA-Compatible" content="ie=edge">';
            $body .= '<title>Recruit</title>';
            $body .= '</head>';
            $body .= '<body>';
            $body .= 	'<table width="640" cellpadding="10" cellspacing="0">';
            $body .= 	'<tr><td align="center" style="font-family:Arial;">	';
            $body .= 		'<h2>Daily Report Kubota</h2>';
            // $body .= 		'<p>อีเมล์ฉบับนี้เพื่อแจ้งให้คุณทราบว่ามีการติดต่อ Booking ผ่านเว็บไซน์ Kubota </p>';
            $body .= 	'</td></tr>';
            $body .= 	'<tr><td align="center">';

            $body .= 		'<p style="line-height:20px;margin:1em 0;">Daily Report ' . date('d-m-Y') . '</p>';
            $body .= 	'</td></tr>';
            $body .= 	'<tr>';
            $body .= 		'<td align="center">';
            $body .= 			'<h4>ข้อมูลการดาวโหลด</h4>';
            // $body .= 			'กรณีไม่ได้รับไฟล์ที่แนบมากับ email สามารถติดต่อ admin ';
            // $body .= 		'สามารถดาวโหลด Report ได้ที่ :  <a href ="' . site_url('export-data_daily') . '?order=666AA'.$dates_time  .'">' . site_url('export') . '</a>   <br>';

            $body .= 		'</td>';
            $body .= 	'</tr> ';
            $body .= 	'</table>';
            $body .=  '</body>';
            $body .=  '</html>';
            add_filter('wp_mail_content_type', function( $content_type ) {
                return 'text/html';
            });
            add_filter('wp_mail_charset', function( $charset ) {
                return 'charset=UTF-8';
            });
            // apply_filters( 'wp_mail_charset', 'charset=UTF-8' );
            $headers[] = 'From: Kubota Prebooking  <kubotamseries@adyim.com>';
            // $headers[] = 'Cc: John Q Codex <jqc@wordpress.org>';
            // $headers[] = 'Cc: iluvwp@wordpress.org';
            if (!wp_mail($to, $sendfromMSG, $body, $headers,$attachments)) {
                echo 'email dont sent';
            }else{
                echo 'email  sent';
            }
    }
}else{
    echo 'contact admin';
}

?>