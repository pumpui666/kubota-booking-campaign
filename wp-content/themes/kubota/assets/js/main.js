window.onload = function () {
	$('.box-loading').addClass('hide');
};

/* Function in Page */
function runScriptHomePage() {
	gsap.set('#header', { opacity: 0, y: -100 });
	gsap.set('#footer', { opacity: 0 });
	gsap.set('.section-highlight-content', { opacity: 0 });
	gsap.set('.section-highlight-content .box-text .text-title', { opacity: 0, y: 100 });
	gsap.set('.section-highlight-content .box-text .detail', { opacity: 0, y: 100 });
	gsap.set('.section-highlight-content .box-text .box-proposal', { opacity: 0, y: 100 });
	gsap.set('.section-highlight-content .box-text .group-btn', { opacity: 0, y: 100 });

	gsap.set('.section-promotion', { opacity: 0 });

	gsap.set('.section-video', { opacity: 0, y: 100 });

	gsap.set('.section-special-strength .title-section', { opacity: 0, y: 100 });
	gsap.set('.section-special-strength .item', { opacity: 0 });
	gsap.set('.section-special-strength .box-text h3', { opacity: 0, y: 100 });
	gsap.set('.section-special-strength .box-text p', { opacity: 0, y: 100 });

	gsap.set('.section-feature', { opacity: 0 });
	gsap.set('.section-feature .leaf', { opacity: 0, x: -50 });
	gsap.set('.section-feature .list-feature', { opacity: 0, y: 100 });

	gsap.set('.section-model', { opacity: 0 });
	gsap.set('.section-model .title-section', { opacity: 0, y: 100 });
	gsap.set('.section-model .item', { opacity: 0, y: 100 });

	window.onload = function () {
		$('.box-loading').addClass('hide');

		setTimeout(function () {
			gsap.to('#header', 0.8, { opacity: 1, y: 0, ease: Power4.easeOut });
			gsap.to('#footer', 0.8, { opacity: 1, ease: Power4.easeOut });
		}, 100);

		ScrollOut({
			targets: '.section-highlight-content',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				$('body').removeClass('show-fixed-position');

				gsap.to('.section-highlight-content', 0.8, { delay: 0.3, opacity: 1, ease: Power4.easeOut });
				gsap.to('.section-highlight-content .box-text .text-title', 0.8, { delay: 0.8, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-highlight-content .box-text .detail', 0.8, { delay: 0.9, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-highlight-content .box-text .box-proposal', 0.8, { delay: 1, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-highlight-content .box-text .group-btn', 0.8, { delay: 1.1, opacity: 1, y: 0, ease: Power4.easeOut });

				gsap.to('.section-promotion', 0.8, { opacity: 1, ease: Power4.easeOut });
			},
			onHidden: function (el) {
				$('body').addClass('show-fixed-position');
			}
		});

		ScrollOut({
			targets: '.section-promotion',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-promotion', 0.8, { opacity: 1, ease: Power4.easeOut });
			}
		});

		ScrollOut({
			targets: '.section-video',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-video', 0.8, { delay: 0.3, opacity: 1, y: 0, ease: Power4.easeOut });
			}
		});

		ScrollOut({
			targets: '.section-special-strength',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-special-strength .title-section', 0.8, { delay: 0.3, opacity: 1, y: 0, ease: Power4.easeOut });
			}
		});

		ScrollOut({
			targets: '.section-special-strength .item-01',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-special-strength .item-01', 0.8, { delay: 0.3, opacity: 1, ease: Power4.easeOut });
				gsap.to('.section-special-strength .item-01 h3', 0.8, { delay: 0.5, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-special-strength .item-01 p', 0.8, { delay: 0.6, opacity: 1, y: 0, ease: Power4.easeOut });
			}
		});
		ScrollOut({
			targets: '.section-special-strength .item-02',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-special-strength .item-02', 0.8, { delay: 0.3, opacity: 1, ease: Power4.easeOut });
				gsap.to('.section-special-strength .item-02 h3', 0.8, { delay: 0.5, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-special-strength .item-02 p', 0.8, { delay: 0.6, opacity: 1, y: 0, ease: Power4.easeOut });
			}
		});
		ScrollOut({
			targets: '.section-special-strength .item-03',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-special-strength .item-03', 0.8, { delay: 0.3, opacity: 1, ease: Power4.easeOut });
				gsap.to('.section-special-strength .item-03 h3', 0.8, { delay: 0.5, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-special-strength .item-03 p', 0.8, { delay: 0.6, opacity: 1, y: 0, ease: Power4.easeOut });
			}
		});
		ScrollOut({
			targets: '.section-special-strength .item-04',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-special-strength .item-04', 0.8, { delay: 0.3, opacity: 1, ease: Power4.easeOut });
				gsap.to('.section-special-strength .item-04 h3', 0.8, { delay: 0.5, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-special-strength .item-04 p', 0.8, { delay: 0.6, opacity: 1, y: 0, ease: Power4.easeOut });
			}
		});

		ScrollOut({
			targets: '.section-feature',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-feature', 0.8, { delay: 0.3, opacity: 1, ease: Power4.easeOut });
				gsap.to('.section-feature .leaf', 0.8, { delay: 0.5, opacity: 1, x: 0, ease: Power4.easeOut });
				gsap.to('.section-feature .list-feature', 0.8, { delay: 0.5, opacity: 1, y: 0, ease: Power4.easeOut });
			}
		});

		ScrollOut({
			targets: '.section-model',
			ScrollingElement: '#wrapper',
			onShown: function (element, ctx, scrollingElement) {
				gsap.to('.section-model', 0.8, { delay: 0.3, opacity: 1, ease: Power4.easeOut });
				gsap.to('.section-model .title-section', 0.8, { delay: 0.5, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-model .item:nth-child(1)', 0.8, { delay: 0.5, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-model .item:nth-child(2)', 0.8, { delay: 0.6, opacity: 1, y: 0, ease: Power4.easeOut });
				gsap.to('.section-model .item:nth-child(3)', 0.8, { delay: 0.7, opacity: 1, y: 0, ease: Power4.easeOut });
			}
		});
	};
}
/* End Function in Page */





/* All Function */

function showLoading() {
	$('.box-loading').removeClass('hide');
}
function hideLoading() {
	$('.box-loading').addClass('hide');
}

/* End All Function */