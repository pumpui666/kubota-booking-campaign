<?php 
	if(!empty($_POST['method'])){
		$method = $_POST['method'];
	}else{
		$method = 1;
	}
	if(!empty($_POST['cm'])){
		$cm = $_POST['cm'];
	}else{
    $cm = 4;
	}
  if(!empty($_POST['personalid'])){
		$personalid = $_POST['personalid'];
	}else{
    Redirect(site_url(), false);// redirect
    // redirect
	}

  if(!empty($_POST['transid'])){
		$transid = $_POST['transid'];
	}else{
    Redirect(site_url(), false);// redirect
    // redirect
	}

  if(!empty($_POST['ref'])){
		$ref = $_POST['ref'];
	}else{
    Redirect(site_url(), false);// redirect
    // redirect
	}
  if(!empty($_POST['phone'])){
		$phone = $_POST['phone'];
	}else{
    Redirect(site_url(), false);// redirect
    // redirect
	}
?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="confirm-otp-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
      <div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>
      
      <div class="container">
        <div class="box-content">
          <div class="inner">
            <div class="title-page">
              <h1 class="title">ยืนยันรหัส OTP</h1>
              <p class="sub-title">ระบุ OTP [รหัสอ้างอิง:<span id="add_ref" ><?php echo $ref; ?></span>] 
              <br class="mobile-only">ที่ได้รับผ่านทาง SMS ที่</p>
            </div>
            
            <div class="form-style">
              <p class="telephone-number"><?php echo $phone; ?></p>

              <form action="<?php echo site_url('register-form'); ?>" method="post" id="submit_form">
                <div class="box-input required">
                  <label for="otp" class="text-label">รหัส OTP</label>
                  <input type="tel" id="otp" class="form-input" name="otp" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="000000">
                  <p class="text-validate">กรุณาระบุ</p>
                </div>
                <input type="hidden" id="transid" name="transid"  value="<?php echo $transid; ?>" >
                <input type="hidden" id="ref" name="ref"  value="<?php echo $ref; ?>" >
                <input type="hidden" id="phone" name="phone"  value="<?php echo $phone; ?>" >
                <input type="hidden" id="personalid" name="personalid" value="<?php echo $personalid; ?>" >
                <input type="hidden" id="method" name="method" value="<?php echo $method; ?>">
                <input type="hidden" id="cm" name="cm" value="<?php echo $cm; ?>">

                <div class="box-resend">
                  <p>OTP มีอายุในการใช้งาน 5 นาที
                    <br><strong>ไม่ได้รับรหัส? <button type="button" class="btn -link btn-resubmit">ขอรหัส OTP อีกครั้ง</button></strong>
                  </p>
                </div>

                <button type="button" class="btn btn-submit">
                  <span>
                    ตกลง
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
                  </span>
                </button>
              
                <div class="box-change-telephone-number">
                  <button type="button" class="btn -outline btn-editphone">
                    <span>
                      เปลี่ยนหมายเลขโทรศัพท์
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/mobile.svg" alt="icon">
                    </span>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> 
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->
  <div style="display: none;">
    <div id="popup" class="popup-check-id">
      <div class="inner">
        <h2>ยืนยันรหัส OTP</h2>
        <div class="box-bg">
          <p id="id_p_box_bg"> </p>
          <!-- <p>หมายเลขบัตรประชาชนนี้ถูกลงทะเบียนแล้ว<br class="show-all">คุณจอง <strong>KUBOTA</strong> รุ่น <strong class="model">M7508</strong></p> -->
        </div>
        <div class="box-remark">
          <p id="id_p_box_remark"> </p>
          <!-- <p>หากต้องการแก้ไขข้อมูลกรุณาติดต่อที่<br class="show-all"><strong>หจก.คูโบต้าศรีสะเกษเลาเจริญ</strong></p> -->
        </div>
      </div>
    </div>
  </div>

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">
      $('.btn-editphone').click(function(){
        showLoading();
        $('#submit_form').attr('action', site_url+'/g-otp');
        $('#submit_form').submit();
      });

      $('.btn-submit').click(function(){
        var check= true;
        if($('#otp').val()=== ""){
          $('#id_p_box_bg').text('กรุณากรอกรหัส OTP');
          popupCheckId();
          check = false;
          // return false;
        }else{
          // $("#otp").parent().removeClass('error');
        }

        var data = new FormData();
        data.append("otp", $('#otp').val());
        data.append("ref", $('#ref').val());
        data.append("transid", $('#transid').val());
        data.append("phone", $('#phone').val());
        data.append("action", 'v-ajax-otp');
        if(check == false){
          return false;
        }
        $('.btn-submit').css('pointer-events','none');
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            $('.btn-submit').css('pointer-events','auto');
            // $('input').val('');
            // $('textarea').val('');
            if(data.Status == 'ok'){
              if(data.objSTATUS[0] == 'OK'){
                
                if($('#method').val() == '1'){
                  showLoading();
                  $('#submit_form').submit();
                }else{
                  showLoading();
                  $('#submit_form').attr('action', site_url+'/agent-register-form');
                  $('#submit_form').submit();
                }
                
              }else{
                console.log('something went wrong'+data.objDETAIL[0]);
                if(data.objDETAIL[0] == "The OTP. Verifed"){
                  // alert('OTPนี้ได้รับการยืนยันไปแล้วกรุณากด ขอรหัส OTP อีกครั้ง');
                  $('#id_p_box_bg').text('OTP นี้ได้รับการยืนยันไปแล้วกรุณากด ขอรหัส OTP อีกครั้ง');
                  popupCheckId();
                }else{
                  // alert(data.objDETAIL[0]);
                  $('#id_p_box_bg').text('กรุณากรอกรหัสให้ถูกต้อง หรือ กรุณากดส่งขอรหัส OTP อีกครั้ง');
                  popupCheckId();
                }
                
              }
            }else{
              if(typeof data.MSG[0] !== "undefined")
              {
                // alert(data.MSG[0]);
                $('#id_p_box_bg').text(data.MSG[0]);
                popupCheckId();
              }else{
                // alert(data.MSG);
                $('#id_p_box_bg').text(data.MSG);
                popupCheckId();
              }
            }

          },
          error: function (data) {
            // alert(data);
            console.log("error: "+data);
            alert('something went wrong! please try again later.');
            // return false;
          }
        }); // close ajax
       
      });

      $('.btn-resubmit').click(function(){
        var check= true;
        if($('#phone').val()=== ""){
          $("#phone").parent().addClass('error');
          check = false;
        }else{
          $("#phone").parent().removeClass('error');
        }

        var data = new FormData();

        data.append("phone", $('#phone').val());
        data.append("action", 'send-ajax-phoneotp');
        if(check == false){
          return false;
        }
        $('.btn-resubmit').css('pointer-events','none');
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            // alert('success');
            $('.btn-submit').css('pointer-events','auto');
            if(data.Status == 'ok'){
              // console.log(data.transid);
              // console.log(data.ref);
              showLoading();
              $('#transid').val(data.transid[0]);
              $('#ref').val(data.ref[0]);
              $('#add_ref').text(data.ref[0]);
              hideLoading();
              // alert('OTP sent!');
            }else{
              if(typeof data.MSG[0] !== "undefined")
              {
                // alert(data.MSG[0]);
                $('#id_p_box_bg').text(data.MSG[0]);
                popupCheckId();
              }else{
                // alert(data.MSG);
                $('#id_p_box_bg').text(data.MSG);
                popupCheckId();
              }
            }

          },
          error: function (data) {
            // alert(data);
            console.log("error: "+data);
            alert('something went wrong! please try again later.');
            // return false;
          }
        }); // close ajax
       
      });
      function popupCheckId() {
        Fancybox.show([{ src: "#popup", type: "inline" }]);
      }
      // showLoading();
      // hideLoading();
  </script>
  <!-- end javascript this page -->