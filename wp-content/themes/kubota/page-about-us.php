
<?php 

$currentLang =  (qtranxf_getLanguage() == 'en')? '/'.qtranxf_getLanguage().'/': '/';
$Lang =  (qtranxf_getLanguage() == 'en')? 'en' : 'th';

// echo qtranxf_generateLanguageSelectCode('c_'.$Lang);
$data = get_aboutus();
// certified
// achievements
// contents
// echo "<pre>"; var_dump($data); die;
?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="about-us-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>
    <!-- start content this page -->
    <!--#container-->
    <main id="main-content">
      <div class="breadcrumb">
        <div class="container">
          <a class="btn-breadcrumb"><?php echo $data['banner'][0]['bannerName']; ?></a>
          <a href="history" class="btn-breadcrumb in-page"><?php echo $data['contents'][0]['history']; ?></a>
          <a href="production-facilities" class="btn-breadcrumb in-page"><?php echo $data['contents'][0]['production_facilities_subject']; ?></a>
          <a href="in-house-engineering" class="btn-breadcrumb in-page"><?php echo $data['contents'][0]['inhouse_engineering']; ?></a>
          <a href="quality-assurance" class="btn-breadcrumb in-page"><?php echo $data['contents'][0]['quality_assurance']; ?></a>
          <a href="certified-awards" class="btn-breadcrumb in-page"><?php echo $data['contents'][0]['certified_awards']; ?></a>
          <a href="ship-orders-records" class="btn-breadcrumb in-page"><?php echo $data['contents'][0]['outstanding_achievements_title_menu']; ?></a>
        </div>
      </div>

      <div class="banner-page">
        <figure class="banner">
          <img src="<?php echo get_template_directory_uri();?>/assets/img/share/img-mock-1366x340.png" alt="" class="img-mock">
          <img src="<?php echo $data['banner'][0]['image']; ?>" alt="">
        </figure>

        <div class="box-text">
          <div class="container">
            <h1><?php echo $data['banner'][0]['bannerName']; ?></h1>
            <p><?php echo $data['banner'][0]['detailBanner']; ?></p>
          </div>
        </div>
      </div>

      <div class="content-page">
        <div class="section-history">
          <div class="container section-01">
            <div class="title-section">
              <h2 class="title"> <?php echo $data['contents'][0]['history']; ?></h2>
            </div>
               <?php echo $data['contents'][0]['history_detail_']; ?>
            </div>

          <figure class="img-full">
            <img src="<?php echo $data['contents'][0]['history_image_1']; ?>" alt="">
          </figure>

          <div class="container section-02">
            <div class="group-content">
              <div class="box-text">
                <?php echo $data['contents'][0]['history_detail_2']; ?>
              </div>

              <figure class="img">
                <img src="<?php echo get_template_directory_uri();?>/assets/img/share/img-mock-512x460.png" alt="" class="img-mock">
                <img src="<?php echo $data['contents'][0]['history_image_2']; ?>" alt="">
              </figure>
            </div>
          </div>
        </div>


        <div class="section-production-facilities">
          <div class="container section-01">
            <div class="title-section">
              <h2 class="title"><?php echo $data['contents'][0]['production_facilities_subject']; ?></h2>
            </div>
            <?php echo $data['contents'][0]['production_facilities_detail']; ?>
          </div>
          
          <figure class="img-full">
            <img src=" <?php echo $data['contents'][0]['production_facilities_image']; ?>" alt="">
          </figure>

          <div class="container section-02">
            <div class="group-content">
              <figure class="img">
                <img src="<?php echo get_template_directory_uri();?>/assets/img/share/img-mock-500x450.png" alt="" class="img-mock">
                <img src="<?php echo $data['contents'][0]['production_facilities_image_2']; ?>" alt="">
              </figure>

              <div class="box-text">
                <div class="title-section">
                  <h2 class="title"><?php echo $data['contents'][0]['production_facilities_subject']; ?></h2>
                </div>

                <div class="nav-tab">
                  <a href="#" class="active">Production</a>
                  <a href="#">Major Facilities / Working Aids</a>
                </div>
                
                <div class="tabs">
                  <div class="tab w50p active">
                    <div class="item">
                      <?php for($i=0;$i<8;$i++): ?>
                        <div class="row">
                          <div class="col">
                            <p><?php echo $data['contents'][0]['production_facilities_productions'][$i]['subject']; ?></p>
                          </div>
                          <div class="col">
                            <p><?php echo $data['contents'][0]['production_facilities_productions'][$i]['detail']; ?></p>
                          </div>
                        </div>
                      <?php endfor; ?>
                    </div>
                    <?php if(count($data['contents'][0]['production_facilities_productions']) > 8): ?>
                      
                        <div class="item">
                        <?php for($i=9;$i<count($data['contents'][0]['production_facilities_productions']);$i++): ?>
                          <div class="row">
                            <div class="col">
                              <p><?php echo $data['contents'][0]['production_facilities_productions'][$i]['subject']; ?></p>
                            </div>
                            <div class="col">
                              <p><?php echo $data['contents'][0]['production_facilities_productions'][$i]['detail']; ?></p>
                            </div>
                          </div>
                          <?php endfor; ?>
                          <div class="row">
                            <?php echo $data['contents'][0]['production_facilities_productions_capability'];    ?>
                          </div>
                          <div class="row">
                          <?php echo $data['contents'][0]['production_facilities_productions_shipmaterial'];    ?>
                          </div>
                        </div>
                    <?php endif; ?>
                  </div>
                  <div class="tab">
                    <div class="item">
                      <?php foreach($data['contents'][0]['production_facilities_major_facilities'] as $key => $value): ?>
                        <div class="row">
                          <div class="col">
                            <ul>
                              <li><?php echo $value['subject']; ?></li>
                            </ul>
                          </div>
                        </div>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="section-in-house-engineering">
          <div class="container section-01">
            <div class="title-section">
              <h2 class="title"><?php echo $data['contents'][0]['inhouse_engineering']; ?></h2>
            </div>
            <?php echo $data['contents'][0]['inhouse_engineering_detail_1']; ?>
          </div>

          <figure class="img-full">
            <img src="<?php echo $data['contents'][0]['inhouse_engineering_image_1']; ?>" alt="">
          </figure>

          <div class="container section-02">
            <div class="group-content">
              <div class="box-text">
                <?php echo $data['contents'][0]['inhouse_engineering_detail_2']; ?> 
              </div>

              <figure class="img">
                <img src="<?php echo get_template_directory_uri();?>/assets/img/share/img-mock-512x460.png" alt="" class="img-mock">
                <img src="<?php echo $data['contents'][0]['inhouse_engineering_image_2']; ?>" alt="">
              </figure>
            </div>
          </div>
        </div>


        <div class="section-quality-assurance">
          <div class="container section-01">
            <div class="title-section">
              <h2 class="title"><?php echo $data['contents'][0]['quality_assurance']; ?></h2>
            </div>
            <?php echo $data['contents'][0]['quality_assurance_detail_1']; ?>
          </div>

          <figure class="img-full">
            <img src="<?php echo $data['contents'][0]['quality_assurance_image_1']; ?>" alt="">
          </figure>

          <div class="container section-02">
            <div class="group-content">
              <div class="box-text">
                <?php echo $data['contents'][0]['quality_assurance_detail_2']; ?>              
              </div>

              <figure class="img">
                <img src="<?php echo get_template_directory_uri();?>/assets/img/share/img-mock-512x460.png" alt="" class="img-mock">
                <img src="<?php echo $data['contents'][0]['quality_assurance_image_2']; ?>" alt="">
              </figure>
            </div>
          </div>
        </div>


        <div class="section-certified-awards">
          <div class="container">
            <div class="title-section">
              <h2 class="title"><?php echo $data['contents'][0]['certified_awards']; ?></h2>
            </div>

            <?php echo $data['contents'][0]['certified_awards_detail']; ?>
            <div class="slide-certified">
              <div class="owl-carousel">
                <?php foreach($data['certified'] as $key => $value): ?>
                  <div class="item">
                    <figure class="img">
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/share/img-mock-287x401.png" alt="" class="img-mock">
                      <img src="<?php echo $value['image']; ?>" alt="">
                    </figure>
                    <div class="box-text">
                      <p><?php echo $value['title']; ?></p>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>


        <div class="section-ship-orders-records">
          <div class="container">
            <div class="title-section">
              <h2 class="title"><?php echo $data['contents'][0]['outstanding_achievements']; ?></h2>
            </div>
            
              <?php echo $data['contents'][0]['outstanding_achievements_detail']; ?>
            <div class="title-section">
              <h2 class="title"><?php echo $data['contents'][0]['outstanding_achievements_title_table']; ?></h2>
            </div>

            <div class="table-orders-records">
              <div class="table-style">
                <table>
                  <thead>
                    <tr>
                      <th><?php echo ($Lang == 'en')? 'List of Customers' : 'รายนามลูกค้า'; ?></th>
                      <th><?php echo ($Lang == 'en')? 'Type of Vessels' : 'ประเภทเรือ'; ?></th>
                      <th><?php echo ($Lang == 'en')? 'Quantity' : 'จำนวน'; ?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $count=0; ?>
                    <?php foreach($data['achievements'] as $key => $value): ?>
                      <tr>
                        <td><?php echo $value['title'] ?></td>
                        <td>
                          <?php foreach($value['contents'] as $k => $v): ?>
                            <?php echo $v['type_of_vessels']."<br>"; ?>
                          <?php endforeach; ?>
                        </td>
                        <td>
                          <?php foreach($value['contents'] as $k => $v): ?>
                            <?php echo $v['quantity']."<br>"; ?>
                            <?php $count = $count + $v['quantity']; ?>
                          <?php endforeach; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>Total</td>
                      <td>&nbsp;</td>
                      <td><?php echo $count; ?></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </main>
    <!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script>
    /*/assets/js/main.js*/
    runScriptAboutUsPage();
  </script>
  <!-- end javascript this page -->
  <?php get_footer('endbody'); ?>