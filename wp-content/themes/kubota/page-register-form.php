<?php 
  session_start();
	if(!empty($_POST['method'])){
		$method = $_POST['method'];
	}else{
		$method = 1;
	}
	if(!empty($_POST['cm'])){
		$cm = $_POST['cm'];
	}else{
    $cm = 4;
	}
  if(!empty($_POST['personalid'])){
		$personalid = $_POST['personalid'];
	}else{
    Redirect(site_url(), false);// redirect
	}

  if(!empty($_POST['transid'])){
		$transid = $_POST['transid'];
	}else{
    Redirect(site_url(), false);// redirect
	}

  if(!empty($_POST['ref'])){
		$ref = $_POST['ref'];
	}else{
    Redirect(site_url(), false);// redirect
	}
  if(!empty($_POST['phone'])){
		$phone = $_POST['phone'];
	}else{
    Redirect(site_url(), false);// redirect
	}
  if(!empty($_POST['otp'])){
		$otp = $_POST['otp'];
	}else{
    Redirect(site_url(), false);// redirect
	}
  $data = [];
  $data['provinces'] = get_provinces();
  $data['dataform'] = get_dataform(); //154
  // $data['fullname_'] = get_field('fullname_',154);
	// $data['email_'] = get_field('email_',154);
	// $data['address_'] = get_field('address_',154);
	// $data['province_'] = get_field('province__',154);
	// $data['use_area_'] = get_field('use_area_',154);
	// $data['paymentmethod_'] = get_field('paymentmethod_',154);
?>
 <?php get_header();?>
  <!--#wrapper-->
  <div id="wrapper" class="register-page">
    <!-- header => [menu, share top content] -->
	<?php get_header('navigation');?>

		<!-- start content this page -->
		<!--#container-->
		<main id="main-content">
      <div class="img-bg">
				<picture>
					<source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg-mobile.jpg" media="(max-width: 479px)" type="image/jpeg">
					<img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-product-bg.jpg" alt="Background" loading="lazy" width="1920" height="680">
				</picture>
			</div>
      
      <div class="container">
        <div class="box-content progress">
          <div class="inner">
            <div class="items">
              <div class="item active">
                <div class="icon">
                  <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/profile.svg" alt="">
                  <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/profile-active.svg" alt="">
                </div>
                <p>1.ข้อมูลส่วนตัว</p>
              </div>
              <div class="item" id="select_series_item">
                <div class="icon">
                  <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/kubota.svg" alt="">
                  <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/kubota-active.svg" alt="">
                </div>
                <p>2.เลือกรุ่นที่ต้องการ</p>
              </div>
              <div class="item" id="select_sumary_item">
                <div class="icon">
                  <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/summary.svg" alt="">
                  <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/summary-active.svg" alt="">
                </div>
                <p>3.สรุปข้อมูล</p>
              </div>
            </div>
          </div>
        </div>
        
        <div id="form-step-01" class="box-content register-form">
          <div class="inner">
            <div class="form-style form-full">
              <form action="<?php echo site_url('thank-you'); ?>" method="post" id="submit_form">
                <input type="hidden" id="register_title" name="register_title" >
                <input type="hidden" id="otp" name="otp"  value="<?php echo $otp; ?>" >
                <input type="hidden" id="transid" name="transid"  value="<?php echo $transid; ?>" >
                <input type="hidden" id="ref" name="ref"  value="<?php echo $ref; ?>" >
                <input type="hidden" id="method" name="method" value="<?php echo $method; ?>">
                <input type="hidden" id="cm" name="cm" value="<?php echo $cm; ?>">

                <a href="#" class="btn-back">
                  <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/back.svg" alt="icon">
                </a>
                
                <div class="title-page">
                  <h1 class="title">
                    <span class="title-profile">แบบฟอร์มสั่งจอง<br class="mobile-only">แทรกเตอร์คูโบต้า <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" class="logo-m-series" width="156" h="16"></span>
                    <span class="title-model">แบบฟอร์มสั่งจอง<br class="mobile-l">แทรกเตอร์คูโบต้า <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" class="logo-m-series" width="156" h="16"></span>
                    <span class="title-summary">รายละเอียดการสั่งจอง<br class="mobile-l">แทรกเตอร์คูโบต้า <img src="<?php echo get_template_directory_uri();?>/assets/img/share/logo-m-series.svg" alt="M-SERIES" class="logo-m-series" width="156" h="16"></span>
                  </h1>
                </div>
                
                <div class="box-steps">
                  <div class="box-step box-profile">
                    
                    <div class="text-remark">
                      <p>จำกัดสิทธิ์ 1 หมายเลขบัตรประชาชน
                        <br class="mobile-only">ต่อการสั่งจอง 1 รุ่น เท่านั้น</p>
                    </div>
                  
                    <h2>
                      <span class="title-profile">กรุณากรอกข้อมูลให้ครบถ้วน</span>
                      <span class="title-summary">ข้อมูลส่วนตัว</span>
                    </h2>
                    <div class="row">
                      <div class="box-input required disabled">
                        <label for="personalid" class="text-label">หมายเลขบัตรประชาชน</label>
                        <input type="tel" id="personalid" class="form-input" name="personalid" value="<?php echo $personalid; ?>" disabled>
                        <p class="text-validate">กรุณาระบุ</p>
                      </div>
                      <?php if($data['dataform']['fullname_'] == 'open'):  ?>
                      <div class="box-input required">
                        <label for="fullname" class="text-label">ชื่อ - นามสกุล</label>
                        <input type="text" id="fullname" class="form-input" name="fullname" >
                        <p class="text-validate">กรุณาระบุ</p>
                      </div>
                      <?php endif; ?>
                      <div class="box-input required disabled">
                        <label for="phone" class="text-label">หมายเลขโทรศัพท์มือถือที่ติดต่อได้</label>
                        <input type="tel" id="phone" class="form-input" name="phone" value="<?php echo $phone; ?>" disabled>
                        <p class="text-validate">กรุณาระบุเบอร์โทรศัพท์ให้ถูกต้อง</p>
                      </div>
                      <?php if($data['dataform']['email_'] == 'open'):  ?>
                      <div class="box-input">
                        <label for="email" class="text-label">อีเมล (ถ้ามี)</label>
                        <input type="email" id="email" class="form-input" name="email" placeholder="info@gmail.com">
                      </div>
                      <?php endif; ?>
                    </div>
                    <div class="row">
                      <?php if($data['dataform']['address_'] == 'open'):  ?>
                      <div class="box-input required w-full">
                        <label for="address" class="text-label">ที่อยู่ (บ้านเลขที่ หมู่ ซอย ถนน)</label>
                        <input type="textarea" id="address" class="form-input" name="address" >
                        <p class="text-validate">กรุณาระบุ</p>
                      </div>
                      <?php endif; ?>
                      <?php if($data['dataform']['province_'] == 'open'):  ?>
                      <div class="box-input required">
                        <label for="provinces" class="text-label">จังหวัด</label>
                        <select class="form-select" name="Ref_prov_id" id="provinces">
                                <option value="" selected disabled>เลือกจังหวัด</option>
                                <?php foreach ($data['provinces'] as $value) { ?>
                                <option value="<?=$value['id']?>"><?=$value['name_th']?></option>
                                <?php } ?>
                          </select>
                        <p class="text-validate">กรุณาระบุ</p>
                      </div>
                      <div class="box-input required">
                        <label for="amphures" class="text-label">อำเภอ</label>
                        <select class="form-select" name="Ref_dist_id" id="amphures">
                          <option value="" selected disabled>เลือกอำเภอ</option>
                          </select>
                        <p class="text-validate">กรุณาระบุ</p>
                      </div>
                      <div class="box-input required">
                        <label for="districts" class="text-label">ตำบล</label>
                        <select class="form-select" name="Ref_subdist_id" id="districts">
                          <option value="" selected disabled>เลือกตำบล</option>
                          </select>
                        <p class="text-validate">กรุณาระบุ</p>
                      </div>
                      <div class="box-input required">
                        <label for="zip_code" class="text-label">รหัสไปรษณีย์</label>
                        <input type="tel" name="zip_code" id="zip_code" class="form-input" placeholder="000000">
                        <p class="text-validate">กรุณาระบุ</p>
                      </div>
                      <?php endif; ?>
                    </div>
                  </div>

                  <div class="box-step box-model">
                    <h2>
                      <span class="title-model">เลือกรุ่นแทรกเตอร์ที่ต้องการสั่งจอง</span>
                      <span class="title-summary">รุ่นที่จอง</span>
                    </h2>
                    <div class="select-model">
                      
                      <div class="item">
                        <input type="radio" id="M9808" name="models_car" <?php echo ($cm == 3)? 'checked' : '' ; ?> value="3">
                        <label for="M9808">
                          <span class="check"></span>
                  
                          <picture>
                            <source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m9808@2x.png 2x" type="image/png">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m9808.png" alt="M9808" loading="lazy" width="176" height="140">
                          </picture>
                          <span class="text">
                            <span class="name-model">แทรกเตอร์คูโบต้า M9808</span>
                            <span class="group-text">
                              <span>ขนาด 98 แรงม้า</span>
                              <span><strong>1,379,000</strong> บาท</span>
                            </span>
                          </span>
                        </label>
                      </div>
                      <div class="item">
                        <input type="radio" id="M8808" name="models_car" <?php echo ($cm == 2)? 'checked' : '' ; ?> value="2">
                        <label for="M8808">
                          <span class="check"></span>
                  
                          <picture>
                            <source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m8808@2x.png 2x" type="image/png">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m8808.png" alt="M8808" loading="lazy" width="176" height="140">
                          </picture>
                          <span class="text">
                            <span class="name-model">แทรกเตอร์คูโบต้า <br class="mobile-only">M8808</span>
                            <span class="group-text">
                              <span>ขนาด 88 แรงม้า</span>
                              <span><strong>1,254,000</strong> บาท</span>
                            </span>
                          </span>
                        </label>
                      </div>
                      <div class="item">
                        <input type="radio" id="M7508" name="models_car" <?php echo ($cm == 1)? 'checked' : '' ; ?> value="1">
                        <label for="M7508">
                          <span class="check"></span>
                  
                          <picture>
                            <source srcset="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m7508@2x.png 2x" type="image/png">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/uploads/img-m7508.png" alt="M7508" loading="lazy" width="176" height="140">
                          </picture>
                          <span class="text">
                            <span class="name-model">แทรกเตอร์คูโบต้า <br class="mobile-only">M7508</span>
                            <span class="group-text">
                              <span>ขนาด 75 แรงม้า</span>
                              <span><strong>1,058,000</strong> บาท</span>
                            </span>
                          </span>
                        </label>
                      </div>

                    </div>
                    <div class="row row-relate">
                      <div class="group-form payment">
                        <?php if($data['dataform']['paymentmethod_'] == 'open'):  ?>
                          <h2>ท่านประสงค์ชำระค่าสินค้าในรูปแบบใด</h2>
                          <div class="form-radio">
                            <input type="radio" id="loan" name="paymentmethod" value="loan">
                            <label for="loan">เช่าซื้อผ่านบริษัท สยามคูโบต้า ลีสซิ่ง จำกัด (SKL)<br><small>ส่วนลดดอกเบี้ยพิเศษ 1% (100 คันแรก)</small></label>
                          </div>
                          <div class="form-radio">
                            <input type="radio" id="cash" name="paymentmethod" value="cash">
                            <label for="cash">เงินสด<br><small>ส่วนลดสินค้า 20,000 บาท (100 คันแรก)</small></label>
                          </div>
                          <div class="form-radio">
                            <input type="radio" id="other" name="paymentmethod" value="other">
                            <label for="other">อื่นๆ โปรดระบุ<br><small>ส่วนลดสินค้า 20,000 บาท (100 คันแรก)</small></label>
                          </div>
                          <div class="group-form-hide">
                            <div class="box-input w-full required">
                              <label for="other_payment_methond_text" class="text-label">โปรดระบุ</label>
                              <input type="text" name="other_payment_methond_text" id="other_payment_methond_text" class="form-input" placeholder="โปรดระบุ">
                              <p class="text-validate">กรุณาระบุ</p>
                            </div>
                          </div>
                        <?php endif; ?>
                      </div>
                      <div class="group-form area">
                        <?php if($data['dataform']['use_area_'] == 'open'):  ?>
                          <h2>พื้นที่ในการใช้งานแทรกเตอร์</h2>
                          <div class="form-radio">
                            <input type="radio" id="profileaddress" name="use_area" value="profileaddress">
                            <label for="profileaddress">ใช้ที่อยู่เดียวกับข้อมูลส่วนตัว</label>
                          </div>
                          <div class="form-radio">
                            <input type="radio" id="otheraddress" name="use_area" value="otheraddress">
                            <label for="otheraddress">ใช้ในพื้นที่อื่น</label>
                          </div>
                          <div class="group-form-hide">
                            <div class="box-input w-full required">
                              <label for="sel1" class="text-label">จังหวัด:</label>
                              <select class="form-select" name="Ref_prov_id_otheraddress" id="provinces_otheraddress">
                                    <option value="" selected>เลือกจังหวัด</option>
                                    <?php foreach ($data['provinces'] as $value) { ?>
                                    <option value="<?=$value['id']?>"><?=$value['name_th']?></option>
                                    <?php } ?>
                              </select>
                              <p class="text-validate">กรุณาระบุ</p>
                            </div>
                            <div class="box-input w-full required">
                              <label for="sel1" class="text-label">อำเภอ</label>
                              <select class="form-select" name="Ref_dist_id_otheraddress" id="amphures_otheraddress">
                                <option value="">เลือกอำเภอ</option>
                              </select>
                              <p class="text-validate">กรุณาระบุ</p>
                            </div>
                          </div>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="group-btn">
                  <a href="#" class="btn -outline btn-cancel">
                    <span>
                      ยกเลิก
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/cross.svg" alt="icon">
                    </span>
                  </a>
                  <a href="#" class="btn btn-next">
                    <span>
                      ถัดไป
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/next.svg" alt="icon">
                    </span>
                  </a>
                  <button type="button" class="btn btn-submit">
                    <span>
                      ตกลง
                      <img src="<?php echo get_template_directory_uri();?>/assets/img/icons/correct.svg" alt="icon">
                    </span>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> 
			
		</main>
		<!-- end content this page -->

    <!-- footer => /body to /html [popup inline] -->
    <?php get_footer(); ?>
  </div>
  <!--end #wrapper-->

  <div style="display: none;">
    <div id="popup" class="popup-check-id">
      <div class="inner">
        <h2>กรอกข้อมูลการสมัคร</h2>
        <div class="box-bg">
          <p id="id_p_box_bg"> </p>
          <!-- <p>หมายเลขบัตรประชาชนนี้ถูกลงทะเบียนแล้ว<br class="show-all">คุณจอง <strong>KUBOTA</strong> รุ่น <strong class="model">M7508</strong></p> -->
        </div>
        <div class="box-remark">
          <p id="id_p_box_remark"> </p>
          <!-- <p>หากต้องการแก้ไขข้อมูลกรุณาติดต่อที่<br class="show-all"><strong>หจก.คูโบต้าศรีสะเกษเลาเจริญ</strong></p> -->
        </div>
      </div>
    </div>
  </div>

  <!-- javascript => inc all js -->
  <?php get_footer('javascript'); ?>

  <!-- start javascript this page -->
  <script type="text/javascript">
      $('.btn-cancel').click(function(){ 
       

        if($( "#select_series_item" ).hasClass( "active" )){
          console.log('cancel');
          $("input[name='paymentmethod']").prop('checked', false);
          $("input[name='use_area']").prop('checked', false);
          $('#other_payment_methond_text').val(''); 
         

        }

        if($( "#select_sumary_item" ).hasClass( "active" )){
          console.log('cancel');
          $("input[name='paymentmethod']").prop('checked', false);
          $("input[name='use_area']").prop('checked', false);
          $('#other_payment_methond_text').val(''); 
          $('#fullname').val('');
          $('#address').val('');
          $('#provinces').val('');
          $('#amphures').val('');
          $('#districts').val('');
          $('#zip_code').val('');
          $('#provinces_otheraddress').val('');
          $('#amphures_otheraddress').val('');

        }
      });

      $('.btn-submit').click(function(){

        var check= true;
        var filter_email =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if($("#email").length != 0) {
          if($('#email').val() != "" ){ 
            if(!filter_email.test($('#email').val())){
                check = false;
                $("#email").parent().addClass('error');
            }else {
              $("#email").parent().removeClass('error');
            }
          }else{
            $("#email").parent().removeClass('error');
          }
        }
        
        if($("#fullname").length != 0) {
          if($('#fullname').val()=== ""){ 
            $("#fullname").parent().addClass('error');
            // alert('กรอกชื่อ'); return false;
            check = false;
          }else{
            $("#fullname").parent().removeClass('error');
          }
        }

        if($("#address").length != 0) {
          if($('#address').val()=== ""){ 
            $("#address").parent().addClass('error');
            // alert('กรอกที่อยู่'); return false;
            check = false;
          }else{
            $("#address").parent().removeClass('error');
          }
        }

        if(($("#provinces").length != 0) &&  ($("#amphures").length != 0) && ($("#districts").length != 0)  ) {
          if($('#provinces').val()=== "" || $('#provinces').val()=== null){ 
            $("#provinces").parent().addClass('error');
            // alert('ระบุจังหวัด'); return false;
            check = false;
          }else{
            $("#provinces").parent().removeClass('error');
          }

          if($('#amphures').val()=== "" || $('#amphures').val()=== null){ 
            $("#amphures").parent().addClass('error');
            // alert('ระบุอำเภอ'); return false;
            check = false;
          }else{
            $("#amphures").parent().removeClass('error');
          }

          // if($('#districts').val()=== "" || $('#districts').val()=== null ){ 
          //   $("#districts").parent().addClass('error');
          //   // alert('ระบุตำบล'); return false;
          //   check = false;
          // }else{
          //   $("#districts").parent().removeClass('error');
          // }

          if($('#zip_code').val()=== "" || $('#zip_code').val()=== null ){ 
            $("#zip_code").parent().addClass('error');
            // alert('ระบุตำบล'); return false;
            check = false;
          }else{
            $("#zip_code").parent().removeClass('error');
          }
        }

        if( $("input[name='models_car']").length != 0   ) {
          if(typeof $("input[name='models_car']:checked").val() === 'undefined' ){ 
            $('#id_p_box_bg').text('กรุณาเลือกรุ่นแทรกเตอร์ที่ต้องการสั่งจอง');
            popupCheckId();
            check = false;
          }
          
        }// check models_car


        if( $("input[name='paymentmethod']").length != 0   ) {
          if(typeof $("input[name='paymentmethod']:checked").val() === 'undefined' ){ 
            $(".payment").parent().addClass('error');
            // alert('เลือกรูปแบบการชำระเงิน'); return false;
            check = false;
          }else{
            $(".payment").parent().removeClass('error');
            // $("#").parent().removeClass('error');
          }

          if(typeof $("input[name='paymentmethod']:checked").val() !== 'undefined' ){ 
            if($("input[name='paymentmethod']:checked").val() == 'other'){
              if($('#other_payment_methond_text').val() == ""){
                $("#other_payment_methond_text").parent().addClass('error');
                check = false;
              }else{
                $("#other_payment_methond_text").parent().removeClass('error');
              }
            }
          }
          
        }// check payment

        if( $("input[name='use_area']").length != 0   ) {
          if( typeof $("input[name='use_area']:checked").val() === 'undefined' ){ 
            // alert('พื้นที่ในการใช้งานแทรกเตอร์'); return false;
            $(".area").parent().addClass('error');
            check = false;
          }else{
            $(".area").parent().removeClass('error');
          }

          if(typeof $("input[name='use_area']:checked").val() !== 'undefined' ){ 
            if($("input[name='use_area']:checked").val() == 'otheraddress'){

              if($('#provinces_otheraddress').val()=== "" || $('#provinces_otheraddress').val()=== null){ 
                $("#provinces_otheraddress").parent().addClass('error');
                // alert('ระบุจังหวัด'); return false;
                check = false;
              }else{
                $("#provinces_otheraddress").parent().removeClass('error');
              }

              if($('#amphures_otheraddress').val()=== "" || $('#amphures_otheraddress').val()=== null){ 
                $("#amphures_otheraddress").parent().addClass('error');
                // alert('ระบุอำเภอ'); return false;
                check = false;
              }else{
                $("#amphures_otheraddress").parent().removeClass('error');
              }

            }
          }

        } //check area class
        
        if(check == false){
          return false;
        }
        $('.btn-submit').css('pointer-events','none'); // prevent multiple click

        var data = new FormData(); //set data
        data.append("otp", $('#otp').val());
        data.append("ref", $('#ref').val());
        data.append("method", $('#method').val());
        data.append("transid", $('#transid').val());
        data.append("phone", $('#phone').val());
        data.append("fullname", $('#fullname').val());
        data.append("email", $('#email').val());   
        data.append("address", $('#address').val());
        data.append("personalid", $('#personalid').val());
        data.append("provinces", $('#provinces option:selected').text());
        data.append("amphures", $('#amphures option:selected').text());
        data.append("districts", $('#districts option:selected').text());
        data.append("zip_code", $('#zip_code').val());
        data.append("other_payment_methond_text", $('#other_payment_methond_text').val());
        data.append("address", $('#address').val());
        data.append("models_car", $("input[name='models_car']:checked").val());
        data.append("paymentmethod", $("input[name='paymentmethod']:checked").val());
        data.append("use_area", $("input[name='use_area']:checked").val());
        data.append("provinces_otheraddress", $('#provinces_otheraddress option:selected').text());
        data.append("amphures_otheraddress", $('#amphures_otheraddress option:selected').text());
        data.append("action", 'save-ajax-register');
        showLoading();
        $.ajax({
          method: "POST",
          url: admin_url,
          data: data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          dataType: "json",
          success: function (data) {
            // alert('success');
            $('.btn-submit').css('pointer-events','auto');
            if(data.Status == 'OK'){
            
              $('#register_title').val(data.codetitle);
              $('#cm').val($("input[name='models_car']:checked").val());
              $('#submit_form').submit(); //if ok goto thank you page
              // hideLoading();
            }else if(data.Status == 'error_login'){
              $('#id_p_box_bg').text(data.MSG);
              popupCheckId();
              // alert(data.MSG);
            }else{
              $('#id_p_box_bg').text(data.MSG);
              popupCheckId();
              // $('#submit_form').attr('action', site_url+'/gu-otp');
              // $('#submit_form').submit();
            }

          },
          error: function (data) {
            // alert(data);
            console.log("error: "+data);
            alert('something went wrong! please try again later.');
            // return false;
          }
        }); // close ajax
       
      });

      $('#provinces').change(function() {
        var id_province = $(this).val();

          $.ajax({
          type: "POST",
          url: site_url+'/ajax_db',
          data: {id:id_province,function:'provinces'},
          success: function(data){
              $('#amphures').html(data); 
              $('#districts').html(' '); 
              $('#districts').val(' ');  
              $('#zip_code').val(' '); 
          }
        });
      });

      $('#amphures').change(function() {
        var id_amphures = $(this).val();

          $.ajax({
          type: "POST",
          url: site_url+'/ajax_db',
          data: {id:id_amphures,function:'amphures'},
          success: function(data){
              $('#districts').html(data);  
          }
        });
      });

      $('#districts').change(function() {
        var id_districts= $(this).val();

          $.ajax({
          type: "POST",
          url: site_url+'/ajax_db',
          data: {id:id_districts,function:'districts'},
          success: function(data){
              $('#zip_code').val(data);
              console.log( $('#zip_code').val(data));
          }
        });
  
      });

      $('#provinces_otheraddress').change(function() {
        var id_province = $(this).val();

          $.ajax({
          type: "POST",
          url: site_url+'/ajax_db',
          data: {id:id_province,function:'provinces'},
          success: function(data){
              $('#amphures_otheraddress').html(data);
          }
        });
      });

      $(function(){
        $('.register-form .btn-next').on('click', function(){
          let itemIndex = $('.progress .item.active').index() + 1;
          let formStep = itemIndex + 1;
          $('.progress .item').removeClass('active').eq(itemIndex).addClass('active');

          $('.register-form').attr('id', 'form-step-0'+formStep+'').addAttr;

          $('html, body').animate({
            scrollTop: 0
          }, 300);

          return false;
        });

        $('.register-form .btn-back').on('click', function(){
          let itemIndex = $('.progress .item.active').index() - 1;
          let formStep = itemIndex + 1;
          $('.progress .item').removeClass('active').eq(itemIndex).addClass('active');

          $('.register-form').attr('id', 'form-step-0'+formStep+'').addAttr;

          $('html, body').animate({
            scrollTop: 0
          }, 300);

          return false;
        });
        
        $('.payment .form-radio').on('change', function () {
          $('.payment .form-radio').removeClass('checked');
          $('.payment .form-input').val("");
          $(this).addClass('checked');
        });

        $('.area .form-radio').on('change', function () {
          $('.area .form-radio').removeClass('checked');
          $('.area option:selected').prop("selected", false);
          $(this).addClass('checked');
        });
      });
      function popupCheckId() {
        Fancybox.show([{ src: "#popup", type: "inline" }]);
      }
  </script>
  <!-- end javascript this page -->